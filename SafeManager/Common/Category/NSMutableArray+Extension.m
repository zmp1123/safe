//
//  NSMutableArray+Extension.m
//  HotNow
//
//  Created by renao on 15/12/25.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import "NSMutableArray+Extension.h"

@implementation NSMutableArray (Extension)

- (instancetype)removeObjs:(BOOL (^)(id obj))condition
{
    NSMutableArray* tmpList = [NSMutableArray new];
    for (id tmp in self) {
        if (condition(tmp)) {
            [tmpList addObject:tmp];
        }
    }
    [self removeObjectsInArray:tmpList];
    return self;
}

- (instancetype)removeObj:(BOOL (^)(id))condition
{
    id tar;
    for (id tmp in self) {
        if (!condition(tmp)) {
            continue;
        }
        tar = tmp;
    }
    if (tar) {
        [self removeObject:tar];
    }
    return self;
}

- (id)findObj:(NSMutableArraySearchBlock)condition
{
    id result;
    for (id tmp in self) {
        if (!condition(tmp))
            continue;
        result = tmp;
    }
    return result;
}
@end
