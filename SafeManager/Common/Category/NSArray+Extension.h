//
//  NSArray+Extension.h
//  HotNow
//
//  Created by renao on 15/12/28.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef BOOL (^ArrayConditionBlock)(id obj);
@interface NSArray (Extension)

- (instancetype)queryObjs:(ArrayConditionBlock)condition;
- (instancetype)map:(id (^)(id obj)) block;
@end
