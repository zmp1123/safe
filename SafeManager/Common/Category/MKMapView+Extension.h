//
//  MKMapView+Extension.h
//  HotNow
//
//  Created by renao on 15/12/25.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Extension)

/**
 *  地图是平移
 */
-(BOOL)isTranslating;

- (float)translatedPercent;
@end
