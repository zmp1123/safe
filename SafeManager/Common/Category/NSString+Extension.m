//
//  NSString+Extension.m
//  ReNao
//
//  Created by zmp1123 on 10/15/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import "CommonCrypto/CommonDigest.h"
#import "NSString+Extension.h"

@implementation NSString (Extension)

#pragma mark 计算字符串大小
- (CGSize)sizeWithFont:(UIFont*)font maxSize:(CGSize)maxSize
{
    NSDictionary* dict = @{ NSFontAttributeName : font };
    CGSize textSize = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil].size;
    return textSize;
}

- (BOOL)isNilOrEmpty
{
    return self.length == 0 || self == nil;
}

- (NSString*)removeCharAtEnd:(unichar)tar
{
    NSString* temp = [NSString stringWithString:self];
    while ([temp characterAtIndex:temp.length - 1] == tar)
    {
        temp = [temp substringToIndex:temp.length - 2];
    }
    return temp;
}

#pragma mark - 计算文字高度、宽度
- (CGFloat)getTextHeightWith:(NSString *)text font:(UIFont *)font width:(CGFloat)width
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    return ceil(rect.size.height);
}
- (CGFloat)getTextWidthWith:(NSString *)text font:(UIFont *)font Height:(CGFloat)Height
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(10000, Height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    return ceil(rect.size.width);
}

+ (NSArray *)rangesOfatString:(NSString *)str
{
    NSMutableArray *ranges = [NSMutableArray array];
    BOOL over = NO;
    NSInteger index = 0;
    NSString *alterableStr = str;
    while (!over) {
        NSRange atRange = [alterableStr rangeOfString:@"@"];
        NSRange resultRange;
        NSLog(@"<%lu || %lu>",atRange.location,atRange.length);
        if (atRange.location > INT_MAX) {
            over = YES;
            break;
        } else {
            resultRange.location = atRange.location + index;
            alterableStr = [alterableStr substringFromIndex:atRange.location];
            NSRange spaceRange = [alterableStr rangeOfString:@" "];
            if (spaceRange.location > INT_MAX) {
                resultRange.length = alterableStr.length;
                NSDictionary *rangeDic = @{@"location":[NSString stringWithFormat:@"%lu",resultRange.location],@"length":[NSString stringWithFormat:@"%lu",resultRange.length]};
                [ranges addObject:rangeDic];
                over = YES;
                break;
            } else {
                resultRange.length = spaceRange.location;
                index = resultRange.location + resultRange.length;
                NSDictionary *rangeDic = @{@"location":[NSString stringWithFormat:@"%lu",resultRange.location],@"length":[NSString stringWithFormat:@"%lu",resultRange.length]};
                [ranges addObject:rangeDic];
                alterableStr = [alterableStr substringFromIndex:spaceRange.location];
            }
        }
    }
    return ranges;
}

- (NSString*)md5_32
{
    NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString* outputString = [NSMutableString new];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [outputString appendFormat:@"%02x", digest[i]];
    }
    return outputString;
}

- (NSString*)md5_16
{
    return [[self md5_32] substringWithRange:NSMakeRange(8, 16)];
}

- (NSDate*)convert2DateFmt:(NSString*)fmt
{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    if (!fmt)
        fmt = @"yyyy-MM-dd HH-mm-ss";
    formater.dateFormat = fmt;
    NSDate* date;
    @try {
        date = [formater dateFromString:self];
    }
    @catch (NSException* exception) {
        NSLog(@"convert2DateFmt error,%@", exception);
    }
    @finally {
        return date;
    }
}

@end
