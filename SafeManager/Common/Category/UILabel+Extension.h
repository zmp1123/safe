//
//  UILabel+Extension.h
//  HotNow
//
//  Created by renao on 15/11/29.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

- (void)setLineSpace:(CGFloat)space;

- (void)changeFontSize:(CGFloat)size;

- (void)scaleFontSize:(CGFloat)scale;

@end
