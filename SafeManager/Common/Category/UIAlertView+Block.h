//
//  UIAlertView+Block.h
//  ReNao
//
//  Created by zmp1123 on 10/12/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CompleteBlock) (NSInteger buttonIndex);

@interface UIAlertView (Block)

// 用Block的方式回调，这时候会默认用self作为Delegate
- (void)showAlertViewWithCompleteBlock:(CompleteBlock) block;

/**
 *  生成带取消按钮和确认按钮的AlertView
 *
 *  @return UIAlertView
 */
+ (UIAlertView*)getNormalAlertViewTitle:(NSString *)title content:(NSString*)content;

@end
