//
//  NSNull+OVNature.m
//  HotNow
//
//  Created by renao on 15/12/16.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import "NSNull+OVNature.h"

@implementation NSNull (OVNature)

- (void)forwardInvocation:(NSInvocation*)anInvocation
{
    if ([self respondsToSelector:[anInvocation selector]]) {
        [anInvocation invokeWithTarget:self];
    }
}

- (NSMethodSignature*)methodSignatureForSelector:(SEL)aSelector
{
    NSMethodSignature* sig = [[NSNull class] instanceMethodSignatureForSelector:aSelector];
    if (!sig) {
        sig = [NSMethodSignature signatureWithObjCTypes:"@^v^c"];
    }
    return sig;
}

@end
