//
//  UIAlertView+Block.m
//  ReNao
//
//  Created by zmp1123 on 10/12/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import "UIAlertView+Block.h"
#import <objc/runtime.h>

@implementation UIAlertView (Block)

static char key;

- (void)showAlertViewWithCompleteBlock:(CompleteBlock)block
{
    if (block) {
        objc_removeAssociatedObjects(self);
        objc_setAssociatedObject(self, &key, block, OBJC_ASSOCIATION_COPY);
        ////设置delegate
        self.delegate = self;
    }
    [self show];
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ///获取关联的对象，通过关键字。
    CompleteBlock block = objc_getAssociatedObject(self, &key);
    if (block) {
        ///block传值
        block(buttonIndex);
    }
}

+ (UIAlertView*)getNormalAlertViewTitle:(NSString*)title content:(NSString*)content
{
    return [[UIAlertView alloc] initWithTitle:title message:content delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
}
@end
