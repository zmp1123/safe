//
//  NSMutableArray+Extension.h
//  HotNow
//
//  Created by renao on 15/12/25.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef BOOL (^NSMutableArraySearchBlock)(id obj);

@interface NSMutableArray (Extension)

- (instancetype)removeObjs:(BOOL (^)(id obj))condition;
- (instancetype)removeObj:(BOOL (^)(id obj))condition;

- (id)findObj:(NSMutableArraySearchBlock)condition;

@end
