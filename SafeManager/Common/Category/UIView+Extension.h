//
//  UIView+Extension.h
//  ReNao
//
//  Created by zmp1123 on 10/15/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;

- (BOOL)popToFrontPosition:(CGRect)rect superView:(UIView*)superView;

@end
