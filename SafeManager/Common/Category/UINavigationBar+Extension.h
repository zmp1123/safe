//
//  UINavigationBar+Extension.h
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Extension)
- (void)lt_setBackgroundColor:(UIColor *)backgroundColor;
@end
