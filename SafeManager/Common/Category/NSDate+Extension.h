//
//  NSDate+Extension.h
//  ReNao
//
//  Created by zmp1123 on 10/15/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)

/**
 *  是否为今天
 */
- (BOOL)isToday;
/**
 *  是否为昨天
 */
- (BOOL)isYesterday;
/**
 *  是否为今年
 */
- (BOOL)isThisYear;

/**
 *  返回一个只有年月日的时间
 */
- (NSDate *)dateWithYMD;

/**
 *  获得与当前时间的差距
 */
- (NSDateComponents *)deltaWithNow;

/**
 *  格式化时间
 *
 */
- (NSString*)dateStringWithFmt:(NSString*)fmt;

@end
