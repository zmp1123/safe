//
//  UIImage+Extension.h
//  ReNao
//
//  Created by zmp1123 on 10/15/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

+ (UIImage *) imageWithName:(NSString *) imageName;
+ (UIImage *) resizableImageWithName:(NSString *)imageName;
- (UIImage*) scaleImageWithSize:(CGSize)size;

@end
