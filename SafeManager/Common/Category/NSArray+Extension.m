//
//  NSArray+Extension.m
//  HotNow
//
//  Created by renao on 15/12/28.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import "NSArray+Extension.h"

@implementation NSArray (Extension)

- (instancetype)queryObjs:(ArrayConditionBlock)condition
{
    NSMutableArray* list = [NSMutableArray new];
    for (id obj in self) {
        if (!condition(obj))
            continue;
        [list addObject:obj];
    }
    return list;
}

-(instancetype)map:(id (^)(id))block{
    NSAssert(block, @"block can't be null");
    NSMutableArray *result = [NSMutableArray new];
    for(id item in self){
        [result addObject:block(item)];
    }
    return result;
}

@end
