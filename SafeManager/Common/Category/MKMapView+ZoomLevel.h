//
//  MKMapView+ZoomLevel.h
//  HotNow
//
//  Created by renao on 15/11/25.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLv
                   animated:(BOOL)animated;
@end
