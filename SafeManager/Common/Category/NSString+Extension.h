//
//  NSString+Extension.h
//  ReNao
//
//  Created by zmp1123 on 10/15/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Extension)

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

/**
 *  string is nil or length is 0
 *
 *  @return BOOL
 */
-(BOOL) isNilOrEmpty;

/**
 *  去除String 结尾的字符
 *  例如http://xxx// ==>http://xx
 *
 *  @param tar char
 *
 *  @return 新的String
 */
- (NSString*)removeCharAtEnd:(unichar)tar;
/**
 *  计算文本高度
 *
 *  @param text  text
 *  @param font  字体
 *  @param width 文本宽度
 *
 *  @return 文本高度
 */
- (CGFloat)getTextHeightWith:(NSString *)text font:(UIFont *)font width:(CGFloat)width;
/**
 *  计算文本宽度
 *
 *  @param text   text
 *  @param font   字体
 *  @param Height 文本高度
 *
 *  @return 文本宽度
 */
- (CGFloat)getTextWidthWith:(NSString *)text font:(UIFont *)font Height:(CGFloat)Height;

+ (NSArray *)rangesOfatString:(NSString *)atString;

- (NSString*)md5_32;
- (NSString*)md5_16;

- (NSDate*)convert2DateFmt:(NSString*)fmt;

@end
