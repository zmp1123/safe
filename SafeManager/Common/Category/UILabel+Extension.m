//
//  UILabel+Extension.m
//  HotNow
//
//  Created by renao on 15/11/29.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

- (void)setLineSpace:(CGFloat)space
{
    NSMutableAttributedString* mutableString = [[NSMutableAttributedString alloc] initWithString:self.text];
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:space];
    [paragraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
    [mutableString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.text.length)];
    [self setAttributedText:mutableString];
}

-(void)changeFontSize:(CGFloat)size
{
    self.font = [self.font fontWithSize:size];
}

-(void)scaleFontSize:(CGFloat)scale
{
    CGFloat size = self.font.pointSize * scale;
    [self changeFontSize:size];
}

@end
