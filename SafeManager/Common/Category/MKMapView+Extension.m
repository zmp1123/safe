//
//  MKMapView+Extension.m
//  HotNow
//
//  Created by renao on 15/12/25.
//  Copyright © 2015年 com.renaoapp. All rights reserved.
//

#import "MKMapView+Extension.h"

@implementation MKMapView (Extension)

- (BOOL)isTranslating
{
    static MKCoordinateSpan span;
    static dispatch_once_t t;
    MKCoordinateSpan mapSpan = self.region.span;
    dispatch_once(&t, ^{
        span = MKCoordinateSpanMake(mapSpan.latitudeDelta, mapSpan.longitudeDelta);
    });
    BOOL result = mapSpan.longitudeDelta == span.longitudeDelta && mapSpan.latitudeDelta == span.latitudeDelta;
    span = MKCoordinateSpanMake(mapSpan.latitudeDelta, mapSpan.longitudeDelta);
    return result;
}


- (float)translatedPercent
{
    float result;
    static MKCoordinateRegion region;
    static dispatch_once_t t;
    MKCoordinateRegion mapRegion = self.region;
    dispatch_once(&t, ^{
        region = MKCoordinateRegionMake(mapRegion.center, mapRegion.span);
    });
    double longitudeOffset = mapRegion.center.longitude - region.center.longitude;
    double latitudeOffset = mapRegion.center.latitude - region.center.latitude;
    double longitudeTransPercent = fabs(longitudeOffset / region.span.longitudeDelta);
    double latitudeTransPercent = fabs(latitudeOffset / region.span.latitudeDelta);

    result = longitudeTransPercent > latitudeTransPercent ? longitudeTransPercent : latitudeTransPercent;
    region = MKCoordinateRegionMake(mapRegion.center, mapRegion.span);
    return result;
}
@end
