//
//  SMSettingViewController.m
//  SafeManager
//
//  Created by zxf on 16/11/1.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMSettingViewController.h"

@interface SMSettingViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, assign) bool baoJingSound;
@property (nonatomic, assign) bool chebufangSound;
@property (nonatomic, assign) bool newVersion;

@end

@implementation SMSettingViewController
@synthesize baoJingSound =_baoJingSound;
@synthesize chebufangSound = _chebufangSound;
@synthesize newVersion = _newVersion;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    [self.view addSubview:self.table];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    if(indexPath.section == 0){
        cell.textLabel.text = indexPath.row == 0 ? @"报警声音" : @"布撤防声音";
    }
    UISwitch *sw = [[UISwitch alloc]initWithFrame:CGRectMake(0, 0, 60, 30)];
    sw.on = true;
    cell.accessoryView = sw;
    sw.tag = indexPath.section * 10 + indexPath.row;
    
    if(indexPath.section == 0){
        sw.on = indexPath.row == 0 ? self.baoJingSound : self.chebufangSound;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [sw addTarget:self action:@selector(onSwitchChange:) forControlEvents:UIControlEventValueChanged];
    return cell;
}

- (void)onSwitchChange:(UISwitch *)sender{
    if(sender.tag == 0){
        self.baoJingSound = sender.on;
    }
    if(sender.tag == 1){
        self.chebufangSound = sender.on;
    }
    if(sender.tag == 10){
        self.newVersion = sender.on;
    }
}

- (bool)baoJingSound{
    id obj = [[NSUserDefaults standardUserDefaults] objectForKey:@"baojingsoundSetting"];
    if(obj){
        _baoJingSound = [obj boolValue];
    }
    return _baoJingSound;
}

- (void)setBaoJingSound:(bool)baoJingSound{
    _baoJingSound = baoJingSound;
    [[NSUserDefaults standardUserDefaults]setBool:baoJingSound forKey:@"baojingsoundSetting"];
}

- (bool)chebufangSound{
    id obj = [[NSUserDefaults standardUserDefaults] objectForKey:@"chebufangSound"];
    if(obj){
        _chebufangSound = [obj boolValue];
    }
    return _chebufangSound;
}

- (void)setChebufangSound:(bool)chebufangSound{
    _chebufangSound = chebufangSound;
    [[NSUserDefaults standardUserDefaults]setBool:chebufangSound forKey:@"chebufangSound"];
}

- (bool)newVersion{
    id obj = [[NSUserDefaults standardUserDefaults] objectForKey:@"newVersion"];
    if(obj){
        _newVersion = [obj boolValue];
    }
    return _newVersion;
}

- (void)setNewVersion:(bool)newVersion{
    _newVersion = newVersion;
    [[NSUserDefaults standardUserDefaults]setBool:newVersion forKey:@"newVersion"];
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _table.dataSource = self;
    }
    return _table;
}

@end
