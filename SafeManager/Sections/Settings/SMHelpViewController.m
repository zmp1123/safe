//
//  SMHelpViewController.m
//  SafeManager
//
//  Created by zxf on 16/11/1.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMHelpViewController.h"

@interface SMHelpViewController ()

@end

@implementation SMHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"使用帮助";
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - 64)];
    NSURLRequest *req = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://139.196.234.54/Home?c=Article&a=Help"]];
    [webView loadRequest:req];
    [self.view addSubview:webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
