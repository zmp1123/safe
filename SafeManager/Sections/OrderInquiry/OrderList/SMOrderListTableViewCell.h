//
//  SMOrderListTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMBaoJing;
@class SMChuJing;
@class SMInsuranceOrder;

@protocol SMOrderListTableViewCellDelegate <NSObject>

@optional

/**
 订单详情按钮响应时间

 @param sid 订单id
 */
- (void)clickOrderDetail:(NSString *)sid;

@end

@interface SMOrderListTableViewCell : UITableViewCell

- (void)refreshModel:(SMBaoJing *)model;

- (void)refreshChuJingModel:(SMChuJing *)model;

- (void)refreshInsuranceOrderModel:(SMInsuranceOrder *)model;

@property (nonatomic, weak) id <SMOrderListTableViewCellDelegate> delegate;

@end
