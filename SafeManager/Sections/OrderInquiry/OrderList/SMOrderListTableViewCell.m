//
//  SMOrderListTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMOrderListTableViewCell.h"
#import "UIColor+Category.h"
#import "SMBaoJing.h"
#import "SMChuJing.h"
#import "SMInsuranceOrder.h"

@interface SMOrderListTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderContentLabel;

@property (weak, nonatomic) IBOutlet UIButton *orderDetailBtn;

@property (nonatomic, strong) SMBaoJing *currentBaoJing;
@property (nonatomic, strong) SMChuJing *currentChuJing;
@property (nonatomic, strong) SMInsuranceOrder *currentInsuranceOrder;

@end

@implementation SMOrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.orderDetailBtn.layer.borderColor = hexColor(5696de).CGColor;
    self.orderDetailBtn.layer.borderWidth = 1.f;
    self.orderDetailBtn.layer.cornerRadius = 3.f;
    self.orderDetailBtn.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)orderDetailAction:(id)sender
{
    if (self.currentBaoJing) {
        if ([self.delegate respondsToSelector:@selector(clickOrderDetail:)]) {
            [self.delegate clickOrderDetail:self.currentBaoJing.oid];
        }
    }else if (self.currentChuJing){
        if ([self.delegate respondsToSelector:@selector(clickOrderDetail:)]) {
            [self.delegate clickOrderDetail:self.currentChuJing.oid];
        }
    }else if (self.currentInsuranceOrder){
        if ([self.delegate respondsToSelector:@selector(clickOrderDetail:)]) {
            [self.delegate clickOrderDetail:self.currentInsuranceOrder.iid];
        }
    }
}

- (void)refreshModel:(SMBaoJing *)model
{
    self.currentBaoJing = model;
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单编号：%@",model.deal_sn];
    self.orderPriceLabel.text = [NSString stringWithFormat:@"订单价格：%@",model.price];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"订单时间：%@",model.order_time];
    self.orderContentLabel.text = [NSString stringWithFormat:@"订单内容：%@",model.vip_title];
}

- (void)refreshChuJingModel:(SMChuJing *)model
{
    self.currentChuJing = model;
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单编号：%@",model.deal_sn];
    self.orderPriceLabel.text = [NSString stringWithFormat:@"订单价格：%@",model.price];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"订单时间：%@",model.order_time];
    self.orderContentLabel.text = [NSString stringWithFormat:@"订单内容：%@",model.service_com_title];
}

- (void)refreshInsuranceOrderModel:(SMInsuranceOrder *)model
{
    self.currentInsuranceOrder = model;
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单编号：%@",model.deal_sn];
    self.orderPriceLabel.text = [NSString stringWithFormat:@"订单价格：%@",model.price];
    self.orderTimeLabel.text = [NSString stringWithFormat:@"订单时间：%@",model.order_time];
    self.orderContentLabel.text = [NSString stringWithFormat:@"订单内容：%@",model.ins_title];
}

@end
