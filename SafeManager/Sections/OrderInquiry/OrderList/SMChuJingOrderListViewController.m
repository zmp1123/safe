//
//  SMChuJingOrderListViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingOrderListViewController.h"
#import "SMOrderListTableViewCell.h"
#import "SMChuJingMgr.h"
#import "SMChuJingOrderDetailViewController.h"

@interface SMChuJingOrderListViewController ()<UITableViewDataSource,UITableViewDelegate, SMOrderListTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation SMChuJingOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"出警服务历史订单";
    [self.view addSubview:self.tableView];
    
    [self getOrderList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64)];
        [_tableView registerNib:[UINib nibWithNibName:@"SMOrderListTableViewCell" bundle:nil] forCellReuseIdentifier:@"SMOrderListTableViewCell"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = hexColor(EDF0F5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMOrderListTableViewCell"];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell refreshChuJingModel:self.dataArr[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210;
}

- (void)getOrderList
{
    [[SMChuJingMgr instance] getChuJingOrderList:^(NSArray *list) {
        self.dataArr = [NSArray arrayWithArray:list];
        [self.tableView reloadData];
    }];
}

#pragma mark - SMOrderListTableViewCellDelegate
- (void)clickOrderDetail:(NSString *)sid
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMOrderInquiryStoryboard" bundle:nil];
    SMChuJingOrderDetailViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMChuJingOrderDetailViewController"];
    vc.oid = sid;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
