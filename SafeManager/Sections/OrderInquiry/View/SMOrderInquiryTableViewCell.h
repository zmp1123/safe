//
//  SMOrderInquiryTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMOrderInquiryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *orderImageView;
@property (weak, nonatomic) IBOutlet UILabel *orderNameLabel;

@end
