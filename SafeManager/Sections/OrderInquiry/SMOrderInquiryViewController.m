//
//  SMOrderInquiryViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMOrderInquiryViewController.h"
#import "SMOrderInquiryTableViewCell.h"
#import "SMBaoJingOrderListViewController.h"
#import "SMChuJingOrderListViewController.h"

@interface SMOrderInquiryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMOrderInquiryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMOrderInquiryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMOrderInquiryTableViewCell"];
    
    if (indexPath.row == 0) {
        cell.orderImageView.image = [UIImage imageNamed:@"orderInquiry_baojing"];
        cell.orderNameLabel.text = @"报警缴费查询";
    }else if (indexPath.row == 1){
        cell.orderImageView.image = [UIImage imageNamed:@"orderInquiry_chujing"];
        cell.orderNameLabel.text = @"出警缴费查询";
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row == 0) {
        SMBaoJingOrderListViewController *vc = [[SMBaoJingOrderListViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        SMChuJingOrderListViewController *vc = [[SMChuJingOrderListViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
