//
//  SMBaoJingOrderDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJingOrderDetailViewController.h"

#import "SMBaoJingMgr.h"
#import "SMBaoJing.h"

@interface SMBaoJingOrderDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation SMBaoJingOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getDetailData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDetailData
{
    [[SMBaoJingMgr instance] getBaoJingOrderDetail:self.oid success:^(SMBaoJing *baojing) {
        if (baojing) {
            self.noLabel.text = [NSString stringWithFormat:@"订单编号：%@",baojing.deal_sn];
            self.startTimeLabel.text = [NSString stringWithFormat:@"生效日期：%@",baojing.start_date];
            self.endTimeLabel.text = [NSString stringWithFormat:@"截止日期：%@",baojing.end_date];
            self.priceLabel.text = [NSString stringWithFormat:@"订单金额：%@",baojing.price];
            self.contentLabel.text = [NSString stringWithFormat:@"订单内容：%@",baojing.vip_title];
            self.snLabel.text = [NSString stringWithFormat:@"设备SN：%@",baojing.device_arm_sn];
        }
    }];
}

@end
