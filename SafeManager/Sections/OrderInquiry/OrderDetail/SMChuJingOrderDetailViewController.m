//
//  SMChuJingOrderDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingOrderDetailViewController.h"
#import "SMChuJingMgr.h"
#import "SMChuJing.h"

@interface SMChuJingOrderDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMChuJingOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getDetailData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDetailData
{
    [[SMChuJingMgr instance] getChuJingOrderDetail:self.oid success:^(SMChuJing *chujing) {
        if (chujing) {
            self.addressLabel.text = [NSString stringWithFormat:@"出警地址：%@",chujing.service_addr];
            self.companyLabel.text = [NSString stringWithFormat:@"服务公司：%@",chujing.service_com_title];
            self.phoneLabel.text = [NSString stringWithFormat:@"服务电话：%@",chujing.service_com_alarm_tel];
            self.noLabel.text = [NSString stringWithFormat:@"订单编号：%@",chujing.deal_sn];
            self.startTimeLabel.text = [NSString stringWithFormat:@"生效日期：%@",chujing.start_date];
            self.endTimeLabel.text = [NSString stringWithFormat:@"截至日期：%@",chujing.end_date];
            self.priceLabel.text = [NSString stringWithFormat:@"订单金额：%@",chujing.price];
            self.snLabel.text = [NSString stringWithFormat:@"设备SN：%@",chujing.device_arm_sn];
        }
    }];
}


@end
