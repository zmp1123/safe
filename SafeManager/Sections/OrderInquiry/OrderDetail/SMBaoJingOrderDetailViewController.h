//
//  SMBaoJingOrderDetailViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMBaoJingOrderDetailViewController : SMBaseViewController

@property (nonatomic, copy) NSString *oid;

@end
