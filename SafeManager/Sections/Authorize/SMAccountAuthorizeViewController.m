//
//  SMAccountAuthorizeViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMAccountAuthorizeViewController.h"
#import "SMAccountAuthorizeTableViewCell.h"
#import "SMUserMgr.h"
#import "SMAddAuthorizeAccountView.h"
#import "SMDeviceAuthorizeViewController.h"
#import "SMAccountAuthorize.h"

@interface SMAccountAuthorizeViewController ()<UITableViewDelegate,UITableViewDataSource,SMAddAuthorizeAccountViewDelegate, SMAccountAuthorizeTableViewCellDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) SMAddAuthorizeAccountView *addView;
@property (nonatomic, strong) UIView *bgView;

@end

@implementation SMAccountAuthorizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(addAccount) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 40, 22);
    [button setImage:[UIImage imageNamed:@"add_account_normal"] forState:UIControlStateNormal];
    [button setTitleColor:rgb(184, 184, 184) forState:UIControlStateNormal];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = menuButton;
    
    [self getAuthorizeAppUserList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}

- (SMAddAuthorizeAccountView *)addView
{
    if (!_addView) {
        _addView = [[[NSBundle mainBundle] loadNibNamed:@"SMAddAuthorizeAccountView" owner:self options:nil] lastObject];
        _addView.center = self.navigationController.view.center;
        _addView.delegate = self;
    }
    return _addView;
}

- (void)clickCancel
{
    [self.bgView removeFromSuperview];
    [self.addView removeFromSuperview];
}

- (void)clickConfirm:(NSString *)no
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] addAuthorizeAppUser:no success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"添加成功" toView:self.view];
        [self getAuthorizeAppUserList];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
    
    [self clickCancel];
}

- (void)addAccount
{
    [self.navigationController.view addSubview:self.bgView];
    [self.navigationController.view addSubview:self.addView];
}

- (void)clickEdit:(NSString *)aid
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"确定要删除子账号吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[SMUserMgr instance] deleteAuthorizeAppUser:aid success:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
            [self getAuthorizeAppUserList];
        } fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
        
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMAccountAuthorizeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMAccountAuthorizeTableViewCell"];
    cell.delegate = self;
    [cell refreshModel:self.dataArr[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMAuthorizeStoryboard" bundle:nil];
    SMDeviceAuthorizeViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDeviceAuthorizeViewController"];
    vc.did = [self.dataArr[indexPath.row] aid];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getAuthorizeAppUserList
{
    [[SMUserMgr instance] getAuthorizeAppUserList:^(NSArray *array) {
        self.dataArr = [NSArray arrayWithArray:array];
        [self.tableView reloadData];
    }];
}

@end
