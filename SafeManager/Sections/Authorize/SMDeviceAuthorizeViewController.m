//
//  SMDeviceAuthorizeViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDeviceAuthorizeViewController.h"
#import "SMDeviceAuthorizeTableViewCell.h"
#import "SMUserMgr.h"

@interface SMDeviceAuthorizeViewController ()<UITableViewDelegate,UITableViewDataSource, SMDeviceAuthorizeTableViewCellDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMDeviceAuthorizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getAuthorizeDeviceList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMDeviceAuthorizeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMDeviceAuthorizeTableViewCell"];
    cell.delegate = self;
    [cell refreshModel:self.dataArr[indexPath.row]];
    return cell;
}

- (void)getAuthorizeDeviceList
{
    [[SMUserMgr instance] getAuthorizeDeviceList:self.did success:^(NSArray *array) {
        self.dataArr = [NSArray arrayWithArray:array];
        [self.tableView reloadData];
    }];
}

- (void)updateDeviceAuthorizeWithDeviceId:(NSString *)deviceId action:(BOOL)action
{
    [[SMUserMgr instance] updateAuthorizeDevice:self.did deviceId:deviceId action:action success:^{  
        
    }];
}

@end
