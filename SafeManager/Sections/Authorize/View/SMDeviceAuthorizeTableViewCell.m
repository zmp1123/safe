//
//  SMDeviceAuthorizeTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDeviceAuthorizeTableViewCell.h"
#import "SMDeviceAuthorize.h"

@interface SMDeviceAuthorizeTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *deviceSwitch;

@property (nonatomic, strong) SMDeviceAuthorize *currentModel;

@end

@implementation SMDeviceAuthorizeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.deviceSwitch addTarget:self action:@selector(updateAuthorize) forControlEvents:UIControlEventValueChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateAuthorize
{
    if ([self.delegate respondsToSelector:@selector(updateDeviceAuthorizeWithDeviceId:action:)]) {
        [self.delegate updateDeviceAuthorizeWithDeviceId:self.currentModel.DeviceId action:self.deviceSwitch.on];
    }
}

- (void)refreshModel:(SMDeviceAuthorize *)model
{
    self.currentModel = model;
    
    self.nameLabel.text = model.title;
    self.deviceSwitch.on = !model.isAuthorize;
}

@end
