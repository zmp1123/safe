//
//  SMAddAuthorizeAccountView.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMAddAuthorizeAccountViewDelegate <NSObject>

@optional
- (void)clickCancel;
- (void)clickConfirm:(NSString *)no;

@end

@interface SMAddAuthorizeAccountView : UIView

@property (weak, nonatomic) IBOutlet UITextField *noTF;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic,weak) id <SMAddAuthorizeAccountViewDelegate> delegate;

@end
