//
//  SMDeviceAuthorizeTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMDeviceAuthorize;

@protocol SMDeviceAuthorizeTableViewCellDelegate <NSObject>

@optional
- (void)updateDeviceAuthorizeWithDeviceId:(NSString *)deviceId action:(BOOL)action;

@end

@interface SMDeviceAuthorizeTableViewCell : UITableViewCell

- (void)refreshModel:(SMDeviceAuthorize *)model;

@property (nonatomic, weak) id<SMDeviceAuthorizeTableViewCellDelegate> delegate;

@end
