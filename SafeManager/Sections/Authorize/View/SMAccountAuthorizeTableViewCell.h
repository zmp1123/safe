//
//  SMAccountAuthorizeTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMAccountAuthorize;

@protocol SMAccountAuthorizeTableViewCellDelegate <NSObject>

@optional
- (void)clickEdit:(NSString *)aid;

@end

@interface SMAccountAuthorizeTableViewCell : UITableViewCell

- (void)refreshModel:(SMAccountAuthorize *)model;

@property (nonatomic, weak) id<SMAccountAuthorizeTableViewCellDelegate> delegate;

@end
