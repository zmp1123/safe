//
//  SMAddAuthorizeAccountView.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMAddAuthorizeAccountView.h"

@interface SMAddAuthorizeAccountView ()


@end

@implementation SMAddAuthorizeAccountView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 3.f;
    self.clipsToBounds = YES;
    
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (IBAction)cancelAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickCancel)]) {
        [self.delegate clickCancel];
    }
}

- (IBAction)confirmAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickConfirm:)]) {
        [self.delegate clickConfirm:self.noTF.text];
    }
}

#pragma mark - 监听键盘
- (void)keyboardWillShow:(NSNotification *)note
{
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        CGRect keyboardF = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGFloat keyboardH = keyboardF.size.height;
        self.transform = CGAffineTransformMakeTranslation(0, - (self.frame.size.height/2 - (self.center.y-keyboardH)));
    }completion:^(BOOL finished) {
        
    }];
}
- (void)keyboardWillHide:(NSNotification *)note
{
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        
    }];
}

@end
