//
//  SMAccountAuthorizeTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMAccountAuthorizeTableViewCell.h"
#import "SMAccountAuthorize.h"

@interface SMAccountAuthorizeTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *aImageView;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (nonatomic, strong) SMAccountAuthorize *currentModel;

@end

@implementation SMAccountAuthorizeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)editAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickEdit:)]) {
        [self.delegate clickEdit:self.currentModel.aid];
    }
}

- (void)refreshModel:(SMAccountAuthorize *)model
{
    self.currentModel = model;
    
    self.nicknameLabel.text = model.nickname;
    self.phoneLabel.text = model.phone;
}

@end
