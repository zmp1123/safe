//
//  SMHomeNewsTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/22.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMNews;

@interface SMHomeNewsTableViewCell : UITableViewCell

- (void)refreshModel:(SMNews *)model;

@end
