//
//  SMHomeNewsTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/22.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMHomeNewsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "SMNews.h"

@interface SMHomeNewsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

@implementation SMHomeNewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.newsImageView.clipsToBounds = YES;
    self.newsImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.newsImageView.layer.cornerRadius = 0.0f;
    self.newsImageView.layer.borderColor = [UIColor clearColor].CGColor;
    self.newsImageView.layer.borderWidth = 0.0;
    self.newsImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)refreshModel:(SMNews *)model
{
    [self.newsImageView sd_setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:nil];
    self.newsTitleLabel.text = model.title;
    self.timeLabel.text = model.createDate;
}

@end
