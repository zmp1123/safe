//
//  SMHomeViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceDetailViewController.h"
#import "SMHomeViewController.h"
#import "UIColor+Category.h"
#import "UINavigationBar+Extension.h"
#import "SMAddDeviceViewController.h"
#import "SMDevice.h"
#import "SMDeviceMgr.h"
#import "SMNews.h"
#import "SMNewsDetailViewController.h"
#import "SMDeviceTableViewCell.h"
#import "SMDeviceActionViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "SMUserMgr.h"
#import "SMUser.h"
#import "SMSosViewController.h"
#import "SMSos.h"
#import "SMHomeNewsTableViewCell.h"
#import <BaiduMapAPI_Location/BMKLocationService.h>

@interface SMHomeViewController () <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, BMKLocationServiceDelegate>

@property (strong, nonatomic) UITableView *table;
@property (strong, nonatomic) UILabel *noticeLb;
@property (strong, nonatomic) UIButton *sosBtn;
@property (strong, nonatomic) UILabel *cityLb;

@property (strong, nonatomic) NSMutableArray *deviceList;
@property (strong, nonatomic) NSMutableArray *newsList;
@property (strong, nonatomic) CLLocationManager *locationMgr;
@property (strong, nonatomic) CLLocation *currentLocation;

@property (copy, nonatomic) NSString *noticeStr;
@property (strong, nonatomic) NSArray *soslist;

@property(strong, nonatomic) BMKLocationService *locService;

@end

@implementation SMHomeViewController

- (void)viewDidLoad {
    self.deviceList = [NSMutableArray new];
    self.newsList = [NSMutableArray new];
    [super viewDidLoad];
    [self initHeader];
    [self.view addSubview:self.table];
    

    if(![CLLocationManager locationServicesEnabled]){
        //机器未开启定位功能
    }else if([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse){
        //未授予定位权限
        [self.locationMgr requestWhenInUseAuthorization];
        
    }else{
        [self.locationMgr startUpdatingLocation];
    }
    
    _locService = [[BMKLocationService alloc]init];
    [_locService startUserLocationService];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _locService.delegate = self;
    [[SMDeviceMgr instance] getArmDeviceListSuccess:^(NSArray *list) {
        self.deviceList = [[NSMutableArray alloc]initWithArray:list];
        [self.table reloadData];
    } Fail:^(NSString *msg) {
        if ([msg isEqualToString:@"列表为空！"]) {
            [self.deviceList removeAllObjects];
            [self.table reloadData];
        }
    }];
    [[SMDeviceMgr instance] getDeviceNewsSuccess:^(NSArray *list) {
        self.newsList = [[NSMutableArray alloc]initWithArray:list];
        [self.table reloadData];
    } Fail:^(NSString *msg) {
        
    }];
    [self getSosList];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _locService.delegate = nil;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *newLocation = [locations lastObject];
    //self.currentLocation = newLocation;
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    // 反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        NSString *cityName = @"未知";
        if (! error) {
            if ([placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks firstObject];
                NSString *city = placemark.locality;
                self.cityLb.text = city;
                if (! city) {
                    cityName = placemark.administrativeArea;
                }
            }
        }
//        self.cityLb.text = cityName;
    }];
    [self.locationMgr stopUpdatingLocation];
}

#pragma mark -
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    self.currentLocation  = userLocation.location;
    
    [self.locService stopUserLocationService];
}


-(void)onLocationClick{
    //todo
}

-(void)onRightBarClikc{
    SMAddDeviceViewController *addController = [SMAddDeviceViewController new];
    [self.navigationController pushViewController:addController animated:true];
}

- (void)initHeader{
    UIView *leftBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
    UIImageView *leftBarIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"homeLocation"]];
    UILabel *place = [[UILabel alloc] init];
    place.font = [UIFont systemFontOfSize:15];
    place.textColor = [UIColor whiteColor];
    place.text = @"";
    self.cityLb = place;
    [leftBar addSubview:leftBarIcon];
    [leftBar addSubview:place];
    [leftBarIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(leftBarIcon.bounds.size);
        make.leading.mas_equalTo(leftBar).offset(0);
        make.centerY.mas_equalTo(leftBar);
    }];
    
    [place mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(leftBarIcon.mas_trailing).offset(5);
        make.trailing.mas_equalTo(leftBar);
        make.centerY.mas_equalTo(leftBar);
    }];
    
    UIBarButtonItem *location = [[UIBarButtonItem alloc] initWithCustomView:leftBar];
    self.navigationItem.leftBarButtonItem = location;
    
    UIButton *addDevice = [UIButton buttonWithType:UIButtonTypeCustom];
    [addDevice setImage:[UIImage imageNamed:@"homeLight"] forState:UIControlStateNormal];
    [addDevice sizeToFit];
    [addDevice addTarget:self action:@selector(onRightBarClikc) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *light = [[UIBarButtonItem alloc] initWithCustomView:addDevice];
    self.navigationItem.rightBarButtonItem = light;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)onDeviceAtionClick:(UIView *)sender{
    NSInteger tag = sender.tag;
    
    SMDeviceDetailViewController *controller = [SMDeviceDetailViewController new];
    controller.device = self.deviceList[tag];
    [self.navigationController pushViewController:controller animated:true];

}

- (void)toSosViewController
{
    if (self.soslist.count > 0) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMSosStoryboard" bundle:nil];
        SMSosViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMSosViewController"];
        vc.sos = self.soslist[0];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0) return self.deviceList.count;
    return self.newsList.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) return 63;
    return indexPath.row == 0 ? 39 : 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 232 : 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        SMDeviceTableViewCell *c = (SMDeviceTableViewCell*)cell;
        [c fillData:self.deviceList[indexPath.row]];
    }else{
        if(self.newsList.count == 0 || indexPath.row == 0) return;
        SMHomeNewsTableViewCell *c = (SMHomeNewsTableViewCell *)cell;
        SMNews *n = self.newsList[indexPath.row -1];
//        c.textLabel.text = n.title;
        [c refreshModel:n];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 232)];
        bg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeHeaderBackground"]];
        UIView *notice = [[UIView alloc] initWithFrame:CGRectMake(0, 200, kScreenWidth, 32)];
        notice.backgroundColor = [UIColor colorWithHexColorString:@"000000" alpha:0.6];
        [notice addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSosViewController)]];
        
        UIImageView *trumpet = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homeTrumpet"]];
        UILabel *txt = [[UILabel alloc] init];
        txt.font = [UIFont systemFontOfSize:12];
        txt.textColor = [UIColor whiteColor];
        txt.text = self.noticeStr?self.noticeStr:@"暂无通知";
        txt.lineBreakMode = NSLineBreakByTruncatingTail;
        [notice addSubview:trumpet];
        [notice addSubview:txt];
        self.noticeLb = txt;
        
        [txt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(12);
            make.trailing.mas_equalTo(notice).offset(-10);
            make.leading.mas_equalTo(trumpet.mas_trailing).offset(7);
            make.centerY.mas_equalTo(notice);
        }];
        
        [trumpet mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_equalTo(notice).offset(15);
            make.centerY.mas_equalTo(notice);
            make.size.mas_equalTo(trumpet.bounds.size);
        }];
        
        [bg addSubview:notice];
        [bg addSubview:self.sosBtn];
        [self.sosBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bg);
            make.centerY.mas_equalTo(bg);
            make.size.mas_equalTo(self.sosBtn.bounds.size);
        }];
        return bg;
    }
    else{
        return [UIView new];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if(indexPath.section == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"c0"];
        if(!cell){
            cell = [[SMDeviceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"c0"];
            UIButton *btn = (UIButton*)cell.accessoryView;
            btn.tag = indexPath.row;
            [btn addTarget:self action:@selector(onDeviceAtionClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else{
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 39)];
            cell.textLabel.text = @"防盗在线";
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            cell.textLabel.textColor = [UIColor colorWithHexColorString:@"222222"];
            cell.imageView.image = [UIImage imageNamed:@"homeFangDaoOl"];
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
            line.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
            [cell addSubview:line];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"SMHomeNewsTableViewCell"];
            if(!cell){
                cell = [[SMHomeNewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SMHomeNewsTableViewCell"];
            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        SMDeviceActionViewController *ctr = [SMDeviceActionViewController new];
        ctr.device = self.deviceList[indexPath.row];
        [self.navigationController pushViewController:ctr animated:true];
    }
    else{
        if(indexPath.row == 0) return;
        SMNews *n = self.newsList[indexPath.row -1];
        SMNewsDetailViewController *conctr = [SMNewsDetailViewController new];
        conctr.newsId = n.newsId;
        [self.navigationController pushViewController:conctr animated:true];
        
    }
}
#pragma mark - sosAction
- (void)sosAction
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] sendSosWithLat:self.currentLocation.coordinate.latitude lng:self.currentLocation.coordinate.longitude success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"sos发送成功" toView:self.view];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (void)getSosList
{
    [[SMDeviceMgr instance] getSosList:^(NSArray *list) {
        self.soslist = [NSArray arrayWithArray:list];
        if (list.count == 0) {
            self.noticeLb.hidden = YES;
        }else{
            self.noticeLb.hidden = NO;
        }
        SMSos *model = self.soslist[0];
        self.noticeStr = [NSString stringWithFormat:@"通知: %@%@",model.sendTime,model.title];
    }];
}

#pragma mark - properties
- (UITableView *)table
{
    if(!_table){
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64) style:UITableViewStyleGrouped];
        _table.delegate = self;
        _table.dataSource = self;
        _table.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_table registerNib:[UINib nibWithNibName:@"SMHomeNewsTableViewCell" bundle:nil] forCellReuseIdentifier:@"SMHomeNewsTableViewCell"];
    }
    return _table;
}

-(UIButton *)sosBtn{
    if(!_sosBtn){
        _sosBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sosBtn.frame = CGRectMake(0, 0, 188, 188);
        [_sosBtn setImage:[UIImage imageNamed:@"sos"] forState:UIControlStateNormal];
        [_sosBtn addTarget:self action:@selector(sosAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sosBtn;
}


- (CLLocationManager *)locationMgr{
    if(!_locationMgr){
        _locationMgr = [[CLLocationManager alloc]init];
        _locationMgr.delegate = self;
    }
    return _locationMgr;
}
@end
