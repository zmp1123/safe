//
//  SMSosViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/13.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMSosViewController.h"
#import "SMSos.h"
#import "SMDeviceMgr.h"

#import <BaiduMapAPI_Map/BMKMapComponent.h>

@interface SMSosViewController ()<BMKMapViewDelegate>

@property (nonatomic, strong) BMKMapView* mapView;

@end

@implementation SMSosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64)];
    [self.view addSubview:self.mapView];

    self.mapView.zoomLevel = 19;
    
    if (self.sos) {
        BMKPointAnnotation *pinAnnotation = [[BMKPointAnnotation alloc] init];
        pinAnnotation.coordinate = CLLocationCoordinate2DMake(self.sos.lat, self.sos.lng);
        pinAnnotation.title = self.sos.title;
        [_mapView addAnnotation:pinAnnotation];
        
        [self.mapView setCenterCoordinate:pinAnnotation.coordinate animated:YES];
        self.title = [NSString stringWithFormat:@"%@--救援位置",self.sos.sendUser];
    }else{
        [[SMDeviceMgr instance] getSosList:^(NSArray *list) {
            if (list.count >0) {
                self.sos = list[0];
                BMKPointAnnotation *pinAnnotation = [[BMKPointAnnotation alloc] init];
                pinAnnotation.coordinate = CLLocationCoordinate2DMake(self.sos.lat, self.sos.lng);
                pinAnnotation.title = self.sos.title;
                [_mapView addAnnotation:pinAnnotation];
                
                [self.mapView setCenterCoordinate:pinAnnotation.coordinate animated:YES];
                self.title = [NSString stringWithFormat:@"%@--救援位置",self.sos.sendUser];
            }
        }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 每次添加大头针都会调用此方法  可以设置大头针的样式
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        newAnnotationView.animatesDrop = YES;// 设置该标注点动画显示
        return newAnnotationView;
    }
    return nil;
}

@end
