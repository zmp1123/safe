//
//  SMSosViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/13.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@class SMSos;

@interface SMSosViewController : SMBaseViewController

@property (nonatomic, strong) SMSos *sos;

@end
