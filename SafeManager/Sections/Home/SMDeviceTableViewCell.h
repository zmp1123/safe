//
//  SMDeviceTableViewCell.h
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMDevice;
@interface SMDeviceTableViewCell : UITableViewCell

-(void)fillData:(SMDevice *)obj;

@end
