//
//  SMDeviceTableViewCell.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceTableViewCell.h"
#import "UIColor+Category.h"
#import "SMDevice.h"
#import <Masonry.h>
#import "SystemMarco.h"

@interface SMDeviceTableViewCell ()

@property (nonatomic, strong) UILabel *topLine;

@end

@implementation SMDeviceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    UIButton *actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [actionBtn setImage:[UIImage imageNamed:@"homePan"] forState:UIControlStateNormal];
    [actionBtn sizeToFit];
    self.accessoryView = actionBtn;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
    [self addSubview:self.topLine];
    
    self.imageView.image = [UIImage new];
    self.textLabel.textColor = [UIColor colorWithHexColorString:@"222222"];
    self.textLabel.font = [UIFont systemFontOfSize:15];
    return self;
}


-(void)fillData:(SMDevice *)obj{
    if (obj.is_online != is_online_on) {
        self.imageView.image = [UIImage imageNamed:@"ic_arm_unknown"];
    }else{
        self.imageView.image = [UIImage imageNamed:obj.arm_status == arm_status_on ? @"homeBu":@"homeChe"];
    }
    self.textLabel.text = obj.title;
}

- (UILabel *)topLine{
    if(!_topLine){
        _topLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0.5, kScreenWidth, 0.5)];
        _topLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
    }
    return _topLine;
}


@end
