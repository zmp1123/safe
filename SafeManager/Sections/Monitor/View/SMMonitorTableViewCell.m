//
//  SMMonitorTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/5.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMMonitorTableViewCell.h"
#import "EZDeviceInfo.h"
#import "UIImageView+WebCache.h"
#import "EZCameraInfo.h"

@interface SMMonitorTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *dImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (nonatomic, strong) EZCameraInfo *currentModel;
@property (nonatomic, assign) NSInteger index;

@end

@implementation SMMonitorTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.dImageView.layer.cornerRadius = 15.f;
    self.dImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickEdit:)]) {
        [self.delegate clickEdit:self.currentModel];
    }
}
- (IBAction)playback:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(playback:cameraIndex:)]) {
        [self.delegate playback:self.currentModel cameraIndex:self.index];
    }
}

- (void)refreshModel:(EZCameraInfo *)model andCameraIndex:(NSInteger)cameraIndex
{
    self.currentModel = model;
    self.index = cameraIndex;
    self.nameLabel.text = model.cameraName;
}

@end
