//
//  SMMonitorTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/5.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EZDeviceInfo;
@class EZCameraInfo;

@protocol SMMonitorTableViewCellDelegate <NSObject>

@optional
- (void)clickEdit:(EZCameraInfo *)model;
- (void)playback:(EZCameraInfo *)model cameraIndex:(NSInteger)cameraIndex;

@end

@interface SMMonitorTableViewCell : UITableViewCell

- (void)refreshModel:(EZCameraInfo *)model andCameraIndex:(NSInteger)cameraIndex;

@property (nonatomic, weak) id<SMMonitorTableViewCellDelegate> delegate;

@property (nonatomic, weak) UIViewController *parentViewController;

@end
