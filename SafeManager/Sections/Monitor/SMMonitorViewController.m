//
//  SMMonitorViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMMonitorViewController.h"
#import "UIColor+Category.h"
#import "SMMonitorTableViewCell.h"
#import "SMUserMgr.h"
#import "EZOpenSDK.h"

#import "EZLivePlayViewController.h"
#import "EZSettingViewController.h"
#import "EZAddByQRCodeViewController.h"
#import "EZPlaybackViewController.h"
#import "EZCameraInfo.h"

@interface SMMonitorViewController ()<UITableViewDelegate, UITableViewDataSource, SMMonitorTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) NSMutableArray *monitorArr;

@property (nonatomic, copy) NSString *ysToken;

@end

@implementation SMMonitorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    // Do any additional setup after loading the view.

    self.tableView.tableFooterView = [[UIView alloc] init];
    self.monitorArr = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getYsAccessToken];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addDeviceAction:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"AddDevice" bundle:nil];
    EZAddByQRCodeViewController *vc = [sb instantiateViewControllerWithIdentifier:@"AddByQRCode"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.monitorArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMMonitorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMMonitorTableViewCell"];
    cell.delegate = self;
    [cell refreshModel:self.monitorArr[indexPath.row] andCameraIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EZMain" bundle:nil];
    EZLivePlayViewController *vc = [sb instantiateViewControllerWithIdentifier:@"EZLivePlayViewController"];
    vc.cameraIndex = [self.monitorArr[indexPath.row] cameraNo]-1;
    for (EZDeviceInfo *deviceInfo in self.dataArr) {
        for (EZCameraInfo *info in deviceInfo.cameraInfo) {
            if ([self.monitorArr[indexPath.row] isEqual:info]) {
                vc.deviceInfo = deviceInfo;
            }
        }
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getYsAccessToken
{
    [[SMUserMgr instance] getYsAccessToken:^(NSDictionary *response) {
        self.ysToken = response[@"accessToken"];
        [EZOpenSDK setAccessToken:self.ysToken];
        //获取设备列表接口
        [EZOpenSDK getDeviceList:0 pageSize:20 completion:^(NSArray *deviceList, NSInteger totalCount, NSError *error) {
            [self.monitorArr removeAllObjects];
            self.dataArr = [NSArray arrayWithArray:deviceList];
            for (EZDeviceInfo *deviceInfo in self.dataArr) {
                for (EZCameraInfo *info in deviceInfo.cameraInfo) {
                    [self.monitorArr addObject:info];
                }
            }
            [self.tableView reloadData];
        }];
    }];
}

#pragma mark - SMMonitorTableViewCellDelegate
- (void)clickEdit:(EZCameraInfo *)model
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EZMain" bundle:nil];
    EZSettingViewController *vc = [sb instantiateViewControllerWithIdentifier:@"EZSettingViewController"];
//    vc. = model;
    for (EZDeviceInfo *deviceInfo in self.dataArr) {
        for (EZCameraInfo *info in deviceInfo.cameraInfo) {
            if ([model isEqual:info]) {
                vc.deviceInfo = deviceInfo;
            }
        }
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)playback:(EZCameraInfo *)model cameraIndex:(NSInteger)cameraIndex
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EZMain" bundle:nil];
    EZPlaybackViewController *vc = [sb instantiateViewControllerWithIdentifier:@"EZPlaybackViewController"];
//    vc.deviceInfo = model;
    vc.cameraIndex = model.cameraNo-1;
    for (EZDeviceInfo *deviceInfo in self.dataArr) {
        for (EZCameraInfo *info in deviceInfo.cameraInfo) {
            if ([model isEqual:info]) {
                vc.deviceInfo = deviceInfo;
            }
        }
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
