//
//  EZAddByQRCodeViewController.h
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/10/28.
//  Copyright © 2015年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EZAddByQRCodeViewControllerDelegate <NSObject>

@optional
- (void)addDeviceByQRCode:(NSString *)str;

@end

@interface EZAddByQRCodeViewController : UIViewController

@property (nonatomic, weak) id<EZAddByQRCodeViewControllerDelegate> delegate;

@end
