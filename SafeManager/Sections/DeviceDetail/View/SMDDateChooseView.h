//
//  SMDDateChooseView.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMDDateChooseViewDelegate <NSObject>

@optional
- (void)dateChoose:(NSString *)date;

@end

@interface SMDDateChooseView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) id<SMDDateChooseViewDelegate> delegate;



@end
