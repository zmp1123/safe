//
//  SMDDateChooseView.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDDateChooseView.h"

@interface SMDDateChooseView ()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation SMDDateChooseView

- (void)awakeFromNib
{
    [super awakeFromNib];
}


- (IBAction)dateChooseAction:(id)sender
{
    NSDateFormatter *selectDateFormatter = [[NSDateFormatter alloc] init];
    selectDateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *date = [selectDateFormatter stringFromDate:self.datePicker.date];
    
    if ([self.delegate respondsToSelector:@selector(dateChoose:)]) {
        [self.delegate dateChoose:date];
    }
}

@end
