//
//  SMDTimeChooseView.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMDTimeChooseView : UIView

@property (weak, nonatomic) IBOutlet UIButton *startTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *endTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;


@end
