//
//  SMDTimeChooseView.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDTimeChooseView.h"

@implementation SMDTimeChooseView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSDateFormatter *selectDateFormatter = [[NSDateFormatter alloc] init];
    selectDateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *date = [selectDateFormatter stringFromDate:[NSDate date]];
    
    [self.startTimeBtn setTitle:date forState:UIControlStateNormal];
    [self.endTimeBtn setTitle:date forState:UIControlStateNormal];
}


@end
