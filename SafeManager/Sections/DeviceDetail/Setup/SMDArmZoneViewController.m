//
//  SMDArmZoneViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDArmZoneViewController.h"
#import "SMDeviceMgr.h"
#import "SMDZone.h"

#import "SMDArmZoneSetViewController.h"

@interface SMDArmZoneViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation SMDArmZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"title_add_business"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(addZone)];
    
    [self getZoneList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addZone
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"24小时防区" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setDeviceArmZoneCodeAdd:@"DCP24HourArm"];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"半布防防区" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setDeviceArmZoneCodeAdd:@"DCPHalfArm"];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"全布防防区" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setDeviceArmZoneCodeAdd:@"DCPAllArm"];
    }];
    [alert addAction:cancelAction];
    [alert addAction:action3];
    [alert addAction:action2];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    
    SMDZone *model = self.dataArr[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@号防区",model.index];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
    SMDArmZoneSetViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDArmZoneSetViewController"];
    vc.zone = self.dataArr[indexPath.row];
    vc.deviceId = self.deviceId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getZoneList
{
    [[SMDeviceMgr instance] getZoneList:self.deviceId success:^(NSArray *list) {
        self.dataArr = [NSArray arrayWithArray:list];
        [self.tableView reloadData];
    }];
}

- (void)setDeviceArmZoneCodeAdd:(NSString *)type
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmZoneCodeAdd:self.deviceId eDeviceCodeProperty:type success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
        [self getZoneList];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

@end
