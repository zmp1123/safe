//
//  SMDArmZoneSetViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDArmZoneSetViewController.h"
#import "SMDZone.h"
#import "SMAddAuthorizeAccountView.h"
#import "SMDeviceMgr.h"

@interface SMDArmZoneSetViewController ()<UITableViewDelegate,UITableViewDataSource,SMAddAuthorizeAccountViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (nonatomic, strong) SMAddAuthorizeAccountView *addView;
@property (nonatomic, strong) UIView *bgView;

@end

@implementation SMDArmZoneSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.tableView.tableFooterView = [[UIView alloc] init];
    self.deleteBtn.layer.cornerRadius = 5.f;
    self.deleteBtn.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"UITableViewCell"];
    cell.detailTextLabel.numberOfLines = 0;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.row == 0) {
        cell.textLabel.text = @"位置";
        cell.detailTextLabel.text = self.zone.position;
    }
    if (indexPath.row == 1) {
        cell.textLabel.text = @"关联监控视频";
        cell.detailTextLabel.text = self.zone.bind_device_video_sn;
    }
    if (indexPath.row == 2) {
        cell.textLabel.text = @"报警类型";
        cell.detailTextLabel.text = self.zone.alarm_type;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 0) {
        [self.navigationController.view addSubview:self.bgView];
        [self.navigationController.view addSubview:self.addView];
    }
    if (indexPath.row == 1) {
        
    }
    if (indexPath.row == 2) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"盗警" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"盗警"];
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"医疗" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"医疗"];
        }];
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"燃气" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"燃气"];
        }];
        UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"周界" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"周界"];
        }];
        UIAlertAction *action5 = [UIAlertAction actionWithTitle:@"火警" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"火警"];
        }];
        UIAlertAction *action6 = [UIAlertAction actionWithTitle:@"劫警" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"劫警"];
        }];
        UIAlertAction *action7 = [UIAlertAction actionWithTitle:@"无声" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self modifiedAlarmType:@"无声"];
        }];
        [alert addAction:cancelAction];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];
        [alert addAction:action4];
        [alert addAction:action5];
        [alert addAction:action6];
        [alert addAction:action7];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}

- (SMAddAuthorizeAccountView *)addView
{
    if (!_addView) {
        _addView = [[[NSBundle mainBundle] loadNibNamed:@"SMAddAuthorizeAccountView" owner:self options:nil] lastObject];
        _addView.center = self.navigationController.view.center;
        _addView.delegate = self;
        _addView.titleLabel.text = @"修改位置";
        _addView.noTF.placeholder = @"请输入防区位置";
    }
    return _addView;
}

- (void)clickCancel
{
    [self.bgView removeFromSuperview];
    [self.addView removeFromSuperview];
}

- (void)clickConfirm:(NSString *)no
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setZoneProperty:self.zone.zid position:no alarm_type:self.zone.zone_type bind_device_video_sn:self.zone.bind_device_video_sn success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
        self.zone.position = no;
        [self.tableView reloadData];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
    [self clickCancel];
}

- (void)modifiedAlarmType:(NSString *)type
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setZoneProperty:self.zone.zid position:self.zone.position alarm_type:type bind_device_video_sn:self.zone.bind_device_video_sn success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
        self.zone.alarm_type = type;
        [self.tableView reloadData];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}
- (IBAction)deleteAction:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmZoneCodeDel:self.deviceId zoneId:self.zone.zid success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

@end
