//
//  SMDControlListTableViewCell.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDControlListTableViewCell.h"
#import "SMDControl.h"

@interface SMDControlListTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) SMDControl *currentModel;

@end

@implementation SMDControlListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)editAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickEdit:)]) {
        [self.delegate clickEdit:self.currentModel];
    }
}

- (void)refreshModel:(SMDControl *)model
{
    self.currentModel = model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"遥控器%@",model.index];
}

@end
