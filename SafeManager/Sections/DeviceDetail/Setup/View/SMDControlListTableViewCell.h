//
//  SMDControlListTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMDControl;

@protocol SMDControlListTableViewCellDelegate <NSObject>

- (void)clickEdit:(SMDControl *)model;

@end

@interface SMDControlListTableViewCell : UITableViewCell

- (void)refreshModel:(SMDControl *)model;

@property (nonatomic, weak) id<SMDControlListTableViewCellDelegate> delegate;

@end
