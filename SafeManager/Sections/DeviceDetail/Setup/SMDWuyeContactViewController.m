//
//  SMDWuyeContactViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDWuyeContactViewController.h"
#import "SMAddAuthorizeAccountView.h"
#import "SMDeviceMgr.h"

@interface SMDWuyeContactViewController ()<SMAddAuthorizeAccountViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (nonatomic, strong) SMAddAuthorizeAccountView *addView;
@property (nonatomic, strong) UIView *bgView;

@end

@implementation SMDWuyeContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.phoneLabel.text = [NSString stringWithFormat:@"物业电话：%@",self.device.wuye_contact];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editContactAction:(id)sender
{
    [self.navigationController.view addSubview:self.bgView];
    [self.navigationController.view addSubview:self.addView];
}

- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}

- (SMAddAuthorizeAccountView *)addView
{
    if (!_addView) {
        _addView = [[[NSBundle mainBundle] loadNibNamed:@"SMAddAuthorizeAccountView" owner:self options:nil] lastObject];
        _addView.center = self.navigationController.view.center;
        _addView.delegate = self;
        _addView.titleLabel.text = @"修改物业电话";
        _addView.noTF.placeholder = @"请输入物业电话";
        _addView.noTF.keyboardType = UIKeyboardTypePhonePad;
    }
    return _addView;
}

- (void)clickCancel
{
    [self.bgView removeFromSuperview];
    [self.addView removeFromSuperview];
}

- (void)clickConfirm:(NSString *)no
{
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmInfo:self.device.device_id title:self.device.title wuye_contact:no province:self.device.province city:self.device.city district:self.device.district addr:self.device.addr success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"信息修改成功" toView:self.view];
        self.phoneLabel.text = [NSString stringWithFormat:@"物业电话：%@",no];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
    [self clickCancel];
}

@end
