//
//  SMDControlListViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDControlListViewController.h"
#import "SMDControlListTableViewCell.h"
#import "SMDeviceMgr.h"
#import "SMDControl.h"

@interface SMDControlListViewController ()<UITableViewDelegate,UITableViewDataSource,SMDControlListTableViewCellDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMDControlListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"title_add_business"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(addControl)];
    
    [self getControlList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMDControlListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMDControlListTableViewCell"];
    cell.delegate = self;
    [cell refreshModel:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)getControlList
{
    [[SMDeviceMgr instance] getControlList:self.deviceId success:^(NSArray *array) {
        self.dataArr = [NSArray arrayWithArray:array];
        [self.tableView reloadData];
    }];
}

- (void)clickEdit:(SMDControl *)model
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"确定要删除该遥控器？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[SMDeviceMgr instance] setDeviceArmRemoteControlDel:model.device_arm_id controlId:model.cid success:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
            [self getControlList];
        } fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)addControl
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"确定要添加该遥控器？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[SMDeviceMgr instance] setDeviceArmRemoteControlAdd:self.deviceId success:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"发送添加遥控器信息成功" toView:self.view];
        } fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
