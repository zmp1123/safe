//
//  SMDTimeSyncViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDTimeSyncViewController.h"
#import "SMDeviceMgr.h"

@interface SMDTimeSyncViewController ()

@property (weak, nonatomic) IBOutlet UIButton *timeSyncBtn;
@end

@implementation SMDTimeSyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.timeSyncBtn.layer.cornerRadius = 5.f;
    self.timeSyncBtn.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)timeSyncAction:(id)sender
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"确定要同步设备时间？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[SMDeviceMgr instance] timeSync:self.deviceId success:^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"同步设备时间成功" toView:self.view];
        } fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
