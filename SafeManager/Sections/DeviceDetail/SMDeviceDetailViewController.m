//
//  SMDeviceDetailViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/20.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceDetailViewController.h"
#import "UIColor+Category.h"
#import "NSDate+Extension.h"
#import <Masonry.h>

#import "SMModifyDeviceNameViewController.h"
#import "SMModifyDeviceAddressViewController.h"
#import "SMDeviceServiceViewController.h"

#import "SMDeviceMgr.h"

@interface SMDeviceDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *serializeNo;
@property (nonatomic, strong) UILabel *adress;
@property (nonatomic, strong) UILabel *serviceType;
@property (nonatomic, strong) UILabel *openDate;
@property (nonatomic, strong) UILabel *alarmOnExpire;
@property (nonatomic, strong) UILabel *alarmOffExpire;
@property (nonatomic, strong) UILabel *deviceType;
@property (nonatomic, strong) UILabel *deviceSoftwareVersion;

@end

@implementation SMDeviceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.items = @[@"设备名:",@"序列号:", @"设备地址:", @"服务模式:", @"设备开通时间:"
                   ,@"报警服务到期时间:", @"出警服务到期时间:", @"服务续费", @"设备型号:", @"设备软件版本:"];
    self.title = @"设备详情";
    [self.view addSubview:self.table];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SMDeviceMgr instance] getDeviceInfo:self.device.device_id Success:^(SMDevice *info) {
        self.device = info;
        [self.table reloadData];
    } Fail:^(NSString *msg) {
        
    }];
}

- (void)deviceArmUnbind
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] deviceArmUnbind:self.device.device_id success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"解绑成功" toView:self.view];
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(back) userInfo:nil repeats:NO];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 62;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.backgroundColor = [UIColor colorWithHexColorString:@"5997db"];
    [btn setTitle:@"解除绑定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 5;
    [btn addTarget:self action:@selector(deviceArmUnbind) forControlEvents:UIControlEventTouchUpInside];
    
    
    [view addSubview:btn];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(view);
        make.height.mas_equalTo(40);
        make.trailing.mas_equalTo(view).offset(-20);
        make.leading.mas_equalTo(view).offset(20);
    }];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, kScreenWidth, 0.5)];
    bottomLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
    return bottomLine;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *reuseId = @"r0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textColor = [UIColor colorWithHexColorString:@"898989"];
        UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, kScreenWidth, 0.5)];
        bottomLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
        [cell addSubview:bottomLine];
    }
    //服务续费
    if(indexPath.row == 7 || indexPath.row == 0 || indexPath.row == 2){
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *menu = self.items[indexPath.row];
    NSString *content = @"";
    switch (indexPath.row) {
        case 0:
            content = self.device.title;
            break;
        case 1:
            content = self.device.device_sn;
            break;
        case 2:
            content = [NSString stringWithFormat:@"%@%@%@%@",self.device.province,self.device.city,self.device.district,self.device.addr];
            break;
        case 3:
            content = self.device.service_status == service_status_on ? @"出警":@"不出警";
            break;
        case 4:
            content = [self.device.last_bind_date dateStringWithFmt:@"yyyy-MM-dd"]? [self.device.last_bind_date dateStringWithFmt:@"yyyy-MM-dd"]:@"";
            break;
        case 5:
        case 6:
            content = [self.device.service_end_date dateStringWithFmt:@"yyyy-MM-dd"]?[self.device.service_end_date dateStringWithFmt:@"yyyy-MM-dd"]:@"";
            break;
        case 8:
            content = self.device.device_arm_type;
            break;
        case 9:
            content = self.device.firmware_version;
            break;
        default:
            break;
    }
    NSString *final = [NSString stringWithFormat:@"%@  %@", menu, content];
    NSMutableAttributedString *t = [[NSMutableAttributedString alloc]initWithString:final];
    [t addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, final.length)];
    [t addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"222222"] range:NSMakeRange(0, menu.length)];
    [t addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"898989"] range:NSMakeRange(menu.length, final.length - menu.length)];
    cell.textLabel.attributedText = t;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.table deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row == 0) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
        SMModifyDeviceNameViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMModifyDeviceNameViewController"];
        vc.device = self.device;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 2) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
        SMModifyDeviceAddressViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMModifyDeviceAddressViewController"];
        vc.device = self.device;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 7) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
        SMDeviceServiceViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDeviceServiceViewController"];
        vc.device = self.device;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _table.dataSource = self;
        _table.delegate = self;
        _table.tableFooterView = [UIView new];
        _table.backgroundColor = [UIColor colorWithHexColorString:@"eef0f4"];
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}





@end
