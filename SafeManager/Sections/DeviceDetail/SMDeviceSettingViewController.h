//
//  SMDeviceSettingViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/28.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"

@class SMDevice;

@interface SMDeviceSettingViewController : SMBaseViewController

@property (nonatomic, copy) NSString *deviceId;
@property (nonatomic, strong) SMDevice *device;

@end
