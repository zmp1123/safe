//
//  SMModifyDeviceNameViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModifyDeviceNameViewController.h"
#import "SMDeviceMgr.h"

@interface SMModifyDeviceNameViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UILabel *limitLabel;

@end

@implementation SMModifyDeviceNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameTF.text = self.device.title;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    
    [self.nameTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.limitLabel.text = [NSString stringWithFormat:@"%ld",26-self.nameTF.text.length];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) stopEditing
{
    [self.view endEditing:YES];
}

- (void)close
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)save
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmInfo:self.device.device_id title:self.nameTF.text wuye_contact:self.device.wuye_contact province:self.device.province city:self.device.city district:self.device.district addr:self.device.addr success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(close) userInfo:nil repeats:NO];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)textField
{
    if ([textField isEqual:self.nameTF]) {
        self.limitLabel.text = [NSString stringWithFormat:@"%ld",26-self.nameTF.text.length];
        if (textField.text.length > 26) {
            textField.text = [textField.text substringToIndex:26];
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.nameTF) {
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if (existedLength - selectedLength + replaceLength > 26) {
            return NO;
        }
    }
    
    return YES;
}

@end
