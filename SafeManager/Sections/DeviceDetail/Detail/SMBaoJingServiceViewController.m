//
//  SMBaoJingServiceViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJingServiceViewController.h"
#import "SMBaoJingMgr.h"
#import "SMBaoJingService.h"
#import "SMBaoJingServiceDetailViewController.h"

@interface SMBaoJingServiceViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMBaoJingServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self getVipTypeList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"UITableViewCell"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    SMBaoJingService *model = self.dataArr[indexPath.row];
    
    cell.imageView.image = [UIImage imageNamed:@"service_choose"];
    cell.textLabel.text = model.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@元",model.price];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMBaoJingServiceDetailViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMBaoJingServiceDetailViewController"];
    vc.device = self.device;
    vc.bjService = self.dataArr[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getVipTypeList
{
    [[SMBaoJingMgr instance] getVipTypeList:^(NSArray *list) {
        self.dataArr = [NSArray arrayWithArray:list];
        [self.tableView reloadData];
    }];
}

@end
