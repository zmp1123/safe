//
//  SMChuJingServiceDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingServiceDetailViewController.h"
#import "SMChuJingMgr.h"
#import "SMChuJingService.h"
#import "SMDevice.h"
#import "SMProtocolViewController.h"
#import "SMPayWayViewController.h"
#import "SMChuJingServiceOrder.h"

@interface SMChuJingServiceDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addrLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@end

@implementation SMChuJingServiceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self getServiceComDetail];
    
    self.nameLabel.text = self.chujingService.title;
    self.priceLabel.text = [NSString stringWithFormat:@"%@元",self.chujingService.price];
    self.addrLabel.text = [NSString stringWithFormat:@"%@%@%@%@",self.device.province,self.device.city,self.device.district,self.device.addr];
    self.timeLabel.text = [NSString stringWithFormat:@"%@年",self.chujingService.duration];
    self.snLabel.text = self.device.device_sn;
    
    self.buyBtn.layer.cornerRadius = 5.f;
    self.buyBtn.clipsToBounds = YES;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)agreeAction:(id)sender
{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}
- (IBAction)protocolAction:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMProtocolViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMProtocolViewController"];
    vc.title = @"出警条款-官家保";
    vc.urlStr = @"http://139.196.234.54/Home?c=Article&a=serviceRules";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)buyAction:(id)sender
{
    if (!self.agreeBtn.selected) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先同意服务相关条款" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [[SMChuJingMgr instance] serviceDealCreate:self.device.device_id serviceComId:self.chujingService.sid serviceAddr:self.addrLabel.text success:^(SMChuJingServiceOrder *order) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMPayStoryboard" bundle:nil];
        SMPayWayViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMPayWayViewController"];
        vc.dealSn = order.dealSn;
        vc.dealId = order.dealId;
        vc.price = order.price;
        vc.dealTitle = order.service_com_title;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}



- (void)getServiceComDetail
{
    [[SMChuJingMgr instance] getServiceComDetail:self.chujingService.sid success:^(SMChuJingService *model) {
        
    }];
}

@end
