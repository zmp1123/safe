//
//  SMDeviceServiceViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDeviceServiceViewController.h"
#import "SMBaoJingServiceViewController.h"
#import "SMChuJingMgr.h"
#import "SMMChooseServiceComTypeView.h"
#import "SMChuJingServiceViewController.h"

@interface SMDeviceServiceViewController ()<UITableViewDelegate,UITableViewDataSource,SMMChooseServiceComTypeViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) SMMChooseServiceComTypeView *chooseServiceComView;

@end

@implementation SMDeviceServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.row == 0) {
        cell.textLabel.text = @"报警服务";
        cell.imageView.image = [UIImage imageNamed:@"ic_alarm_call"];
    }
    if (indexPath.row == 1) {
        cell.textLabel.text = @"出警服务";
        cell.imageView.image = [UIImage imageNamed:@"ic_alarm_rescue"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 0) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
        SMBaoJingServiceViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMBaoJingServiceViewController"];
        vc.device = self.device;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (indexPath.row == 1) {
        [self getServiceComList];
    }
}

- (void)getServiceComList
{
    [[SMChuJingMgr instance] getServiceComTypeList:^(NSArray *list) {
        [self.navigationController.view addSubview:self.bgView];
        [self.navigationController.view addSubview:self.chooseServiceComView];
        
        self.chooseServiceComView.dataArr = [NSArray arrayWithArray:list];
        [self.chooseServiceComView.pickView reloadAllComponents];
        self.bgView.alpha = 0;
        [UIView animateWithDuration:0.25 animations:^{
            self.bgView.alpha = 0.6;
            self.chooseServiceComView.transform = CGAffineTransformMakeTranslation(0, -self.chooseServiceComView.frame.size.height);
        } completion:^(BOOL finished) {
            
        }];
    }];
}

#pragma mark -SMMChooseServiceComTypeViewDelegate
- (void)chooseServiceComType:(NSString *)tid
{
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
        self.chooseServiceComView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self.bgView removeFromSuperview];
        [self.chooseServiceComView removeFromSuperview];
    }];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMChuJingServiceViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMChuJingServiceViewController"];
    vc.tid = tid;
    vc.device = self.device;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UI
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}

- (SMMChooseServiceComTypeView *)chooseServiceComView
{
    if (!_chooseServiceComView) {
        _chooseServiceComView = [[[NSBundle mainBundle] loadNibNamed:@"SMMChooseServiceComTypeView" owner:self options:nil] lastObject];
        _chooseServiceComView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, 250);
        _chooseServiceComView.delegate = self;
    }
    return _chooseServiceComView;
}

@end
