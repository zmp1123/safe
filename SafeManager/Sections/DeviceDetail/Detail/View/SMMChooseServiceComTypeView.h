//
//  SMMChooseServiceComTypeView.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMServiceComTypeModel;

@protocol SMMChooseServiceComTypeViewDelegate <NSObject>

@optional
- (void)chooseServiceComType:(NSString *)tid;

@end

@interface SMMChooseServiceComTypeView : UIView
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, weak) id<SMMChooseServiceComTypeViewDelegate> delegate;

@end
