//
//  SMMChoosePlaceView.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMMChoosePlaceView.h"

@interface SMMChoosePlaceView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, strong) NSDictionary *areasDict;
@property (nonatomic, strong) NSString *selectedProvince;
@property (nonatomic, strong) NSString *selectedCity;
@property (nonatomic, strong) NSString *selectedArea;
@property (nonatomic, assign) NSInteger selectedIndex_province;
@property (nonatomic, assign) NSInteger selectedIndex_city;

@end

@implementation SMMChoosePlaceView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.pickView.delegate = self;
    self.pickView.dataSource = self;
    
    [self getAreaData];
    
    self.selectedIndex_province = 0;
    NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
    self.selectedProvince = [dic allKeys][0];
    
    self.selectedIndex_city = 0;
    NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
    NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
    self.selectedCity = [d2 allKeys][0];
    self.selectedArea = [d2 objectForKey:self.selectedCity][0];
    
    [self.pickView reloadComponent:1];
    [self.pickView reloadComponent:2];
    [self.pickView selectRow:0 inComponent:1 animated:NO];
    [self.pickView selectRow:0 inComponent:2 animated:NO];
}

#pragma mark - 获取省市区数据
- (void)getAreaData
{
    NSString* urlPlistPath = [[NSBundle mainBundle] pathForResource:@"Area" ofType:@"plist"];
    self.areasDict = [NSDictionary dictionaryWithContentsOfFile:urlPlistPath];
    
}
#pragma mark -  UIPickerViewDelegate, UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [[self.areasDict allKeys] count];
    }else if (component == 1){
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        return [[d1 allKeys] count];
        
    }else if (component == 2){
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        
        NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
        NSArray *arr = [d2 objectForKey:self.selectedCity];
        return arr.count;
    }
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",row]];
        return [dic allKeys][0];
    }else if (component == 1) {
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",row]];
        return [d2 allKeys][0];
    }else if (component == 2){
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        
        NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
        NSArray *arr = [d2 objectForKey:self.selectedCity];
        return arr[row];
    }
    return @"haha";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        self.selectedProvince = [self pickerView:pickerView titleForRow:row forComponent:component];
        self.selectedIndex_province = row;
        self.selectedIndex_city = 0;
        
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
        self.selectedCity = [d2 allKeys][0];
        
        self.selectedArea = [d2 objectForKey:self.selectedCity][0];
        
        [self.pickView reloadComponent:1];
        [self.pickView reloadComponent:2];
        [self.pickView selectRow:0 inComponent:1 animated:NO];
        [self.pickView selectRow:0 inComponent:2 animated:NO];
    }else if (component == 1){
        self.selectedCity = [self pickerView:pickerView titleForRow:row forComponent:component];
        self.selectedIndex_city = row;
        
        NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
        NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
        NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
        self.selectedArea = [d2 objectForKey:self.selectedCity][0];
        
        [self.pickView reloadComponent:2];
        [self.pickView selectRow:0 inComponent:2 animated:NO];
    }else if (component == 2){
        self.selectedArea = [self pickerView:pickerView titleForRow:row forComponent:component];;
    }
    
    if ([self.delegate respondsToSelector:@selector(chooseProvice:city:district:)]) {
        [self.delegate chooseProvice:self.selectedProvince city:self.selectedCity district:self.selectedArea];
    }
}

@end
