//
//  SMMChoosePlaceView.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMMChoosePlaceViewDelegate <NSObject>

@optional
- (void)chooseProvice:(NSString *)provice city:(NSString *)city district:(NSString *)district;

@end

@interface SMMChoosePlaceView : UIView

@property (weak, nonatomic) IBOutlet UIPickerView *pickView;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (nonatomic, weak) id<SMMChoosePlaceViewDelegate> delegate;

@end
