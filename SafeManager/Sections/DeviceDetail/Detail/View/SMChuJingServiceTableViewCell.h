//
//  SMChuJingServiceTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMChuJingServiceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
