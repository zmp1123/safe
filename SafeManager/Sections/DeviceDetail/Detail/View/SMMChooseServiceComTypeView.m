//
//  SMMChooseServiceComTypeView.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMMChooseServiceComTypeView.h"
#import "SMServiceComTypeModel.h"

@interface SMMChooseServiceComTypeView ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, strong) NSString *selectedProvince;
@property (nonatomic, strong) NSString *selectedCity;
@property (nonatomic, strong) NSString *selectedArea;
@property (nonatomic, assign) NSInteger selectedIndex_0;
@property (nonatomic, assign) NSInteger selectedIndex_1;

@end

@implementation SMMChooseServiceComTypeView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    self.pickView.delegate = self;
//    self.pickView.dataSource = self;
//    
//    [self getAreaData];
//    
//    self.selectedIndex_province = 0;
//    NSDictionary *dic = [self.areasDict objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_province]];
//    self.selectedProvince = [dic allKeys][0];
//    
//    self.selectedIndex_city = 0;
//    NSDictionary *d1 = [dic objectForKey:self.selectedProvince];
//    NSDictionary *d2 = [d1 objectForKey:[NSString stringWithFormat:@"%ld",self.selectedIndex_city]];
//    self.selectedCity = [d2 allKeys][0];
//    self.selectedArea = [d2 objectForKey:self.selectedCity][0];
//    
//    [self.pickView reloadComponent:1];
//    [self.pickView reloadComponent:2];
//    [self.pickView selectRow:0 inComponent:1 animated:NO];
//    [self.pickView selectRow:0 inComponent:2 animated:NO];
}

#pragma mark -  UIPickerViewDelegate, UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0) {
        return self.dataArr.count;
    }else if (component == 1){
        
        NSArray *arr1 = [self.dataArr[self.selectedIndex_0] subList];
        
        return [arr1 count];
        
    }else if (component == 2){
        
        NSArray *arr1 = [self.dataArr[self.selectedIndex_0] subList];
        NSArray *arr2 = [arr1[self.selectedIndex_1] subList];

        return [arr2 count];
    }
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        SMServiceComTypeModel *model = self.dataArr[row];
        return model.title;
    }else if (component == 1) {
        NSArray *arr1 = [self.dataArr[self.selectedIndex_0] subList];
        SMServiceComTypeModel *model1 = arr1[row];
        return model1.title;
    }else if (component == 2){
        NSArray *arr1 = [self.dataArr[self.selectedIndex_0] subList];
        NSArray *arr2 = [arr1[self.selectedIndex_1] subList];
        SMServiceComTypeModel *model2 = arr2[row];
        return model2.title;
    }
    return @"haha";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        self.selectedIndex_0 = row;
        self.selectedIndex_1 = 0;
        
        [self.pickView reloadComponent:1];
        [self.pickView reloadComponent:2];
        [self.pickView selectRow:0 inComponent:1 animated:NO];
        [self.pickView selectRow:0 inComponent:2 animated:NO];
    }else if (component == 1){
        self.selectedIndex_1 = row;
        
        [self.pickView reloadComponent:2];
        [self.pickView selectRow:0 inComponent:2 animated:NO];
    }else if (component == 2){
        
    }
}
- (IBAction)confirmAction:(id)sender
{
    NSInteger row0 = [self.pickView selectedRowInComponent:0];
    NSInteger row1 = [self.pickView selectedRowInComponent:1];
    NSInteger row3 = [self.pickView selectedRowInComponent:2];
    
    NSArray *arr1 = [self.dataArr[row0] subList];
    NSArray *arr2 = [arr1[row1] subList];
    SMServiceComTypeModel *model2 = arr2[row3];
    
    if ([self.delegate respondsToSelector:@selector(chooseServiceComType:)]) {
        [self.delegate chooseServiceComType:model2.tid];
    }
}

@end
