//
//  SMChuJingServiceViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingServiceViewController.h"
#import "SMChuJingMgr.h"
#import "SMChuJingServiceTableViewCell.h"
#import "SMChuJingService.h"
#import "SMChuJingServiceDetailViewController.h"

@interface SMChuJingServiceViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMChuJingServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self getServiceComList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMChuJingService *model = self.dataArr[indexPath.row];
    
    SMChuJingServiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMChuJingServiceTableViewCell"];
    cell.sNameLabel.text = model.title;
    cell.phoneLabel.text = model.alarm_tel;
    cell.priceLabel.text = [NSString stringWithFormat:@"%@元/年",model.price];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMChuJingServiceDetailViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMChuJingServiceDetailViewController"];
    vc.device = self.device;
    vc.chujingService = self.dataArr[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getServiceComList
{
    [[SMChuJingMgr instance] getServiceComList:self.tid success:^(NSArray *list) {
        self.dataArr = [NSArray arrayWithArray:list];
        [self.tableView reloadData];
    }];
}


@end
