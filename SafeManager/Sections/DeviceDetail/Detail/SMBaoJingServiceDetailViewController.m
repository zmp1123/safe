//
//  SMBaoJingServiceDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJingServiceDetailViewController.h"
#import "SMBaoJingService.h"
#import "SMDevice.h"
#import "SMProtocolViewController.h"
#import "SMBaoJingMgr.h"
#import "SMPayWayViewController.h"
#import "SMBaoJingServiceOrder.h"

@interface SMBaoJingServiceDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;

@end

@implementation SMBaoJingServiceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.buyBtn.layer.cornerRadius = 5.f;
    self.buyBtn.clipsToBounds = YES;
    
    self.titleLabel.text = self.bjService.title;
    self.priceLabel.text = [NSString stringWithFormat:@"%@元",self.bjService.price];
    self.timeLabel.text = [NSString stringWithFormat:@"%@年",self.bjService.duration];
    self.snLabel.text = self.device.device_sn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)agreeAction:(id)sender
{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}
- (IBAction)protocolAction:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMProtocolViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMProtocolViewController"];
    vc.title = @"报警条款-官家保";
    vc.urlStr = @"http://139.196.234.54/Home?c=Article&a=alarmRules";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)buyAction:(id)sender
{
    if (!self.agreeBtn.selected) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先同意服务相关条款" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [[SMBaoJingMgr instance] vipDealCreate:self.bjService.sid deviceId:self.device.device_id success:^(SMBaoJingServiceOrder *order) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMPayStoryboard" bundle:nil];
        SMPayWayViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMPayWayViewController"];
        vc.dealSn = order.dealSn;
        vc.dealId = order.dealId;
        vc.price = order.price;
        vc.dealTitle = order.vip_title;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

@end
