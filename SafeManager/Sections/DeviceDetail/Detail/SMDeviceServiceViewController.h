//
//  SMDeviceServiceViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@class SMDevice;

@interface SMDeviceServiceViewController : SMBaseViewController

@property (nonatomic, strong) SMDevice *device;

@end
