//
//  SMModifyDeviceAddressViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModifyDeviceAddressViewController.h"
#import "SMDevice.h"
#import "SMDeviceMgr.h"

#import "SMMChoosePlaceView.h"

@interface SMModifyDeviceAddressViewController ()<SMMChoosePlaceViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *zoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *addrTF;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) SMMChoosePlaceView *choosePlaceView;

@end

@implementation SMModifyDeviceAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.zoneLabel.text = [NSString stringWithFormat:@"%@%@%@",self.device.province,self.device.city,self.device.district];
    self.addrTF.text = self.device.addr;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    
    [self.addrTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) stopEditing
{
    [self.view endEditing:YES];
}

- (void)close
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)save
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmInfo:self.device.device_id title:self.device.title wuye_contact:self.device.wuye_contact province:self.device.province city:self.device.city district:self.device.district addr:self.addrTF.text success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(close) userInfo:nil repeats:NO];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (IBAction)chooseZoneAction:(id)sender
{
    [self.navigationController.view addSubview:self.bgView];
    [self.navigationController.view addSubview:self.choosePlaceView];
    
    self.bgView.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0.6;
        self.choosePlaceView.transform = CGAffineTransformMakeTranslation(0, -self.choosePlaceView.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)textField
{
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

#pragma mark - SMMChoosePlaceViewDelegate
- (void)chooseProvice:(NSString *)provice city:(NSString *)city district:(NSString *)district
{
    self.zoneLabel.text = [NSString stringWithFormat:@"%@%@%@",provice,city,district];
    self.device.province = provice;
    self.device.city = city;
    self.device.district = district;
}

- (void)confirmChoose
{
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
        self.choosePlaceView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self.bgView removeFromSuperview];
        [self.choosePlaceView removeFromSuperview];
    }];
}

#pragma mark - UI
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}

- (SMMChoosePlaceView *)choosePlaceView
{
    if (!_choosePlaceView) {
        _choosePlaceView = [[[NSBundle mainBundle] loadNibNamed:@"SMMChoosePlaceView" owner:self options:nil] lastObject];
        _choosePlaceView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, 250);
        _choosePlaceView.delegate = self;
        [_choosePlaceView.confirmBtn addTarget:self action:@selector(confirmChoose) forControlEvents:UIControlEventTouchUpInside];
    }
    return _choosePlaceView;
}

@end
