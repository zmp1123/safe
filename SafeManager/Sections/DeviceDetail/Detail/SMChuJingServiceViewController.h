//
//  SMChuJingServiceViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@class SMDevice;
@interface SMChuJingServiceViewController : SMBaseViewController

@property (nonatomic, copy) NSString *tid;
@property (nonatomic, strong) SMDevice *device;
@end
