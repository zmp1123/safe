//
//  SMDeviceDetailViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/20.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"
#import "SMDevice.h"

@interface SMDeviceDetailViewController : SMBaseViewController

@property (nonatomic, strong) SMDevice *device;

@end
