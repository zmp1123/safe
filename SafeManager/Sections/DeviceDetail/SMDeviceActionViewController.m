//
//  SMDeviceActionViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/27.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceActionViewController.h"
#import "UIColor+Category.h"
#import <Masonry.h>
#import "SMDeviceSettingViewController.h"
#import "SMDevice.h"

#import "SMDeviceMgr.h"
#import "SMDeviceRecord.h"
#import "SMDTimeChooseView.h"
#import "SMDDateChooseView.h"

#import "MJRefreshNormalHeader.h"

@interface SMDeviceActionViewController ()<UITableViewDelegate, UITableViewDataSource,SMDDateChooseViewDelegate>

@property (nonatomic, strong) UITableView *logTable;
@property (nonatomic, strong) UIButton *callBtn;
@property (nonatomic, strong) UIButton *bufangBtn;
@property (nonatomic, strong) UIButton *chefangBtn;
@property (nonatomic, strong) UIButton *banbufangBtn;
@property (nonatomic, strong) UIImageView *isOnline;
@property (nonatomic, strong) UIImageView *netWork;
@property (nonatomic, strong) UIImageView *battery;

@property (nonatomic, strong) UILabel *isOnlineDesc;
@property (nonatomic, strong) UILabel *netWorkDesc;
@property (nonatomic, strong) UILabel *batteryDesc;
@property (nonatomic, strong) UILabel *bufangDesc;
@property (nonatomic, strong) UILabel *chefangDesc;
@property (nonatomic, strong) UILabel *banbufangDesc;

@property (nonatomic, strong) UIView *footer;
@property (nonatomic, strong) SMDTimeChooseView *timeView;
@property (nonatomic, strong) SMDDateChooseView *dateChooseView;
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIButton *settingBtn;
@property (nonatomic, strong) UIButton *msgBtn;

@property (nonatomic, strong) NSMutableArray *logMsgs;

@property (strong, nonatomic) MJRefreshNormalHeader* mjRefreshHeader;
@property (assign, nonatomic) SMDeviceMessageType currentType;
@property (assign, nonatomic) BOOL isChooseTime;
@property (assign, nonatomic) NSInteger pageIndex;

@end

@implementation SMDeviceActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"管家保";
    self.pageIndex = 1;
    self.logMsgs = [NSMutableArray array];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
    UIBarButtonItem *settingBar = [[UIBarButtonItem alloc]initWithCustomView:self.settingBtn];
    UIBarButtonItem *msgBar = [[UIBarButtonItem alloc]initWithCustomView:self.msgBtn];
    self.navigationItem.rightBarButtonItems = @[msgBar, settingBar];
    
    [self initMjRefreshHeader];
    
    if (self.device) {
        self.title = self.device.title;
        [self.view addSubview:self.logTable];
        [self.view addSubview:self.footer];
        [self getDeviceLastRecord:SMDeviceMessageType_All];
    }else if (self.deviceArmId){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[SMDeviceMgr instance] getDeviceInfo:self.deviceArmId Success:^(SMDevice *info) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.device = info;
            self.title = self.device.title;
            [self.view addSubview:self.logTable];
            [self.view addSubview:self.footer];
            [self getDeviceLastRecord:SMDeviceMessageType_All];
        } Fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
    }
}

- (void)initMjRefreshHeader
{
    _mjRefreshHeader = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex ++;
        if (self.isChooseTime) {
            [self getDeviceLastRecord:SMDeviceMessageType_All startDate:self.timeView.startTimeBtn.titleLabel.text endDate:self.timeView.endTimeBtn.titleLabel.text];
        }else{
            [self getDeviceLastRecord:self.currentType];
        }
    }];
    [_mjRefreshHeader setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [_mjRefreshHeader setTitle:@"释放刷新" forState:MJRefreshStatePulling];
    [_mjRefreshHeader setTitle:@"正在更新" forState:MJRefreshStateRefreshing];
//    _mjRefreshHeader.stateLabel.textColor = [UIColor whiteColor];
    
    _mjRefreshHeader.lastUpdatedTimeLabel.hidden = true;
    self.logTable.mj_header = _mjRefreshHeader;
}


#pragma mark - Table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.logMsgs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"c0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SMDeviceRecord *model = self.logMsgs[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@",model.op,model.type_name,model.log_time];
}

#pragma mark - navigationBar Action

- (void)tosettingViewController
{
    SMDeviceSettingViewController *vc = [[SMDeviceSettingViewController alloc] init];
    vc.deviceId = self.device.device_id;
    vc.device = self.device;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)chooseMessageType
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *AlarmAction = [UIAlertAction actionWithTitle:@"查询报警记录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.timeView removeFromSuperview];
        self.currentType = SMDeviceMessageType_Alarm;
        self.isChooseTime = NO;
        self.pageIndex = 1;
        [self getDeviceLastRecord:SMDeviceMessageType_Alarm];
    }];
    UIAlertAction *ArmAction = [UIAlertAction actionWithTitle:@"查询布撤防记录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.timeView removeFromSuperview];
        self.currentType = SMDeviceMessageType_Arm;
        self.isChooseTime = NO;
        self.pageIndex = 1;
        [self getDeviceLastRecord:SMDeviceMessageType_Arm];
    }];
    UIAlertAction *TimeAction = [UIAlertAction actionWithTitle:@"按时间查询" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.view addSubview:self.timeView];
        self.isChooseTime = YES;
        //self.logMsgs = [NSArray array];
        [self.logTable reloadData];
        self.pageIndex = 1;
        self.currentType = SMDeviceMessageType_All;
    }];
    UIAlertAction *AllAction = [UIAlertAction actionWithTitle:@"常规查询" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.timeView removeFromSuperview];
        self.isChooseTime = NO;
        self.currentType = SMDeviceMessageType_All;
        self.pageIndex = 1;
        [self getDeviceLastRecord:SMDeviceMessageType_All];
    }];
    [alert addAction:cancelAction];
    [alert addAction:AllAction];
    [alert addAction:TimeAction];
    [alert addAction:ArmAction];
    [alert addAction:AlarmAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Action
- (void)showChooseDateView:(UIButton*)btn
{
    [self.navigationController.view addSubview:self.bgView];
    [self.navigationController.view addSubview:self.dateChooseView];
    self.dateChooseView.transform = CGAffineTransformMakeTranslation(0, self.dateChooseView.frame.size.height);
    self.dateChooseView.titleLabel.text = (btn.tag == 1)?@"选择查询开始时间":@"选择查询结束时间";
    self.bgView.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0.6;
        self.dateChooseView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)searchRecordWithTime
{
    [self.timeView removeFromSuperview];
    [self getDeviceLastRecord:SMDeviceMessageType_All startDate:self.timeView.startTimeBtn.titleLabel.text endDate:self.timeView.endTimeBtn.titleLabel.text];
}

- (void)callBtnAction
{
    if (self.device.wuye_contact.length == 0) {
        [MBProgressHUD showError:@"请先设置物业电话！"];
        return;
    }
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSString *tel=[NSString stringWithFormat:@"tel://%@",self.device.wuye_contact];
    NSURL *telURL =[NSURL URLWithString:tel];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
}

#pragma mark - Data
- (void)getDeviceLastRecord:(SMDeviceMessageType)type
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] getDeviceLastRecord:self.device.device_id page:self.pageIndex type:type startDate:nil endDate:nil success:^(NSArray *list) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.pageIndex == 1) {
            [self.logMsgs removeAllObjects];
        }
        NSArray *tmp = [[self.logMsgs reverseObjectEnumerator] allObjects];
        NSMutableArray *tmpArr = [NSMutableArray array];
        [tmpArr addObjectsFromArray:tmp];
        [tmpArr addObjectsFromArray:list];
        self.logMsgs = [NSMutableArray arrayWithArray:[[tmpArr reverseObjectEnumerator] allObjects]];
        [self.logTable reloadData];
        if (self.pageIndex == 1) {
            [self.logTable setContentOffset:CGPointMake(0, self.logTable.contentSize.height -self.logTable.bounds.size.height) animated:NO];
        }
        [_mjRefreshHeader endRefreshing];
    }fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
        [_mjRefreshHeader endRefreshing];
    }];
}
- (void)getDeviceLastRecord:(SMDeviceMessageType)type startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] getDeviceLastRecord:self.device.device_id page:self.pageIndex type:type startDate:startDate endDate:endDate success:^(NSArray *list) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.pageIndex == 1) {
            [self.logMsgs removeAllObjects];
        }
        NSArray *tmp = [[self.logMsgs reverseObjectEnumerator] allObjects];
        NSMutableArray *tmpArr = [NSMutableArray array];
        [tmpArr addObjectsFromArray:tmp];
        [tmpArr addObjectsFromArray:list];
        self.logMsgs = [NSMutableArray arrayWithArray:[[tmpArr reverseObjectEnumerator] allObjects]];
        [self.logTable reloadData];
        if (self.pageIndex == 1) {
            [self.logTable setContentOffset:CGPointMake(0, self.logTable.contentSize.height -self.logTable.bounds.size.height) animated:YES];
        }
        [_mjRefreshHeader endRefreshing];
    }fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
        [_mjRefreshHeader endRefreshing];
    }];
}
#pragma mark - SMDDateChooseViewDelegate
- (void)dateChoose:(NSString *)date
{
    if ([self.dateChooseView.titleLabel.text isEqualToString:@"选择查询开始时间"]) {
        [self.timeView.startTimeBtn setTitle:date forState:UIControlStateNormal];
    }else{
        [self.timeView.endTimeBtn setTitle:date forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.bgView.alpha = 0;
        self.dateChooseView.transform = CGAffineTransformMakeTranslation(0, self.dateChooseView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.bgView removeFromSuperview];
        [self.dateChooseView removeFromSuperview];
    }];
}

#pragma mark - Action
- (void)setDeviceArmDOTArm
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmDOTArm:self.device.device_id success:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"发布布防消息成功" toView:self.view];
    }fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}
- (void)setDeviceArmDOTHalfArm
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmDOTHalfArm:self.device.device_id success:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"发布半布防消息成功" toView:self.view];
    }fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}
- (void)setDeviceArmDOTDisarm
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] setDeviceArmDOTDisarm:self.device.device_id success:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"发布撤防消息成功" toView:self.view];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

#pragma mark - UI
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = 0.6;
    }
    return _bgView;
}
- (SMDTimeChooseView *)timeView
{
    if (!_timeView) {
        _timeView = [[[NSBundle mainBundle] loadNibNamed:@"SMDTimeChooseView" owner:self options:nil] lastObject];
        _timeView.frame = CGRectMake(0, 0, kScreenWidth, 50);
        _timeView.startTimeBtn.tag = 1;
        _timeView.endTimeBtn.tag = 2;
        [_timeView.startTimeBtn addTarget:self action:@selector(showChooseDateView:) forControlEvents:UIControlEventTouchUpInside];
        [_timeView.endTimeBtn addTarget:self action:@selector(showChooseDateView:) forControlEvents:UIControlEventTouchUpInside];
        [_timeView.searchBtn addTarget:self action:@selector(searchRecordWithTime) forControlEvents:UIControlEventTouchUpInside];
    }
    return _timeView;
}

- (SMDDateChooseView *)dateChooseView
{
    if (!_dateChooseView) {
        _dateChooseView = [[[NSBundle mainBundle] loadNibNamed:@"SMDDateChooseView" owner:self options:nil] lastObject];
        _dateChooseView.frame = CGRectMake(0, kScreenHeight - 250, kScreenWidth, 250);
        _dateChooseView.delegate = self;
    }
    return _dateChooseView;
}

- (UITableView *)logTable{
    if(!_logTable){
        _logTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight/2.0-64 - 20) style:UITableViewStylePlain];
        _logTable.dataSource = self;
        _logTable.delegate = self;
        _logTable.tableFooterView = [UIView new];
        _logTable.tableHeaderView = [UIView new];
        _logTable.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
        _logTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _logTable;
}

- (UIButton *)settingBtn{
    if(!_settingBtn){
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settingBtn setImage:[UIImage imageNamed:@"dSetting"] forState:UIControlStateNormal];
        [_settingBtn sizeToFit];
        [_settingBtn addTarget:self action:@selector(tosettingViewController) forControlEvents:UIControlEventTouchUpInside];
    }
    return _settingBtn;
}

- (UIButton *)msgBtn{
    if(!_msgBtn){
        _msgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_msgBtn setImage:[UIImage imageNamed:@"dMsg"] forState:UIControlStateNormal];
        [_msgBtn addTarget:self action:@selector(chooseMessageType) forControlEvents:UIControlEventTouchUpInside];
        [_msgBtn sizeToFit];
    }
    return _msgBtn;
}

-(UIView *)footer{
    if(!_footer){
        // Do any additional setup after loading the view.
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight/2.0-64 - 20, kScreenWidth, kScreenHeight/2.0 + 20)];
        
        
        float h = self.callBtn.bounds.size.height;
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(0.0, 0)];
        [path addLineToPoint:CGPointMake(kScreenWidth, 0)];
        [path addLineToPoint:CGPointMake(kScreenWidth, h)];
        [path addQuadCurveToPoint:CGPointMake(0, h) controlPoint:CGPointMake(kScreenWidth/2.0, 0)];
        [path addLineToPoint:CGPointMake(0, 0)];
        
        CAShapeLayer *pathLayer = [CAShapeLayer layer];
        pathLayer.frame = CGRectMake(0, 0, kScreenWidth, h);
        pathLayer.path = path.CGPath;
        pathLayer.strokeColor = [[UIColor colorWithHexColorString:@"5696de"] CGColor];
        pathLayer.fillColor = [[UIColor colorWithHexColorString:@"5696de"] CGColor];
        pathLayer.lineWidth = 0;
        pathLayer.lineJoin = kCALineJoinBevel;
        [footer.layer addSublayer:pathLayer];
        footer.backgroundColor = [UIColor whiteColor];
        
        [footer addSubview:self.callBtn];
        [footer addSubview:self.isOnline];
        [footer addSubview:self.netWork];
        [footer addSubview:self.battery];
        [footer addSubview:self.bufangBtn];
        [footer addSubview:self.chefangBtn];
        [footer addSubview:self.banbufangBtn];
        
        
        
        [self.callBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(footer);
            make.centerX.mas_equalTo(footer);
            make.size.mas_equalTo(self.callBtn.frame.size);
        }];
        
        float offsetX = (kScreenWidth - 3*self.bufangBtn.frame.size.width)/4.0;
        [self.bufangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(footer).offset(-50);
            make.trailing.mas_equalTo(self.chefangBtn.mas_leading).offset(-offsetX);
            make.size.mas_equalTo(self.bufangBtn.frame.size);
        }];
        
        [self.chefangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.chefangBtn.frame.size);
            make.centerY.mas_equalTo(self.bufangBtn);
            make.centerX.mas_equalTo(footer);
        }];
        
        [self.banbufangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.banbufangBtn.frame.size);
            make.leading.mas_equalTo(self.chefangBtn.mas_trailing).offset(offsetX);
            make.centerY.mas_equalTo(self.bufangBtn);
        }];
        
        [self.isOnline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.bufangBtn);
            make.size.mas_equalTo(self.isOnline.frame.size);
            make.bottom.mas_equalTo(self.bufangBtn.mas_top).offset(-63);
        }];
        
        [self.netWork mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.netWork.frame.size);
            make.centerX.mas_equalTo(self.chefangBtn);
            make.bottom.mas_equalTo(self.bufangBtn.mas_top).offset(-63);
        }];
        
        [self.battery mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.battery.frame.size);
            make.centerX.mas_equalTo(self.banbufangBtn);
            make.bottom.mas_equalTo(self.bufangBtn.mas_top).offset(-63);
        }];
        
        [footer addSubview:self.isOnlineDesc];
        [footer addSubview:self.netWorkDesc];
        [footer addSubview:self.batteryDesc];
        [footer addSubview:self.bufangDesc];
        [footer addSubview:self.chefangDesc];
        [footer addSubview:self.banbufangDesc];
        
        [self.isOnlineDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.isOnlineDesc.frame.size);
            make.centerX.mas_equalTo(self.isOnline);
            make.top.mas_equalTo(self.isOnline.mas_bottom).offset(10);
        }];
        
        [self.netWorkDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.netWorkDesc.frame.size);
            make.centerX.mas_equalTo(self.netWork);
            make.top.mas_equalTo(self.isOnline.mas_bottom).offset(10);
        }];
        
        [self.batteryDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.batteryDesc.frame.size);
            make.centerX.mas_equalTo(self.battery);
            make.top.mas_equalTo(self.isOnline.mas_bottom).offset(10);
        }];
        
        [self.bufangDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.bufangDesc.frame.size);
            make.centerX.mas_equalTo(self.bufangBtn);
            make.top.mas_equalTo(self.bufangBtn.mas_bottom).offset(10);
        }];
        
        [self.chefangDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.chefangDesc.frame.size);
            make.centerX.mas_equalTo(self.chefangBtn);
            make.top.mas_equalTo(self.bufangBtn.mas_bottom).offset(10);
        }];
        
        [self.banbufangDesc mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.banbufangDesc.frame.size);
            make.centerX.mas_equalTo(self.banbufangBtn);
            make.top.mas_equalTo(self.bufangBtn.mas_bottom).offset(10);
        }];
        
        _footer = footer;
    }
    return _footer;
}


- (UILabel *)descLb:(NSString *)txt{
    UILabel *lb = [UILabel new];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor colorWithHexColorString:@"222222"];
    lb.text = txt;
    [lb sizeToFit];
    return lb;
}

- (UILabel *)banbufangDesc{
    if(!_banbufangDesc){
        _banbufangDesc = [self descLb:@"半布防"];
    }
    return _banbufangDesc;
}
- (UILabel *)chefangDesc{
    if(!_chefangDesc){
        _chefangDesc = [self descLb:@"撤防"];
    }
    return _chefangDesc;
}

- (UILabel *)bufangDesc{
    if(!_bufangDesc){
        _bufangDesc = [self descLb:@"布防"];
    }
    return _bufangDesc;
}
- (UILabel *)batteryDesc{
    if(!_batteryDesc){
        _batteryDesc = [self descLb:@"市电正常"];
    }
    return _batteryDesc;
}

- (UILabel *)netWorkDesc{
    if(!_netWorkDesc){
        _netWorkDesc = [self descLb:@"网络未知"];
    }
    return _netWorkDesc;
}
- (UILabel *)isOnlineDesc{
    if(!_isOnlineDesc){
        if (self.device.is_online == is_online_unknown) {
            _isOnlineDesc = [self descLb:@"设备离线"];
        }else if (self.device.is_online == is_online_on){
            _isOnlineDesc = [self descLb:@"设备在线"];
        }else if (self.device.is_online == is_online_off){
            _isOnlineDesc = [self descLb:@"设备离线"];
        }
    }
    return _isOnlineDesc;
}

- (UIButton *)callBtn{
    if(!_callBtn){
        _callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_callBtn setImage:[UIImage imageNamed:@"dCall"] forState:UIControlStateNormal];
        [_callBtn sizeToFit];
        [_callBtn addTarget:self action:@selector(callBtnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _callBtn;
}

-(UIButton *)banbufangBtn{
    if(!_banbufangBtn){
        _banbufangBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_banbufangBtn setImage:[UIImage imageNamed:@"dbanbuNormal"] forState:UIControlStateNormal];
        [_banbufangBtn sizeToFit];
        [_banbufangBtn addTarget:self action:@selector(setDeviceArmDOTHalfArm) forControlEvents:UIControlEventTouchUpInside];
    }
    return _banbufangBtn;
}
-(UIButton *)chefangBtn{
    if(!_chefangBtn){
        _chefangBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chefangBtn setImage:[UIImage imageNamed:@"dChefangNormal"] forState:UIControlStateNormal];
        [_chefangBtn sizeToFit];
        [_chefangBtn addTarget:self action:@selector(setDeviceArmDOTDisarm) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chefangBtn;
}

- (UIButton *)bufangBtn{
    if(!_bufangBtn){
        _bufangBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bufangBtn setImage:[UIImage imageNamed:@"dbufangNormal"] forState:UIControlStateNormal];
        [_bufangBtn sizeToFit];
        [_bufangBtn setTitle:@"布防" forState:UIControlStateNormal];
        [_bufangBtn addTarget:self action:@selector(setDeviceArmDOTArm) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bufangBtn;
}

-(UIImageView *)battery{
    if(!_battery){
        if (self.device.ac_status == ac_status_unknown) {
            _battery = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dBattery0"]];
        }else if (self.device.ac_status == ac_status_on){
            _battery = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dBattery2"]];
        }else if (self.device.ac_status == ac_status_off){
            _battery = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dBattery1"]];
        }
        
    }
    return _battery;
}
-(UIImageView *)netWork{
    if(!_netWork){
        if (self.device.signal_status == -1) {
            _netWork = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dNetwork0"]];
        }else{
            _netWork = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"dNetwork%d",self.device.signal_status]]];
            if (self.device.signal_status == 5) {
                _netWork = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dNetwork4"]];
            }
        }
    }
    return _netWork;
}
- (UIImageView *)isOnline{
    if(!_isOnline){
        if (self.device.is_online == is_online_unknown) {
            _isOnline = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dOffline"]];
        }else if (self.device.is_online == is_online_on){
            _isOnline = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dOnline"]];
        }else if (self.device.is_online == is_online_off){
            _isOnline = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dOffline"]];
        }
    }
    return _isOnline;
}

@end
