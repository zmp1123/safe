//
//  SMDeviceActionViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/27.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"
@class SMDevice;

@interface SMDeviceActionViewController : SMBaseViewController

@property (nonatomic, strong) SMDevice *device;

@property (nonatomic, copy) NSString *deviceArmId;

@end
