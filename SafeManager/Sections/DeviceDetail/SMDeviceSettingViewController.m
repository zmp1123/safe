//
//  SMDeviceSettingViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/28.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceSettingViewController.h"

#import "SMDControlListViewController.h"
#import "SMDTimeSyncViewController.h"
#import "SMDUpdateFirmwareViewController.h"
#import "SMDWuyeContactViewController.h"
#import "SMDArmZoneViewController.h"

@interface SMDeviceSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSArray *menus;
@property (nonatomic, strong) NSArray *images;


@end

@implementation SMDeviceSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设备设置";
    [self.view addSubview:self.table];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.menus.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"xxxx"];
    cell.textLabel.text = self.menus[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:self.images[indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.table deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 0) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDWuyeContactViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDWuyeContactViewController"];
        vc.device = self.device;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDArmZoneViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDArmZoneViewController"];
        vc.deviceId = self.deviceId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 2){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDControlListViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDControlListViewController"];
        vc.deviceId = self.deviceId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDTimeSyncViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDTimeSyncViewController"];
        vc.deviceId = self.deviceId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 4){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDUpdateFirmwareViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDUpdateFirmwareViewController"];
        vc.deviceId = self.deviceId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource = self;
        _table.tableFooterView = [UIView new];
        _table.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 20)];
    }
    return _table;
}
- (NSArray *)menus{
    if(!_menus){
        _menus = @[@"物业电话设置", @"防区设置", @"遥控器设置", @"同步时间", @"设备在线升级"];
    }
    return _menus;
}
- (NSArray *)images{
    if (!_images) {
        _images = @[@"device_set_wuye",@"device_set_fangqu",@"device_set_yaokongqi",@"device_set_tongbu",@"device_set_shengji"];
    }
    return _images;
}

@end
