//
//  SMPayWayViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMPayWayViewController.h"
#import "SMPayWayTableViewCell.h"
#import "SMPayMgr.h"
#import <AlipaySDK/AlipaySDK.h>
#import "Order.h"
#import "WXApi.h"
#import "WXApiObject.h"

typedef enum:NSInteger {
    PayType_Ali     = 1,
    PayType_WX      = 2,
}PayType;

@interface SMPayWayViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (nonatomic,assign) PayType paytype;

@end

@implementation SMPayWayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.payBtn.layer.cornerRadius = 5.f;
    self.payBtn.clipsToBounds = YES;
    
    self.noLabel.text = self.dealSn;
    self.priceLabel.text = [NSString stringWithFormat:@"%@元",self.price];
    self.receiveLabel.text = @"管家保";
    self.goodsLabel.text = self.dealTitle;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alipayResult:) name:@"alipayResult" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMPayWayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMPayWayTableViewCell"];
    if (indexPath.row == 0) {
        cell.pImageView.image = [UIImage imageNamed:@"payway_zhifubao"];
        cell.pNameLabel.text = @"支付宝";
        cell.pSelectImageView.image = [UIImage imageNamed:@"icon_pay_unselect"];
    }
    if (indexPath.row == 1) {
        cell.pImageView.image = [UIImage imageNamed:@"payway_weixin"];
        cell.pNameLabel.text = @"微信";
        cell.pSelectImageView.image = [UIImage imageNamed:@"icon_pay_unselect"];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.tableView reloadData];
    SMPayWayTableViewCell *cell = (SMPayWayTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.pSelectImageView.image = [UIImage imageNamed:@"icon_pay_select"];
    
    if (indexPath.row == 0) {
        self.paytype = PayType_Ali;
    }
    if (indexPath.row == 1) {
        self.paytype = PayType_WX;
    }
}

#pragma mark - Action
- (IBAction)payAction:(id)sender
{
    if (!self.paytype) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请选择付款方式" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.paytype == PayType_Ali) {
        [self alipay];
    }else if (self.paytype == PayType_WX){
        [self wxpay];
    }
}

#pragma mark - alipay
- (void)alipay
{
    [[SMPayMgr instance] getAlipaySignWithPartner:@"2088421635267699" service:@"mobile.securitypay.pay" sellerId:@"314398084@qq.com" outTradeNo:self.dealSn subject:self.dealTitle body:@"管家保" totalFee:self.price success:^(NSDictionary *data) {
        
        [[AlipaySDK defaultService] payOrder:data[@"string"] fromScheme:@"SafeManager" callback:^(NSDictionary *resultDic) {
            
        }];
    }];
}
#pragma mark - wxpay
- (void)wxpay
{
    [[SMPayMgr instance] getWxPayIndex:self.dealSn WIDtotal_fee:self.price success:^(NSDictionary *data) {
        NSMutableString *stamp  = [data objectForKey:@"timestamp"];
        
        PayReq* req             = [[PayReq alloc] init];
        req.partnerId           = [data objectForKey:@"partnerid"];
        req.prepayId            = [data objectForKey:@"prepayid"];
        req.nonceStr            = [data objectForKey:@"noncestr"];
        req.timeStamp           = stamp.intValue;
        req.package             = [data objectForKey:@"package"];
        req.sign                = [data objectForKey:@"sign"];
        [WXApi sendReq:req];
        
    }];
}

#pragma mark - notification
- (void)alipayResult:(NSNotification *)noti
{
    // 支付宝支付结果
    NSDictionary *result = noti.object;
    if ([result[@"resultStatus"] isEqualToString:@"9000"]) {
        // 查询状态
        
        // 成功
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"支付成功" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:result[@"memo"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
}

@end
