//
//  SMPayWayViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMPayWayViewController : SMBaseViewController

@property (nonatomic, copy) NSString *dealId;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *dealTitle;
@property (nonatomic, copy) NSString *dealSn;

@end
