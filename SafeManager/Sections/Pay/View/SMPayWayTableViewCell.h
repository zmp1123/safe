//
//  SMPayWayTableViewCell.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMPayWayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pImageView;
@property (weak, nonatomic) IBOutlet UILabel *pNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pSelectImageView;

@end
