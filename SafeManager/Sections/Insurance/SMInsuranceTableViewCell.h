//
//  SMInsuranceTableViewCell.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMInsuranceTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *priceLb;

@end
