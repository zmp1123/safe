//
//  SMMyInsuranceViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMMyInsuranceViewController.h"
#import "SMOrderListTableViewCell.h"
#import "SMInsuranceMgr.h"
#import "SMMyInsuranceOrderDetailViewController.h"

@interface SMMyInsuranceViewController ()<UITableViewDataSource,UITableViewDelegate, SMOrderListTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation SMMyInsuranceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的保险";
    
    [self.view addSubview:self.tableView];
    
    [self getOrderList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64)];
        [_tableView registerNib:[UINib nibWithNibName:@"SMOrderListTableViewCell" bundle:nil] forCellReuseIdentifier:@"SMOrderListTableViewCell"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = hexColor(EDF0F5);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SMOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SMOrderListTableViewCell"];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell refreshInsuranceOrderModel:self.dataArr[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210;
}

- (void)getOrderList
{
    [[SMInsuranceMgr instance] getInsuranceDealList:^(NSArray *list) {
        self.dataArr = [NSArray arrayWithArray:list];
        [self.tableView reloadData];
    }];
}

#pragma mark - SMOrderListTableViewCellDelegate
- (void)clickOrderDetail:(NSString *)sid
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMMyInsuranceOrderDetailStoryboard" bundle:nil];
    SMMyInsuranceOrderDetailViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMMyInsuranceOrderDetailViewController"];
    vc.iid = sid;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
