//
//  SMMyInsuranceOrderDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/5.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMMyInsuranceOrderDetailViewController.h"
#import "SMInsuranceOrder.h"
#import "SMInsuranceMgr.h"

@interface SMMyInsuranceOrderDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *peopleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardnoLabel;
@property (weak, nonatomic) IBOutlet UILabel *inoLabel;
@property (weak, nonatomic) IBOutlet UILabel *onoLabel;
@property (weak, nonatomic) IBOutlet UILabel *starttimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endtimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMMyInsuranceOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self getInsuranceDealList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getInsuranceDealList
{
    [[SMInsuranceMgr instance] getInsuranceDealDetail:self.iid success:^(SMInsuranceOrder *order) {
        self.peopleLabel.text = [NSString stringWithFormat:@"被保险人：%@",order.people];
        self.addressLabel.text = [NSString stringWithFormat:@"保险地址：%@",order.addr_home];
        self.cardnoLabel.text = [NSString stringWithFormat:@"身份证号：%@",order.id_number];
        self.inoLabel.text = [NSString stringWithFormat:@"保险编号：%@",order.ins_deal_sn];
        self.onoLabel.text = [NSString stringWithFormat:@"订单编号：%@",order.deal_sn];
        self.starttimeLabel.text = [NSString stringWithFormat:@"生效日期：%@",order.start_date];
        self.endtimeLabel.text = [NSString stringWithFormat:@"截止日期：%@",order.end_date];
        self.priceLabel.text = [NSString stringWithFormat:@"保险费：%@",order.price];
        self.contentLabel.text = [NSString stringWithFormat:@"投保财产：%@",order.property];
        self.maxLabel.text = [NSString stringWithFormat:@"最大保额：%@",order.coverage];
    }];
}


@end
