//
//  SMInsuranceTableViewCell.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMInsuranceTableViewCell.h"
#import "UIColor+Category.h"
#import "SMInsuranceTableViewCell.h"

#import <Masonry.h>

@implementation SMInsuranceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self addSubview:self.priceLb];
    [self.priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.trailing.mas_equalTo(self).offset(-35);
    }];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UILabel *)priceLb{
    if(!_priceLb){
        _priceLb = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
        _priceLb.textColor = [UIColor colorWithHexColorString:@"222222"];
        _priceLb.font = [UIFont systemFontOfSize:15];
        _priceLb.textAlignment = NSTextAlignmentRight;
    }
    return _priceLb;
}
@end
