//
//  SMAddInsuranceViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"
@class SMInsurance;
@interface SMAddInsuranceViewController : SMBaseViewController

@property (nonatomic, strong) SMInsurance *insurance;

@end
