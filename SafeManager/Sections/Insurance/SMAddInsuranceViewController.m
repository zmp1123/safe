//
//  SMAddInsuranceViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMAddInsuranceViewController.h"
#import "SMInsuranceTableViewCell.h"
#import "SMDeviceMgr.h"
#import "UIColor+Category.h"
#import "NSArray+Extension.h"
#import "SMUserMgr.h"
#import "SMInsurance.h"
#import <DownPicker.h>
#import <UIDownPicker.h>
#import "SMInsuranceMgr.h"
#import "SMPayWayViewController.h"
#import "SMProtocolViewController.h"

@interface SMAddInsuranceViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSArray *menus;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UIButton *agreeBtn;
@property (nonatomic, strong) UIButton *protocolBtn;
@property (nonatomic, strong) SMUser *user;

@property (nonatomic, strong) UITextField *shopName; //店铺名
@property (nonatomic, strong) UITextField *propertyName;//投保财产名
@property (nonatomic, strong) UIDownPicker  *deviceAddressPicker; //投保地址
@property (nonatomic, strong) UIDownPicker *year;       //投保年数
@property (nonatomic, strong) UIDownPicker *counts;     //投保数量
@property (nonatomic, strong) UIDownPicker *userTypes;  //用户类型

@property (nonatomic, strong) NSArray *deviceList;

@end

@implementation SMAddInsuranceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SMUserMgr *userMgr = [SMUserMgr instance];
    self.user = userMgr.user;
    if(!self.user){
        [userMgr queryUserSuccess:^(SMUser *user) {
            self.user = user;
            [self.table reloadData];
        } fail:^(NSString *msg) {
            
        }];
    }
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f1f3f7"];
    self.title = @"保险详情";
    [self.view addSubview:self.table];
    
    self.hub = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:self.hub];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.menus.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[SMInsuranceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"c0"];
    cell.textLabel.text = self.menus[indexPath.row];
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
    line.backgroundColor = [UIColor colorWithHexColorString:@"c0c0c0"];
    [cell addSubview:line];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row == 3){
        //店铺名称
        [cell.contentView addSubview:self.shopName];
        [self.shopName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.shopName.frame.size.height);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    if(indexPath.row == 4){
        [cell.contentView addSubview:self.deviceAddressPicker];
        [self.deviceAddressPicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    if(indexPath.row == 6){
        //投保财产
        [cell.contentView addSubview:self.propertyName];
        [self.propertyName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.propertyName.frame.size.height);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    if(indexPath.row == 7){
        //购买年数
        [cell.contentView addSubview:self.year];
        [self.year mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    if(indexPath.row == 8){
        //购买份数
        [cell.contentView addSubview:self.counts];
        [self.counts mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    if(indexPath.row == 9){
        SMInsuranceTableViewCell *c = (SMInsuranceTableViewCell*)cell;
        c.priceLb.text = [NSString stringWithFormat:@"%i", (int)self.insurance.price];
    }
    //用户类型
    if(indexPath.row == 10){
        [cell.contentView addSubview:self.userTypes];
        [self.userTypes mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.centerY.mas_equalTo(cell.contentView );
            make.trailing.mas_equalTo(cell.contentView ).offset(-30);
            make.leading.mas_equalTo(cell.contentView ).offset(100);
        }];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SMInsuranceTableViewCell *c = (SMInsuranceTableViewCell *)cell;
    switch (indexPath.row) {
        case 0:
            c.priceLb.text = self.user.userName;
            break;
        case 1:
            c.priceLb.text = self.user.idNumber;
            break;
        case 2:
            c.priceLb.text = self.user.email;
            break;
        case 5:
            c.priceLb.text = self.user.address;
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - Data
- (void)insuranceDealCreate
{
    if (!self.agreeBtn.selected) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先同意保险相关条款" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.propertyName.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入投保财产" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    NSInteger count = 1;
    if (self.counts.text.length == 0) {
        count = 1;
    }else{
        count = [self.counts.text integerValue];
    }
    
    NSInteger durationCount = 1;
    if ([self.year.text?self.year.text:self.year.placeholder isEqualToString:@"1年"]) {
        durationCount = 1;
    }else if ([self.year.text?self.year.text:self.year.placeholder isEqualToString:@"2年"]){
        durationCount = 2;
    }else if ([self.year.text?self.year.text:self.year.placeholder isEqualToString:@"3年"]){
        durationCount = 3;
    }else if ([self.year.text?self.year.text:self.year.placeholder isEqualToString:@"4年"]){
        durationCount = 4;
    }else if ([self.year.text?self.year.text:self.year.placeholder isEqualToString:@"5年"]){
        durationCount = 5;
    }
    NSString *deviceId;
    for (SMDevice *device in self.deviceList) {
        NSString *tmp;
        if (self.deviceAddressPicker.text.length == 0) {
            tmp = self.deviceAddressPicker.placeholder;
        }else{
            tmp = self.deviceAddressPicker.text;
        }
        if ([tmp isEqualToString:[NSString stringWithFormat:@"%@%@%@%@",device.province,device.city,device.district,device.addr]]) {
            deviceId = device.device_id;
        }else{
            
        }
    }
    
    NSString *addr;
    if (self.deviceAddressPicker.text.length == 0) {
        addr = self.deviceAddressPicker.placeholder;
    }else{
        addr = self.deviceAddressPicker.text;
    }
    
    NSString *ptype;
    if (self.userTypes.text.length == 0) {
        ptype = self.userTypes.placeholder;
    }else{
        ptype = self.userTypes.text;
    }
    
    [self.hub showAnimated:YES];
    [[SMInsuranceMgr instance] insuranceDealCreateWithProperty:self.propertyName.text count:count durationCount:durationCount deviceId:deviceId insuranceId:self.insurance.insuranceId shopAddr:addr peopleType:ptype success:^(NSDictionary *data) {
        
        [self.hub hideAnimated:YES];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMPayStoryboard" bundle:nil];
        SMPayWayViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMPayWayViewController"];
        vc.dealId = data[@"dealId"];
        vc.dealSn = data[@"dealSn"];
        vc.dealTitle = data[@"ins_title"];
        vc.price = data[@"price"];
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (void)agreeAction
{
    self.agreeBtn.selected = !self.agreeBtn.selected;
}

- (void)protocolAction
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceInfoStoryboard" bundle:nil];
    SMProtocolViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMProtocolViewController"];
    vc.title = @"保险条款-官家保";
    vc.urlStr = @"http://139.196.234.54/Home?c=Article&a=InsuranceRule";
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 显示操作状态
- (void)showState:(BOOL)state andStr:(NSString *)str
{
    if (state) {
        self.hub.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"share_success"]];
    }else{
        self.hub.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"share_tip"]];
    }
    self.hub.mode = MBProgressHUDModeCustomView;
    self.hub.labelText = str;
    [self.hub hide:YES afterDelay:1.0];
}

#pragma mark -properties
-(UILabel *)titleLb{
    if(!_titleLb){
        _titleLb = [[UILabel alloc]initWithFrame:CGRectMake(0, 0.5, kScreenWidth, 50)];
        _titleLb.textColor = [UIColor colorWithHexColorString:@"222222"];
        _titleLb.font = [UIFont systemFontOfSize:15];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.text = self.insurance.title;
        _titleLb.backgroundColor = [UIColor whiteColor];
    }
    return _titleLb;
}

-(UITextField *)shopName{
    if(!_shopName){
        _shopName = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth - 100, 30)];
        _shopName.font = [UIFont systemFontOfSize:12];
        NSString *s = @"请输入店铺名称（可选）";
        NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc]initWithString:s];
        [placeholder addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, s.length)];
        [placeholder addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"898989"] range:NSMakeRange(0, s.length)];
        _shopName.attributedPlaceholder = placeholder;
        _shopName.textAlignment = NSTextAlignmentRight;
    }
    return _shopName;
}

- (UITextField *)propertyName{
    if(!_propertyName){
        _propertyName = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth - 100, 30)];
        _propertyName.font = [UIFont systemFontOfSize:12];
        NSString *s = @"请输入投保财产（必选）";
        NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc]initWithString:s];
        [placeholder addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, s.length)];
        [placeholder addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"898989"] range:NSMakeRange(0, s.length)];
        _propertyName.attributedPlaceholder = placeholder;
        _propertyName.textAlignment = NSTextAlignmentRight;
    }
    return _propertyName;
}

- (UIDownPicker *)deviceAddressPicker{
    if(!_deviceAddressPicker){
        _deviceAddressPicker = [[UIDownPicker alloc]initWithData:@[@"", @""]];
        _deviceAddressPicker.font = [UIFont systemFontOfSize:12];
        _deviceAddressPicker.textColor = [UIColor colorWithHexColorString:@"898989"];
        _deviceAddressPicker.textAlignment = NSTextAlignmentRight;
        _deviceAddressPicker.placeholder = @"地址1";
        SMDeviceMgr *deviceMgr = [SMDeviceMgr instance];
        __block NSArray *menus;
        if(deviceMgr.myDevices){
            self.deviceList = [NSArray arrayWithArray:deviceMgr.myDevices];
            menus = [deviceMgr.myDevices map:^id(id obj) {
                SMDevice *d = obj;
                return [NSString stringWithFormat:@"%@%@%@%@",d.province,d.city,d.district,d.addr];;
            }];
        }else{
            [deviceMgr getArmDeviceListSuccess:^(NSArray *list) {
                self.deviceList = [NSArray arrayWithArray:list];
                menus = [list map:^id(id obj) {
                    SMDevice *d = obj;
                    return [NSString stringWithFormat:@"%@%@%@%@",d.province,d.city,d.district,d.addr];
                }];
                
            } Fail:^(NSString *msg) {
                
            }];
        }
        [_deviceAddressPicker.DownPicker setData:menus];
        _deviceAddressPicker.placeholder = menus.firstObject;
        
        _deviceAddressPicker.myBlock = ^(NSString *str){};
        
    }
    return _deviceAddressPicker;
}

- (UIDownPicker *)year{
    if(!_year){
        _year = [[UIDownPicker alloc]initWithData:@[@"1年", @"2年", @"3年", @"4年", @"5年"]];
        _year.font = [UIFont systemFontOfSize:12];
        _year.textColor = [UIColor colorWithHexColorString:@"898989"];
        _year.textAlignment = NSTextAlignmentRight;
        _year.placeholder = @"1年";
        _year.myBlock = ^(NSString *str){
            NSInteger count;
            if (self.counts.text.length == 0) {
                count = 1;
            }else{
                count = [self.counts.text integerValue];
            }
            if ([str isEqualToString:@"1年"]) {
                self.insurance.price = 100 * 1 * count;
            }else if ([str isEqualToString:@"2年"]){
                self.insurance.price = 100 * 2 * count;
            }else if ([str isEqualToString:@"3年"]){
                self.insurance.price = 100 * 3 * count;
            }else if ([str isEqualToString:@"4年"]){
                self.insurance.price = 100 * 4 * count;
            }else if ([str isEqualToString:@"5年"]){
                self.insurance.price = 100 * 5 * count;
            }
            [self.table reloadData];
        };
    }
    return _year;
}

- (UIDownPicker *)counts{
    if(!_counts){
        _counts = [[UIDownPicker alloc]initWithData:@[@"1", @"2", @"3", @"4", @"5"]];
        _counts.font = [UIFont systemFontOfSize:12];
        _counts.textColor = [UIColor colorWithHexColorString:@"898989"];
        _counts.textAlignment = NSTextAlignmentRight;
        _counts.placeholder = @"1";
        
        _counts.myBlock = ^(NSString *str){
            NSInteger year;
            if (self.year.text.length == 0) {
                year = 1;
            }else{
                year = [self.year.text integerValue];
            }
            self.insurance.price = year * [str integerValue] * 100;
            [self.table reloadData];
        };
    }
    return _counts;
}

- (UIDownPicker *)userTypes{
    if(!_userTypes){
        _userTypes = [[UIDownPicker alloc]initWithData:@[@"个人", @"商户", @"企业"]];
        _userTypes.font = [UIFont systemFontOfSize:12];
        _userTypes.textColor = [UIColor colorWithHexColorString:@"898989"];
        _userTypes.textAlignment = NSTextAlignmentRight;
        _userTypes.placeholder = @"个人";
        _userTypes.myBlock = ^(NSString *str){
        };
    }
    return _userTypes;
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-64) style:UITableViewStylePlain];
       
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 50.5)];
        [header addSubview:self.titleLb];
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithHexColorString:@"c0c0c0"];
        [header addSubview:line];
        _table.tableHeaderView = header;
        
        UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 190)];
        [footer addSubview:self.agreeBtn];
        [footer addSubview:self.protocolBtn];
        [footer addSubview:self.submitBtn];
        UILabel *desc0 = [[UILabel alloc] initWithFrame:CGRectMake(0, 63, kScreenWidth, 25)];
        desc0.text = @"1、投保人、身份证、邮箱、住址请填写正确";
        desc0.font = [UIFont systemFontOfSize:14];
        desc0.textColor = [UIColor colorWithHexColorString:@"898989"];
        UILabel *desc1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 63, kScreenWidth, 25)];
        desc1.text = @"如需修改，请在个人信息内修改。";
        desc1.font = [UIFont systemFontOfSize:14];
        desc1.textColor = [UIColor colorWithHexColorString:@"898989"];
        UILabel *desc2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 63, kScreenWidth, 25)];
        desc2.text = @"2、投保地址为主机地址，如错误请在设备详情里";
        desc2.font = [UIFont systemFontOfSize:14];
        desc2.textColor = [UIColor colorWithHexColorString:@"898989"];
        UILabel *desc3 = [[UILabel alloc] initWithFrame:CGRectMake(0, 63, kScreenWidth, 25)];
        desc3.text = @"修改";
        desc3.font = [UIFont systemFontOfSize:14];
        desc3.textColor = [UIColor colorWithHexColorString:@"898989"];
        [desc0 sizeToFit];
        [desc1 sizeToFit];
        [desc2 sizeToFit];
        [desc3 sizeToFit];
        [footer addSubview:desc0];
        [footer addSubview:desc1];
        [footer addSubview:desc2];
        [footer addSubview:desc3];
        
        [desc0 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(desc0.bounds.size);
            make.centerX.mas_equalTo(footer);
            make.top.mas_equalTo(self.submitBtn.mas_bottom).offset(15);
        }];
        [desc2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(desc2.bounds.size);
            make.centerX.mas_equalTo(footer);
            make.top.mas_equalTo(desc1.mas_bottom).offset(4);
        }];
        [desc1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(desc1.bounds.size);
            make.centerX.mas_equalTo(footer);
            make.top.mas_equalTo(desc0.mas_bottom).offset(4);
        }];
        [desc3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(desc3.bounds.size);
            make.centerX.mas_equalTo(footer);
            make.top.mas_equalTo(desc2.mas_bottom).offset(4);
        }];
      
        _table.tableFooterView = footer;
        _table.dataSource = self;
        _table.delegate = self;
        _table.backgroundColor = [UIColor colorWithHexColorString:@"f1f3f7"];
    }
    return _table;
}

- (NSArray *)menus{
    if(!_menus){
        _menus = @[@"被保险人", @"身份证号", @"邮箱", @"店铺名称", @"保险地址", @"住宅地址", @"投保财产", @"投保时间（年）",@"购买份数", @"保险费（元）"
                   ,@"用户类型"];
    }
    return _menus;
}

- (UIButton *)submitBtn{
    if(!_submitBtn){
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.backgroundColor = [UIColor colorWithHexColorString:@"5997db"];
        _submitBtn.frame = CGRectMake(10, 58, kScreenWidth - 20, 38);
        [_submitBtn setTitle:@"购买" forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(insuranceDealCreate) forControlEvents:UIControlEventTouchUpInside];
        _submitBtn.layer.cornerRadius = 5;
    }
    return _submitBtn;
}
- (UIButton *)agreeBtn
{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _agreeBtn.frame = CGRectMake(0, 0, 50, 50);
        [_agreeBtn setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
        [_agreeBtn addTarget:self action:@selector(agreeAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _agreeBtn;
}
- (UIButton *)protocolBtn
{
    if (!_protocolBtn) {
        _protocolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _protocolBtn.frame = CGRectMake(50, 0, 140, 50);
        [_protocolBtn setTitle:@"同意保险相关条款" forState:UIControlStateNormal];
        _protocolBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _protocolBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_protocolBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [_protocolBtn addTarget:self action:@selector(protocolAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _protocolBtn;
}
@end
