//
//  SMInsuranceViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMInsuranceViewController.h"
#import "UIColor+Category.h"
#import "SMInsuranceMgr.h"
#import "SMInsurance.h"
#import "SMInsuranceTableViewCell.h"
#import "SMAddInsuranceViewController.h"
#import "SMMyInsuranceViewController.h"
#import "RTHomeCycleScrollView.h"
#import "SMNewsAd.h"
#import "UIImageView+WebCache.h"
#import "SMNewsAdDetailViewController.h"

@interface SMInsuranceViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIScrollView *imageScroll;
@property (nonatomic, strong) UIPageControl *pageCtrl;
@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSMutableArray *propertyInsurances;
@property (nonatomic, retain) RTHomeCycleScrollView *mainScorllView;
@property (nonatomic, assign) NSInteger headScrollViewImageCount;
@property (nonatomic, strong) NSArray *adsArr;

@end

@implementation SMInsuranceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    self.propertyInsurances = [NSMutableArray new];
    
    [self.view addSubview:self.table];
    
    [[SMInsuranceMgr instance] getInsuranceList:^(NSArray *list) {
        self.propertyInsurances = [[NSMutableArray alloc]initWithArray:list];
        [self.table reloadData];
    }];
    [self getAdList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentPage:) name:@"currentPage" object:nil];
}

- (void)currentPage:(NSNotification *)notification{
    self.pageCtrl.currentPage = [notification.object integerValue];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section != 0) return [UIView new];
//    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
//    bg.image = [UIImage imageNamed:@"insuranceBg"];
    return self.imageScroll;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"c00"];
            cell.imageView.image = [UIImage imageNamed:@"insuranceProperty"];
            cell.textLabel.text = @"财产保障";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 39, kScreenWidth, 0.5)];
            bottomLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
            [cell addSubview:bottomLine];
            return cell;
        }
        else{
            static NSString *reuseId = @"c01";
            cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
            if(!cell){
                cell = [[SMInsuranceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
                cell.imageView.image = [UIImage imageNamed:@"insuranceIcon"];
                cell.textLabel.text = @"知乎8个月保障";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 58.5, kScreenWidth, 0.5)];
                bottomLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
                [cell addSubview:bottomLine];
            }
            SMInsurance *ins = self.propertyInsurances[indexPath.row - 1];
            cell.textLabel.text = ins.title;
            ((SMInsuranceTableViewCell*)cell).priceLb.text = [NSString stringWithFormat:@"%i元", (int)ins.price];
        }
    }
    else{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"c10"];
        cell.imageView.image = [UIImage imageNamed:@"insuranceMine"];
        cell.textLabel.text = @"我的保险";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 47.5, kScreenWidth, 0.5)];
        bottomLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
        [cell addSubview:bottomLine];
        UILabel *topLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        topLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
        [cell addSubview:topLine];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor colorWithHexColorString:@"222222"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(indexPath.row != 0){
            SMAddInsuranceViewController *controller = [SMAddInsuranceViewController new];
            SMInsurance *insu = self.propertyInsurances[indexPath.row - 1];
            controller.insurance = insu;
            [self.navigationController pushViewController:controller animated:true];
        }
    }else{
        SMMyInsuranceViewController *vc = [[SMMyInsuranceViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return self.propertyInsurances.count + 1;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return indexPath.row == 0 ? 79/2.0 : 118/2.0;
    }
    return 48;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 150 : 0.01;
}

#pragma mark - Data
- (void)getAdList
{
    [[SMInsuranceMgr instance] getAdList:^(NSArray *list) {
        self.headScrollViewImageCount = list.count;
        self.adsArr = [NSArray arrayWithArray:list];
        [self initHeadCycleScrollView];
    } fail:^(NSString *msg) {
        
    }];
}

- (void)initHeadCycleScrollView
{
    [self updateViewConstraints];
    
    if (self.headScrollViewImageCount > 1) {
        NSMutableArray *viewsArray = [@[] mutableCopy];
        for (int i = 0; i < self.headScrollViewImageCount; ++i) {
            
            SMNewsAd *model = self.adsArr[i];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
            [imageView sd_setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"insuranceBg"]];
            imageView.contentMode = UIViewContentModeScaleToFill;
            [viewsArray addObject:imageView];
        }
        
        self.mainScorllView = [[RTHomeCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150) animationDuration:5.0];
        self.mainScorllView.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1];
        
        self.mainScorllView.fetchContentViewAtIndex = ^UIView *(NSInteger pageIndex){
            return viewsArray[pageIndex];
        };
        
        __block SMInsuranceViewController *homeVC = self;
        self.mainScorllView.totalPagesCount = ^NSInteger(void){
            return homeVC.headScrollViewImageCount;
        };
        self.mainScorllView.TapActionBlock = ^(NSInteger pageIndex){
            NSLog(@"点击了第%ld个",(long)pageIndex);
            SMNewsAd *model = homeVC.adsArr[pageIndex];
            if (model.isHasContent) {
                SMNewsAdDetailViewController *conctr = [SMNewsAdDetailViewController new];
                conctr.adId = model.adid;
                [homeVC.navigationController pushViewController:conctr animated:true];
            }
        };
        [self.imageScroll addSubview:self.mainScorllView];
    }else if(self.headScrollViewImageCount == 1){
        SMNewsAd *model = self.adsArr[0];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:[UIImage imageNamed:@"insuranceBg"]];
        [self.imageScroll addSubview:imageView];
    }else{
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
        imageView.image = [UIImage imageNamed:@"insuranceBg"];
        [self.imageScroll addSubview:imageView];
    }
    [self.imageScroll addSubview:self.pageCtrl];
    [self.pageCtrl.superview bringSubviewToFront:self.pageCtrl];
}

#pragma mark - UI
- (UIPageControl *)pageCtrl
{
    if (!_pageCtrl) {
        _pageCtrl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 150-30, kScreenWidth, 30)];
        _pageCtrl.pageIndicatorTintColor = [UIColor grayColor];
        _pageCtrl.currentPageIndicatorTintColor = [UIColor whiteColor];
        _pageCtrl.numberOfPages = self.headScrollViewImageCount;
        _pageCtrl.currentPage = 0;
        _pageCtrl.userInteractionEnabled = NO;
    }
    return _pageCtrl;
}
- (UIScrollView *)imageScroll{
    if(!_imageScroll){
        _imageScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
    }
    return _imageScroll;
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _table.dataSource = self;
        _table.delegate = self;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}

@end
