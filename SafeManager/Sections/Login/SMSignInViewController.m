//
//  SMSignInViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/29.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMLoginTextField.h"
#import "UIColor+Category.h"
#import "SMSignInViewController.h"
#import "SMUserMgr.h"
#import "AppDelegate.h"
#import <Masonry.h>
#import <BEMCheckBox.h>

@interface SMSignInViewController ()

@property (nonatomic, strong) UITextField *account;
@property (nonatomic, strong) UITextField *verifyCode;
@property (nonatomic, strong) UITextField *password;
@property (nonatomic, strong) BEMCheckBox *readCheck;
@property (nonatomic, strong) UIImageView *background;

@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UIButton *getVerifyCodeBtn;
@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation SMSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.background];
    [self.view addSubview:self.submitBtn];
    [self.view addSubview:self.account];
    [self.view addSubview:self.password];
    [self.view addSubview:self.verifyCode];
    [self.view addSubview:self.backBtn];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(20);
        make.size.mas_equalTo(self.backBtn.bounds.size);
        make.top.mas_equalTo(self.view).offset(20);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self.view).offset(-20);
        make.leading.mas_equalTo(self.view).offset(20);
        make.bottom.mas_equalTo(self.view).offset(-60);
        make.height.mas_equalTo(32);
    }];
    
    [self.verifyCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.view);
        make.trailing.mas_equalTo(self.view).offset(-20);
        make.leading.mas_equalTo(self.view).offset(20);
        make.height.mas_equalTo(self.verifyCode.frame.size.height);
    }];
    [self.account mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.verifyCode.mas_top).offset(-20);
        make.trailing.mas_equalTo(self.view).offset(-20);
        make.leading.mas_equalTo(self.view).offset(20);
        make.height.mas_equalTo(self.account.frame.size.height);
    }];
    [self.password mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.verifyCode.mas_bottom).offset(20);
        make.trailing.mas_equalTo(self.view).offset(-20);
        make.leading.mas_equalTo(self.view).offset(20);
        make.height.mas_equalTo(self.password.frame.size.height);
    }];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing)]];
}

- (void)stopEditing
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onSendCodeClick{
    NSString *phone = self.account.text;
    if([phone  isEqual: @""]) return;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] sendVerifyCodePhoneNo:phone success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"验证码发送成功" toView:self.view];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (void)onsubmitClick{
    NSString *phone = self.account.text;
    NSString *pwd = self.password.text;
    NSString *code = self.verifyCode.text;
    if([phone isEqualToString:@""] || [pwd isEqualToString:@""] || [code isEqualToString:@""]) return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] registerUserPhone:phone
                                   password:pwd
                                 verifyCode:code
                                    success:^(SMUser *user) {
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                        [(AppDelegate *)([UIApplication sharedApplication].delegate) showMain];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[[UIAlertView alloc]initWithTitle:msg message:msg delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil] show];
    }];
}

- (UIImageView *)background{
    if(!_background){
        _background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _background.image = [UIImage imageNamed:@"login"];
        UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-1"]];
        [_background addSubview:logo];
        [logo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_background);
            make.top.mas_equalTo(_background).offset(62);
        }];
    }
    return _background;
}

- (UIButton *)getVerifyCodeBtn{
    if(!_getVerifyCodeBtn){
        _getVerifyCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_getVerifyCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _getVerifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_getVerifyCodeBtn sizeToFit];
        CGSize size = _getVerifyCodeBtn.frame.size;
        size.width += 20;
        _getVerifyCodeBtn.frame = CGRectMake(0, 0, size.width, size.height);
        _getVerifyCodeBtn.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
        _getVerifyCodeBtn.layer.cornerRadius = 5;
        [_getVerifyCodeBtn addTarget:self action:@selector(onSendCodeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getVerifyCodeBtn;
}

- (UIButton *)submitBtn{
    if(!_submitBtn){
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_submitBtn setTitle:@"注册" forState:UIControlStateNormal];
        _submitBtn.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
        _submitBtn.layer.cornerRadius = 5;
        [_submitBtn addTarget:self action:@selector(onsubmitClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitBtn;
}

- (UITextField *)account{
    if(!_account){
        _account = [[SMLoginTextField alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45)];
        _account.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginUser-1"]];
        _account.leftViewMode = UITextFieldViewModeAlways;
        _account.placeholder = @"账号";
    }
    return _account;
}

- (UITextField *)verifyCode{
    if(!_verifyCode){
        _verifyCode = [[SMLoginTextField alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45)];
        _verifyCode.placeholder = @"验证码";
        _verifyCode.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"verifyCode"]];
        _verifyCode.leftViewMode = UITextFieldViewModeAlways;
        _verifyCode.rightView = self.getVerifyCodeBtn;
        _verifyCode.rightViewMode = UITextFieldViewModeAlways;
    }
    return _verifyCode;
}

- (UITextField *)password{
    if(!_password){
        _password = [[SMLoginTextField alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 45)];
        _password.placeholder = @"密码";
        _password.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"password"]];
        CGSize size = [UIImage imageNamed:@"verifyCode"].size;
        _password.leftView.frame = CGRectMake(0, 0, size.width, size.height);
        _password.leftViewMode = UITextFieldViewModeAlways;
    }
    return _password;
}

- (UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [_backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [_backBtn setTitle:@"返回" forState:UIControlStateNormal];
        _backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _backBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [_backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

@end
