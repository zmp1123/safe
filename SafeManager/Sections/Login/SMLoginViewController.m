//
//  SMLoginViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMLoginViewController.h"
#import "UIColor+Category.h"
#import "SMLoginTextField.h"
#import "SMUserMgr.h"
#import "AppDelegate.h"
#import "SMSignInViewController.h"
#import <Masonry.h>
#import "UMessage.h"
#import "SMForgetPwdViewController.h"

@interface SMLoginViewController ()

@property (nonatomic, strong)UIImageView *background;
@property (nonatomic, strong)UITextField *name;
@property (nonatomic, strong)UITextField *password;
@property (nonatomic, strong)UIButton *login;
@property (nonatomic, strong)UIButton *signIn;
@property (nonatomic, strong)UIButton *forgetPwdBtn;

@property (nonatomic,strong) SMUserMgr *userMgr;
@end

@implementation SMLoginViewController

- (void)viewDidLoad {
    self.userMgr = [SMUserMgr instance];
    [super viewDidLoad];
    [self.view addSubview:self.background];
    
     [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing)]];
}

- (void)stopEditing
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onLoginClick{
    NSString *usrName = self.name.text;
    NSString *usrPwd = self.password.text;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(usrPwd && usrName){
        [self.userMgr loginWithUserName:usrName password:usrPwd success:^(id res) {
            
            [UMessage addAlias:res[@"id"] type:@"zhihu" response:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
                if (responseObject) {
                    
                    NSLog(@"友盟推送Alias:%@",responseObject);
                }else{
                    NSLog(@"%@",error);
                }
            }];

            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [app showMain];
        } fail:^(NSString *msg) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:msg toView:self.view];
        }];
    }
}

- (void)onSignInClick{
    SMSignInViewController *controller = [SMSignInViewController new];
    [self presentViewController:controller animated:true completion:^{
        
    }];
}

- (void)forgetPwdAction{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMLoginStoryboard" bundle:nil];
    SMForgetPwdViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMForgetPwdViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (UIImageView *)background{
    if(!_background){
        _background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _background.image = [UIImage imageNamed:@"login"];
        _background.userInteractionEnabled = true;
        UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-1"]];
        [_background addSubview:logo];
        [logo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_background);
            make.top.mas_equalTo(_background).offset(62);
        }];
        [_background addSubview:self.name];
        [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(logo.mas_bottom).offset(65);
            make.centerX.mas_equalTo(_background);
            make.size.mas_equalTo(self.name.bounds.size);
        }];
        
        [_background addSubview:self.password];
        [self.password mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.name.mas_bottom).offset(15);
            make.centerX.mas_equalTo(_background);
            make.size.mas_equalTo(self.password.bounds.size);
        }];
        
        [_background addSubview:self.forgetPwdBtn];
        [self.forgetPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.password.mas_bottom).offset(10);
            make.size.mas_equalTo(self.forgetPwdBtn.bounds.size);
            make.right.mas_equalTo(self.password.mas_right);
        }];
        
        [_background addSubview:self.login];
        [self.login mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.login.bounds.size);
            make.top.mas_equalTo(self.password.mas_bottom).offset(65);
            make.centerX.mas_equalTo(_background);
        }];
        [_background addSubview:self.signIn];
        
        [self.signIn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.signIn.bounds.size);
            make.top.mas_equalTo(self.login.mas_bottom).offset(9);
            make.centerX.mas_equalTo(_background);
        }];
    }
    return _background;
}

- (UITextField *)name{
    if(!_name){
        _name = [[SMLoginTextField alloc]initWithFrame:CGRectMake(0, 0, 285, 45)];
        _name.font = [UIFont systemFontOfSize:18];
        _name.textColor = [UIColor colorWithHexColorString:@"5384d4"];
        _name.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginUser-1"]];
        _name.leftViewMode = UITextFieldViewModeAlways;
        _name.keyboardType = UIKeyboardTypeNumberPad;
        
        NSString *p = @"请输入您的用户名";
        NSMutableAttributedString *placeHolder = [[NSMutableAttributedString alloc] initWithString:p];
        [placeHolder addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, p.length)];
        [placeHolder addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"5384d4"] range:NSMakeRange(0, p.length)];
        _name.attributedPlaceholder = placeHolder;
        if(self.userMgr.lastUserName){
            _name.text = self.userMgr.lastUserName;
        }
    }
    return _name;
}

- (UITextField *)password{
    if(!_password){
        _password = [[SMLoginTextField alloc]initWithFrame:CGRectMake(0, 0, 285, 45)];
        _password.font = [UIFont systemFontOfSize:18];
        _password.secureTextEntry = true;
        _password.textColor = [UIColor colorWithHexColorString:@"5384d4"];
        CGSize size = [UIImage imageNamed:@"loginUser-1"].size;
        _password.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
        _password.leftView.frame = CGRectMake(0, 0, size.width, size.height);
        _password.leftViewMode = UITextFieldViewModeAlways;
        
        NSString *p = @"请输入密码";
        NSMutableAttributedString *placeHolder = [[NSMutableAttributedString alloc] initWithString:p];
        [placeHolder addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, p.length)];
        [placeHolder addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexColorString:@"5384d4"] range:NSMakeRange(0, p.length)];
        _password.attributedPlaceholder = placeHolder;
    }
    return _password;
}

- (UIButton *)login{
    if(!_login){
        _login = [UIButton buttonWithType:UIButtonTypeCustom];
        _login.titleLabel.font = [UIFont systemFontOfSize:23];
        _login.titleLabel.textColor = [UIColor whiteColor];
        _login.frame = CGRectMake(0, 0, 285, 32);
        _login.layer.cornerRadius = 16;
        [_login setTitle:@"登录" forState:UIControlStateNormal];
        _login.backgroundColor = [UIColor colorWithHexColorString:@"5696de"];
        _login.layer.borderColor = [UIColor colorWithHexColorString:@"739fd3"].CGColor;
        _login.layer.borderWidth = 1;
        [_login addTarget:self action:@selector(onLoginClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login;
}

- (UIButton *)signIn{
    if(!_signIn){
        _signIn = [UIButton buttonWithType:UIButtonTypeCustom];
        _signIn.titleLabel.font = [UIFont systemFontOfSize:23];
        [_signIn setTitleColor:[UIColor colorWithHexColorString:@"739fd3"] forState:UIControlStateNormal];
        _signIn.frame = CGRectMake(0, 0, 285, 32);
        _signIn.layer.cornerRadius = 16;
        [_signIn setTitle:@"注册" forState:UIControlStateNormal];
        _signIn.backgroundColor = [UIColor clearColor];
        _signIn.layer.borderColor = [UIColor colorWithHexColorString:@"739fd3"].CGColor;
        _signIn.layer.borderWidth = 1;
        [_signIn addTarget:self action:@selector(onSignInClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _signIn;
}

- (UIButton *)forgetPwdBtn{
    if (!_forgetPwdBtn) {
        _forgetPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_forgetPwdBtn setTitleColor:[UIColor colorWithHexColorString:@"5696de"] forState:UIControlStateNormal];
        _forgetPwdBtn.frame = CGRectMake(0, 0, 100, 32);
        _forgetPwdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _forgetPwdBtn.layer.cornerRadius = 16;
        [_forgetPwdBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
        _forgetPwdBtn.backgroundColor = [UIColor clearColor];
        [_forgetPwdBtn addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetPwdBtn;
}

@end
