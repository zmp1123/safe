//
//  SMLoginTextField.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMLoginTextField.h"
#import "UIColor+Category.h"

@implementation SMLoginTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(50, frame.size.height - 6, frame.size.width - 55, 0.5)];
    line.backgroundColor = [UIColor colorWithHexColorString:@"5384d4"];
    [self addSubview:line];
    return self;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return CGRectMake(50, 0, bounds.size.width - 50, bounds.size.height);
}

- (CGRect)textRectForBounds:(CGRect)bounds{
     return CGRectMake(50, 0, bounds.size.width - 50, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds{
     return CGRectMake(50, 0, bounds.size.width - 50, bounds.size.height);
}

@end
