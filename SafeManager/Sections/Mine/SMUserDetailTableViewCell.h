//
//  SMUserDetailTableViewCell.h
//  SafeManager
//
//  Created by zxf on 16/10/30.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMUserDetailTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *detailLb;

@end
