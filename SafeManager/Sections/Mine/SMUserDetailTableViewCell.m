//
//  SMUserDetailTableViewCell.m
//  SafeManager
//
//  Created by zxf on 16/10/30.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMUserDetailTableViewCell.h"
#import <Masonry.h>
@implementation SMUserDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self addSubview:self.detailLb];
    [self.detailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.trailing.equalTo(self).offset(-30);
        make.width.mas_equalTo(self.detailLb.frame.size.width);
    }];
    return self;
}

-(UILabel *)detailLb{
    if(!_detailLb){
        _detailLb = [UILabel new];
    }
    return _detailLb;
}

@end
