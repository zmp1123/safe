//
//  SMChangePwdViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/24.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChangePwdViewController.h"
#import "SMUserMgr.h"

@interface SMChangePwdViewController ()
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SMChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.getCodeBtn.layer.cornerRadius = 3.f;
    self.getCodeBtn.clipsToBounds = YES;
    self.saveBtn.layer.cornerRadius = 3.f;
    self.saveBtn.clipsToBounds = YES;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopEditing)]];
}

- (void)stopEditing
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)getCodeAction:(id)sender {
    SMUserMgr *mgr = [SMUserMgr instance];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] sendVerifyCodePhoneNo:mgr.user.phone success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"短信验证码发送成功" toView:self.view];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}
- (IBAction)saveAction:(id)sender {
    SMUserMgr *mgr = [SMUserMgr instance];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] appUserModifyPwdByVerifyCode:self.codeTF.text phone:mgr.user.phone password:self.pwdTF.text success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"修改成功" toView:self.view];
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(backAction:) userInfo:nil repeats:NO];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
