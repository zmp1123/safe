//
//  SMMineViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMMineViewController.h"
#import "UIColor+Category.h"
#import "SMUserMgr.h"
#import "SMSettingViewController.h"
#import "SMAboutViewController.h"
#import "SMHelpViewController.h"
#import <UIImageView+AFNetworking.h>
#import "SMUserDetailViewController.h"
#import <Masonry.h>
#import "SMOrderInquiryViewController.h"
#import "SMAccountAuthorizeViewController.h"
#import "SMLoginViewController.h"
#import "AppDelegate.h"
#import "UIImageVIew+WebCache.h"

@interface SMMineViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *phoneNo;
@property (nonatomic, strong) UIImageView *portrait;
@property (nonatomic, strong) SMUser *user;
@end

@implementation SMMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    [self.view addSubview:self.table];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    SMUserMgr *userMgr = [SMUserMgr instance];
    if(userMgr.user){
        self.user = userMgr.user;
        [self fillData:self.user];
    }else{
        [userMgr queryUserSuccess:^(SMUser *user) {
            self.user = user;
            [self fillData:user];
        } fail:^(NSString *msg) {
            
        }];
    }
}

- (void)fillData:(SMUser *)user{
    self.name.text = [user.nickName  isEqual: @""] || user.nickName == NULL ? @"未知": user.nickName;
    self.phoneNo.text = [NSString stringWithFormat:@"手机号：%@", user.phone == NULL ? @"未知": user.phone];
    if ([user.avatar containsString:@"http://139.196.234.54/"]) {
        [self.portrait sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"minePortrait"]];
    }else{
        [self.portrait sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://139.196.234.54/%@",user.avatar]] placeholderImage:[UIImage imageNamed:@"minePortrait"]];
    }
//    [self.portrait setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"minePortrait"]];
}

- (void)logout
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMUserMgr instance] appUserLogout:^(id res) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user.lastUserPwd"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user.lastUserName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        SMLoginViewController *vc = [[SMLoginViewController alloc] init];
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        app.window.rootViewController = vc;

    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        //订单查询
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMOrderInquiryStoryboard" bundle:nil];
        SMOrderInquiryViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMOrderInquiryViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if(indexPath.section == 1){
        if(indexPath.row == 0){
            //设置
            [self.navigationController pushViewController:[SMSettingViewController new] animated:true];
        }
        if(indexPath.row == 1){
            //关于
            [self.navigationController pushViewController:[SMAboutViewController new] animated:true];
        }
        if(indexPath.row == 2){
            //使用帮助
            [self.navigationController pushViewController:[SMHelpViewController new] animated:true];
        }
    }
    else{
        if(indexPath.row == 0){
            //账号授权
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMAuthorizeStoryboard" bundle:nil];
            SMAccountAuthorizeViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMAccountAuthorizeViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        if(indexPath.row == 1){
            //退出
            [self logout];
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 3;
    }else{
        return 2;
    }
//    return section == 0 ? 1 : 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 184 : 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell  = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"xxx"];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor colorWithHexColorString:@"222222"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //todo
    NSString *menuName, *iconName;
    if(indexPath.section == 0){
        menuName = @"缴费查询";
        iconName = @"mineBill";
    }
    else if(indexPath.section == 1){
        menuName = indexPath.row == 0 ? @"设置" : @"关于";
        iconName = indexPath.row == 0 ? @"mineSetting" : @"mineAbout";
        
        if (indexPath.row == 0) {
            menuName = @"设置";
            iconName = @"mineSetting";
        }else if (indexPath.row == 1){
            menuName = @"关于";
            iconName = @"mineAbout";
        }else{
            menuName = @"使用帮助";
            iconName = @"ic_profile_help";
        }
    }
    else{
        menuName = indexPath.row == 0 ? @"账号授权" : @"退出";
        iconName = indexPath.row == 0 ? @"mineAccount" : @"mineExit";
    }
    
    cell.imageView.image = [UIImage imageNamed:iconName];
    cell.textLabel.text = menuName;
    
    UILabel *btLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, kScreenWidth, 0.5)];
    btLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
    [cell addSubview:btLine];
    
    if(indexPath.row == 0){
        UILabel *topLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        topLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
        [cell addSubview:topLine];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section != 0) return [UIView new];
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 184)];
    view.image = [UIImage imageNamed:@"mineBg"];
    [view addSubview:self.portrait];
    [view addSubview:self.name];
    [view addSubview:self.phoneNo];
    view.userInteractionEnabled = true;
    
    [self.portrait mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.portrait.bounds.size);
        make.top.mas_equalTo(view).offset(20);
        make.centerX.mas_equalTo(view);
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(view);
        make.left.mas_equalTo(view);
        make.height.mas_equalTo(31);
        make.top.mas_equalTo(self.portrait.mas_bottom).offset(5);
    }];
    
    [self.phoneNo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(view);
        make.left.mas_equalTo(view);
        make.height.mas_equalTo(31);
        make.top.mas_equalTo(self.name.mas_bottom).offset(-5);
    }];
    
    return view;
}

- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _table.dataSource = self;
        _table.delegate = self;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}


- (UILabel *)name{
    if(!_name){
        _name = [UILabel new];
        _name.font = [UIFont systemFontOfSize:15];
        _name.textColor = [UIColor whiteColor];
        _name.text = @" ";
        _name.textAlignment = NSTextAlignmentCenter;
    }
    return _name;
}

- (UILabel *)phoneNo{
    if(!_phoneNo){
        _phoneNo = [UILabel new];
        _phoneNo.textColor = [UIColor whiteColor];
        _phoneNo.font = [UIFont systemFontOfSize:14];
        _phoneNo.text = @"手机号：13823136007";
        _phoneNo.textAlignment = NSTextAlignmentCenter;
    }
    return _phoneNo;
}

- (UIImageView *)portrait{
    if(!_portrait){
        _portrait = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
        _portrait.image = [UIImage imageNamed:@"minePortrait"];
        _portrait.layer.cornerRadius = 45;
        _portrait.layer.masksToBounds = true;
        _portrait.userInteractionEnabled = true;
        
        UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onPotraitClick)];
        reg.numberOfTapsRequired = 1;
        [_portrait addGestureRecognizer:reg];
    }
    return _portrait;
}

- (void)onPotraitClick{
    SMUserDetailViewController *controller = [SMUserDetailViewController new];
    [self.navigationController pushViewController:controller animated:true];
}

@end
