//
//  SMChangeEmailViewController.h
//  SafeManager
//
//  Created by zxf on 16/11/1.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMChangeEmailViewController : SMBaseViewController

@property (nonatomic, copy) NSString *defaultEmail;

@end
