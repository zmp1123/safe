//
//  SMChangeSexViewController.m
//  SafeManager
//
//  Created by zxf on 16/11/1.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMChangeSexViewController.h"
#import "SMUserMgr.h"
#import "NSArray+Extension.h"
@interface SMChangeSexViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic ,strong) UITableView *table;
@property (nonatomic, assign) int sex;

@end

@implementation SMChangeSexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    SMUserMgr *mgr = [SMUserMgr instance];
    self.sex = mgr.user.sex;
    self.title = @"修改性别";
    UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
    [submit setTitle:@"提交" forState:UIControlStateNormal];
    [submit sizeToFit];
    [submit addTarget:self action:@selector(onSubmitClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:submit];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    [self.view addSubview:self.table];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.sex = (int)indexPath.row;
    for (UITableViewCell *cell in tableView.visibleCells) {
         cell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    cell.textLabel.text = indexPath.row == 0 ? @"男":@"女";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SMUserMgr *mgr = [SMUserMgr instance];
    cell.accessoryType = mgr.user.sex == indexPath.row ? UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    return cell;
}


- (void)onSubmitClick{
    SMUserMgr *mgr = [SMUserMgr instance];
    [mgr updateUserInfoNickName:nil idNumber:nil avatar:nil email:nil sex:self.sex qq:nil province:nil city:nil distrit:nil addr:nil success:^(SMUser *usr) {
        [self.navigationController popViewControllerAnimated:true];
    } fail:^(NSString *msg) {
        [self simpleAlertString:msg];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource =self;
        _table.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
        _table.tableFooterView = [UIView new];
        _table.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 20)];
    }
    return _table;
}

@end
