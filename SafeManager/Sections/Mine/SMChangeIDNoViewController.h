//
//  SMChangeIDNoViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/31.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMChangeIDNoViewController : SMBaseViewController

@property (nonatomic, copy) NSString *defaultIDNo;

@end
