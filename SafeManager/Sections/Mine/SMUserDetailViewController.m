//
//  SMUserDetailViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/30.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMUserDetailViewController.h"
#import "UIColor+Category.h"
#import "SMUserMgr.h"
#import "SMUserDetailTableViewCell.h"
#import "SMChangeNameViewController.h"
#import "SMChangeIDNoViewController.h"
#import "SMChangeSexViewController.h"
#import "SMChangeAddressViewController.h"
#import "SMChangeQQViewController.h"
#import "SMChangeEmailViewController.h"
#import <UIImageView+AFNetworking.h>
#import <Masonry.h>
#import <AFNetworking.h>
#import <MBProgressHUD.h>
#import "SMChangePwdViewController.h"
#import "UIImageVIew+WebCache.h"

@interface SMUserDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) UIImageView *avatar;
@property (nonatomic, strong) MBProgressHUD *hub;

@property (nonatomic, strong) NSArray *menus;

@property (nonatomic, strong) SMUser *user;

@end

@implementation SMUserDetailViewController
@synthesize user=_user;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.table.hidden = YES;

    self.title = @"个人信息";
    self.menus = @[@[@[@"头像",@"iconfont-leixing"], @[@"姓名",@"iconfont-dianpu"], @[@"身份证号码",@"idCard"], @[@"性别",@"iconfont-iconxingbie"], @[@"地址",@"iconfont-dizhi"]],
                   @[@[@"手机",@"iconfont-dianhua"], @[@"qq",@"qq"], @[@"邮箱",@"documents"]],
                   @[@[@"更改密码",@"iconfont-leixing"]]];
    [self.view addSubview:self.table];
    self.hub = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hub];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    SMUserMgr *mgr = [SMUserMgr instance];
    if(mgr.user){
        self.user = mgr.user;
        self.table.hidden = NO;
    }else{
        [mgr queryUserSuccess:^(SMUser *user) {
            self.user = user;
            self.table.hidden = NO;
        } fail:^(NSString *msg) {
            
        }];
    }
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)changeAvatar
{
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从手机相册选择", nil];
    [sheet showInView:self.view];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        switch (indexPath.row) {
            case 0:
                [self changeAvatar];
                break;
            case 1:{
                SMChangeNameViewController *ctr = [SMChangeNameViewController new];
                ctr.defaultName = self.user.nickName;
                [self.navigationController pushViewController:ctr animated:true];
                break;
            }
            case 2:{
                SMChangeIDNoViewController *vc = [SMChangeIDNoViewController new];
                vc.defaultIDNo = self.user.idNumber;
                [self.navigationController pushViewController:vc animated:true];
                break;
            }
            case 3:{
                [self.navigationController pushViewController:[SMChangeSexViewController new] animated:true];
                break;
            }
            case 4:{
                [self.navigationController pushViewController:[SMChangeAddressViewController new] animated:true];
                break;
            }
            default:
                break;
        }
    }
    if(indexPath.section == 1){
        if(indexPath.row == 1){
            SMChangeQQViewController *vc = [SMChangeQQViewController new];
            vc.defaultQQ = self.user.qq;
            [self.navigationController pushViewController:vc animated:true];
        }
        if(indexPath.row == 2){
            //email
            SMChangeEmailViewController * vc = [SMChangeEmailViewController new];
            vc.defaultEmail = self.user.email;
            [self.navigationController pushViewController:vc animated:true];
        }
    }
    if(indexPath.section == 2){
        //change pwd
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMMineStoryboard" bundle:nil];
        SMChangePwdViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMChangePwdViewController"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;
    switch (buttonIndex)
    {
            // 拍照
        case 0:
            [self takePhoto];
            break;
            // 手机相册
        case 1:
            [self localPhoto];
            break;
        default:
            break;
    }
}

#pragma mark - delegate:UIImagePickerControllerDelegate

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }else
    {
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)localPhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

//当选择一张图片后进入这里
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //初始化imageNew为从相机中获得的--
    UIImage *imageNew = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    //设置image的尺寸
    CGSize imagesize = imageNew.size;
    imagesize.height =300;
    imagesize.width =300;
    //对图片大小进行压缩--
    imageNew = [self imageWithImage:imageNew scaledToSize:imagesize];
    NSData *imageData = UIImageJPEGRepresentation(imageNew,0.3);
//    self.avatar.image = [UIImage imageWithData:imageData];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    //清楚头像缓存
//    [self.imageManager.imageCache removeImageForKey:nil];
    
    //上传至服务器
    [self.hub showAnimated:YES];
    SMUserMgr *mgr = [SMUserMgr instance];
    __weak SMUserMgr *weakMgr = mgr;
    [mgr uploadImageData:imageData success:^(NSDictionary *res) {
        NSLog(@"%@", res);
        if([res[@"status"]integerValue] == 0){
            self.hub.mode = MBProgressHUDModeCustomView;
            self.hub.label.text = @"修改成功";
            [self.hub hideAnimated:YES afterDelay:1.0];
            [weakMgr updateUserInfoNickName:nil idNumber:nil avatar:res[@"result"][@"url"] email:nil sex:9 qq:nil province:nil city:nil distrit:nil addr:nil success:^(SMUser *usr) {
                
            } fail:^(NSString *msg) {
                
            }];
        }else{
            self.hub.mode = MBProgressHUDModeCustomView;
            self.hub.label.text = @"修改失败";
            [self.hub hideAnimated:YES afterDelay:1.0];
        }
    } fail:^(NSString *msg) {
        self.hub.mode = MBProgressHUDModeCustomView;
        self.hub.label.text = @"修改失败";
        [self.hub hideAnimated:YES afterDelay:1.0];
    }];
    
}
//对图片尺寸进行压缩
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *data = self.menus[indexPath.section][indexPath.row];
    cell.textLabel.text = data[0];
    cell.imageView.image = [UIImage imageNamed:data[1]];
    if(self.user == NULL) return;
    if(indexPath.section == 0){
        switch (indexPath.row) {
            case 0:
                //[self.avatar setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", self.user.avatar]]];
                if ([self.user.avatar containsString:@"http://139.196.234.54/"]) {
                    [self.avatar sd_setImageWithURL:[NSURL URLWithString:self.user.avatar] placeholderImage:nil];
                }else{
                    [self.avatar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://139.196.234.54/%@",self.user.avatar]] placeholderImage:nil];
                }
                
                break;
            case 1:
                cell.detailTextLabel.text = self.user.nickName == NULL ? @"未知":self.user.nickName;
                break;
            case 2:
                cell.detailTextLabel.text = self.user.idNumber;
                break;
            case 3:
                cell.detailTextLabel.text = self.user.sex == SexMale ? @"男":@"女";
                break;
            case 4:
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@%@%@", self.user.provice, self.user.city, self.user.district, self.user.address];
                break;
            default:
                break;
        }
    }
    if(indexPath.section == 1){
        switch (indexPath.row) {
            case 0:
                cell.detailTextLabel.text = self.user.phone;
                break;
            case 1:
                cell.detailTextLabel.text = self.user.qq;
                break;
            case 2:
                cell.detailTextLabel.text = self.user.email;
                break;
            default:
                break;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[SMUserDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@""];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UILabel *topLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
    topLine.backgroundColor = [UIColor colorWithHexColorString:@"e0e0e0"];
    [cell addSubview:topLine];
    if(indexPath.row == 0 && indexPath.section == 0){
        [cell addSubview:self.avatar];
        [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(self.avatar.frame.size);
            make.centerY.mas_equalTo(cell);
            make.trailing.mas_equalTo(cell).offset(-30);
        }];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *list = self.menus[section];
    return list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.menus.count;
}


- (UIImageView *)avatar{
    if(!_avatar){
        UIImage *img = [UIImage imageNamed:@"littleAvatar"];
        _avatar = [[UIImageView alloc] initWithImage:img];
        //傻逼ui
        CGSize size = CGSizeMake(38, 38);
        _avatar.frame = CGRectMake(0, 0, size.width, size.width);
        _avatar.layer.cornerRadius = size.width/2.0;
        _avatar.clipsToBounds = true;
    }
    return _avatar;
}
- (UITableView *)table{
    if(!_table){
        _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _table.delegate = self;
        _table.dataSource = self;
        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _table;
}


-(SMUser *)user{
    return _user;
}
-(void)setUser:(SMUser *)user{
    _user =user;
    [self.table reloadData];
}
@end
