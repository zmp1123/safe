//
//  SMChangeNameViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/31.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMChangeNameViewController.h"
#import "SMAddDeviceTextField.h"
#import "SMUserMgr.h"

@interface SMChangeNameViewController ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation SMChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    self.title = @"修改姓名";
    UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
    [submit setTitle:@"提交" forState:UIControlStateNormal];
    [submit sizeToFit];
    [submit addTarget:self action:@selector(onSubmitClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:submit];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    [self.view addSubview:self.textField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)onSubmitClick{
    NSString *newName = self.textField.text;
    if([newName isEqualToString:@""]) return;
    SMUserMgr *mgr = [SMUserMgr instance];
    [mgr updateUserInfoNickName:newName idNumber:nil avatar:nil email:nil sex:100 qq:nil province:nil city:nil distrit:nil addr:nil success:^(SMUser *usr) {
        [self.navigationController popViewControllerAnimated:true];
    } fail:^(NSString *msg) {
        [self simpleAlertString:msg];
    }];
}

- (UITextField *)textField{
    if(!_textField){
        _textField = [[SMAddDeviceTextField alloc] initWithFrame:CGRectMake(0, 20, kScreenWidth, 45)];
        UILabel *top = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        top.backgroundColor = [UIColor colorWithHexColorString:@"c7c7c7"];
        [_textField addSubview:top];
        UILabel *bot = [[UILabel alloc] initWithFrame:CGRectMake(0, 44.5, kScreenWidth, 0.5)];
        bot.backgroundColor = [UIColor colorWithHexColorString:@"c7c7c7"];
        [_textField addSubview:bot];
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.placeholder = @"请输入姓名";
        _textField.text = self.defaultName == NULL ? @"" : self.defaultName;
    }
    return _textField;
}

@end
