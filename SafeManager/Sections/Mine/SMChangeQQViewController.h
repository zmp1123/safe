//
//  SMChangeQQViewController.h
//  SafeManager
//
//  Created by zxf on 16/11/1.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMChangeQQViewController : SMBaseViewController

@property (nonatomic, copy) NSString *defaultQQ;

@end
