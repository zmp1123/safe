//
//  SMAddDeviceViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMAddDeviceViewController.h"
#import "UIColor+Category.h"
#import "SMAddDeviceTextField.h"
#import <Masonry.h>
#import "SMDeviceMgr.h"
#import "EZAddByQRCodeViewController.h"

@interface SMAddDeviceViewController ()<EZAddByQRCodeViewControllerDelegate>

@property (nonatomic, strong) UITextField *nameField;
@property (nonatomic, strong) UITextField *serialNoField;
@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation SMAddDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加设备";
    self.view.backgroundColor = [UIColor colorWithHexColorString:@"f2f3f7"];
    [self.view addSubview:self.nameField];
    [self.view addSubview:self.serialNoField];
    [self.view addSubview:self.submitBtn];
    
    [self.serialNoField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.serialNoField.bounds.size);
        make.top.mas_equalTo(self.nameField.mas_bottom).offset(0);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.submitBtn.bounds.size);
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.serialNoField.mas_bottom).offset(12);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addDeviceByInput:) name:@"addDeviceByInput" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addDeviceByInput" object:nil];
}

- (void)addDeviceByInput:(NSNotification *)noti
{
    self.serialNoField.text = noti.object;
}

- (void)deviceArmBind
{
    if (self.nameField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请填写设备名称" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (self.serialNoField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请填写设备序列号" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SMDeviceMgr instance] deviceArmBind:self.serialNoField.text deviceTitle:self.nameField.text success:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showSuccess:@"添加成功" toView:self.view];
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(back) userInfo:nil repeats:NO];
    } fail:^(NSString *msg) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:msg toView:self.view];
    }];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)toAddViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"AddDevice" bundle:nil];
    EZAddByQRCodeViewController *vc = [sb instantiateViewControllerWithIdentifier:@"AddByQRCode"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - EZAddByQRCodeViewControllerDelegate
- (void)addDeviceByQRCode:(NSString *)str
{
    self.serialNoField.text = str;
}

#pragma mark - properties
- (UITextField *)nameField{
    if(!_nameField){
        _nameField = [[SMAddDeviceTextField alloc]initWithFrame:CGRectMake(0, 10, kScreenWidth, 49)];
        _nameField.placeholder = @"请输入设备名";
        UILabel *line  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithHexColorString:@"c0c0c0"];
        [_nameField addSubview:line];
    }
    return _nameField;
}

- (UITextField *)serialNoField{
    if(!_serialNoField){
        _serialNoField = [[SMAddDeviceTextField alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 49)];
        _serialNoField.placeholder = @"请输入序列号或扫描二维码";
        UILabel *line  = [[UILabel alloc]initWithFrame:CGRectMake(0, .50, kScreenWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithHexColorString:@"c0c0c0"];
        [_serialNoField addSubview:line];
        UILabel *line1  = [[UILabel alloc]initWithFrame:CGRectMake(0, 48.5, kScreenWidth, 0.5)];
        line1.backgroundColor = [UIColor colorWithHexColorString:@"c0c0c0"];
        [_serialNoField addSubview:line1];
        UIButton *quard = [UIButton buttonWithType:UIButtonTypeCustom];
        [quard setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
        [quard sizeToFit];
        [quard addTarget:self action:@selector(toAddViewController) forControlEvents:UIControlEventTouchUpInside];
        _serialNoField.rightView = quard;
        _serialNoField.rightViewMode = UITextFieldViewModeAlways;
    }
    return _serialNoField;
}

- (UIButton *)submitBtn{
    if(!_submitBtn){
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.frame = CGRectMake(0, 0, kScreenWidth - 30, 38);
        _submitBtn.backgroundColor = [UIColor colorWithHexColorString:@"5997db"];
        _submitBtn.layer.cornerRadius = 10;
        [_submitBtn setTitle:@"添加" forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(deviceArmBind) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitBtn;
}
@end
