//
//  SMAddDeviceTextField.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMAddDeviceTextField.h"

@implementation SMAddDeviceTextField

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    return self;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds{
    return CGRectMake(bounds.size.width - 50, 0, 50, 50);
}

- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectMake(10, 0, bounds.size.width - 10, bounds.size.height);
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds{
    return CGRectMake(10, 0, bounds.size.width - 10, bounds.size.height);
}
- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectMake(10, 0, bounds.size.width - 10, bounds.size.height);
}
@end
