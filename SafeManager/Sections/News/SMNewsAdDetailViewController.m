//
//  SMNewsAdDetailViewController.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/23.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMNewsAdDetailViewController.h"

@interface SMNewsAdDetailViewController ()

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation SMNewsAdDetailViewController

- (void)viewDidLoad {
    self.title = @"新闻";
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://139.196.234.54/Home/Article/ad?id=%@", self.adId]]];
    [self.webView loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIWebView *)webView{
    if(!_webView){
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    }
    return _webView;
}

@end
