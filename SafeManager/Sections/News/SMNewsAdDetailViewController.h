//
//  SMNewsAdDetailViewController.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/23.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMNewsAdDetailViewController : SMBaseViewController

@property (nonatomic, copy) NSString *adId;

@end
