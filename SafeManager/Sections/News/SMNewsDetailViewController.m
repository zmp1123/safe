//
//  SMNewsDetailViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMNewsDetailViewController.h"

@interface SMNewsDetailViewController ()

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation SMNewsDetailViewController

- (void)viewDidLoad {
    self.title = @"新闻";
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://139.196.234.54/Home?c=Article&id=%@", self.newsId]]];
    [self.webView loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIWebView *)webView{
    if(!_webView){
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    }
    return _webView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
