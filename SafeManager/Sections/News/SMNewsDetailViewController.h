//
//  SMNewsDetailViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseViewController.h"

@interface SMNewsDetailViewController : SMBaseViewController

@property (nonatomic, copy) NSString *newsId;

@end
