//
//  SMTabViewController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMTabViewController.h"
#import "SMHomeViewController.h"
#import "SMNavigationController.h"
#import "SMMineViewController.h"
#import "SMInsuranceViewController.h"
#import "SMMonitorViewController.h"

@interface SMTabViewController ()<UITabBarControllerDelegate>

@end

@implementation SMTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SMHomeViewController *homeCtr = [SMHomeViewController new];
    homeCtr.title = @"首页";
    homeCtr.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:[UIImage imageNamed:@"homeIconGray"] selectedImage:[UIImage imageNamed:@"homeIconBlue"]];
    homeCtr.tabBarItem.selectedImage = [[UIImage imageNamed:@"homeIconBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    SMNavigationController *homeNav = [[SMNavigationController alloc] initWithRootViewController:homeCtr];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMMonitorStoryboard" bundle:nil];
    SMMonitorViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMMonitorViewController"];
    vc.title = @"监控";
    vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"监控" image:[UIImage imageNamed:@"monitorIconGray"] selectedImage:[UIImage imageNamed:@"monitorIconBlue"]];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:@"monitorIconBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    SMNavigationController *monitorNav = [[SMNavigationController alloc] initWithRootViewController:vc];
    
    SMInsuranceViewController *insuranceCtr = [SMInsuranceViewController new];
    insuranceCtr.title = @"保险";
    insuranceCtr.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"保险" image:[UIImage imageNamed:@"insuranceIconGray"] selectedImage:[UIImage imageNamed:@"insuranceIconBlue"]];
    insuranceCtr.tabBarItem.selectedImage = [[UIImage imageNamed:@"insuranceIconBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    SMNavigationController *insuranceNav = [[SMNavigationController alloc] initWithRootViewController:insuranceCtr];
    
    SMMineViewController *mineCtr = [SMMineViewController new];
    mineCtr.title = @"我";
    mineCtr.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我" image:[UIImage imageNamed:@"mineIconGray"] selectedImage:[UIImage imageNamed:@"mineIconBlue"]];
    mineCtr.tabBarItem.selectedImage = [[UIImage imageNamed:@"mineIconBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    SMNavigationController *mineNav = [[SMNavigationController alloc]initWithRootViewController:mineCtr];
    
    
    [self setViewControllers:@[homeNav, monitorNav, insuranceNav, mineNav]];
    self.delegate = self;
    self.selectedIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
