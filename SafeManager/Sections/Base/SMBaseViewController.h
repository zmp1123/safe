//
//  SMBaseViewController.h
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SystemMarco.h"
#import "UIColor+Category.h"
#import <Masonry.h>
#import "MBProgressHUD.h"
#import "MBProgressHUD+MJ.h"

@interface SMBaseViewController : UIViewController

@property (nonatomic, strong) MBProgressHUD *hub;

- (void)simpleAlertString:(NSString *)msg;

@end
