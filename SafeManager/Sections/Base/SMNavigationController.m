//
//  SMNavigationController.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMNavigationController.h"
#import "UINavigationBar+Extension.h"
#import "UIColor+Category.h"

@interface SMNavigationController ()

@end

@implementation SMNavigationController

+ (void)initialize{
    if (self == [SMNavigationController class]) {
        [self setupNavigationBar];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([self.viewControllers count]>0)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateHighlighted];
        [backButton addTarget:self action:@selector(backViewCtrl) forControlEvents:UIControlEventTouchUpInside];
        
        CGSize imageSize = backButton.currentBackgroundImage.size;
        
        backButton.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)backViewCtrl
{
    [self popViewControllerAnimated:YES];
}

+ (void)setupNavigationBar{
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    
    NSMutableDictionary *titleTextAttributes = [NSMutableDictionary dictionary];
    titleTextAttributes[NSForegroundColorAttributeName] = [UIColor whiteColor];
    titleTextAttributes[NSFontAttributeName] = [UIFont systemFontOfSize:17.0];
    navigationBar.titleTextAttributes = titleTextAttributes;
    
    navigationBar.barTintColor = [UIColor colorWithHexColorString:@"5696de"];
    navigationBar.translucent = NO;
    navigationBar.barStyle = UIBarStyleBlack;
    navigationBar.tintColor = [UIColor whiteColor];

    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
