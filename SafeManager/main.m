//
//  main.m
//  SafeManager
//
//  Created by zxf on 16/10/2.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
