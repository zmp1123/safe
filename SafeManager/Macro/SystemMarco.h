//
//  SystemMarco.h
//  ReNao
//
//  Created by zmp1123 on 10/8/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

/**
 *  与系统相关的宏定义
 */
#ifndef RedPass_SystemMarco_h
#define RedPass_SystemMarco_h


/**
 *  获取手机屏幕尺寸
 */
#define kScreenFrame    [UIScreen mainScreen].bounds

/**
 *  获取手机屏幕宽度
 */
#define kScreenWidth    (kScreenFrame.size.width)

/**
 *  获取手机屏幕高度
 */
#define kScreenHeight   (kScreenFrame.size.height)

/**
 *  状态栏高度
 */
#define kStatusBarHeight     20.0

/**
 *  导航条高度
 */
#define kNavigationBarHeight 44.0

/**
 *  tabbar高度
 */
#define kTabBarHeight        49.0


/*********************************************/
/****************关于系统版本判断****************/
/*********************************************/

/**
 *  判断当前设备版本是否等于v
 */
#define SYSTEM_VERSION_EQUAL_TO(v)                    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
/**
 *  判断当前设备版本是否大于v
 */
#define SYSTEM_VERSION_GREATER_THAN(v)                ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
/**
 *  判断当前设备版本是否大于等于v
 */
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
/**
 *  判断当前设备版本是否小于v
 */
#define SYSTEM_VERSION_LESS_THAN(v)                   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
/**
 *  判断当前设备版本是否小于等于v
 */
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)       ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/**
 *  判断当前iPhone/iPad是否是iOS6系统
 */
#define iOS6 (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6") && SYSTEM_VERSION_LESS_THAN(@"7"))
/**
 *  判断当前iPhone/iPad是否是iOS7系统
 */
#define iOS7 (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") && SYSTEM_VERSION_LESS_THAN(@"8"))
/**
 *  判断当前iPhone/iPad是否是iOS8系统
 */
#define iOS8 (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8") && SYSTEM_VERSION_LESS_THAN(@"9"))


/*************************************/
/****************other****************/
/*************************************/

/**
 *  颜色r，g，b三个参数。
 */
#define color(r,g,b) [UIColor colorWithRed:(r) green:(g) blue:(b) alpha:1.0]

/**
 *  语言
 */
#define RNNSLocalizedString(string,defaultString) [[RNSoftwareTools defaultServices]getTheLanguageStringByIdentifierString:(string)andDefaultString:(defaultString)]

#define sCreen_scale kScreenWidth / 375.00

#endif
