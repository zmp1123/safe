//
//  SMDeviceRecord.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDeviceRecord.h"

@implementation SMDeviceRecord

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.op = data[@"operator"];
    self.type_name = data[@"type_name"];
    self.log_time = data[@"log_time"];
    return self;
}

@end
