//
//  SMBaoJing.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMBaoJing : SMModel

/*
 {
 "app_user_id" = 25;
 "create_time" = "2016-09-16 17:16:13";
 "deal_sn" = VIP2016091617161335122;
 "deal_status" = 1;
 desc = "";
 "device_arm_id" = 1;
 "end_date" = "2017-04-01";
 flag = 0;
 id = 32;
 "order_time" = "2016-09-16 17:16:13";
 "pay_time" = "";
 price = 666;
 "start_date" = "2016-11-03";
 "vip_detail" =             {
    description = "";
    duration = 99;
    id = 1;
    price = 666;
    title = "\U7ec8\U8eab\U670d\U52a1\U8d39";
 };
 "vip_id" = 1;
 "vip_title" = "\U7ec8\U8eab\U670d\U52a1\U8d39";
 }
 */

@property (nonatomic, copy) NSString *deal_sn;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *vip_title;
@property (nonatomic, copy) NSString *oid;

@property (nonatomic, copy) NSString *device_arm_sn;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *end_date;

@end
