//
//  SMChuJing.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMChuJing : SMModel

/*
 "app_user_id" = 25;
 "create_time" = "2016-11-03 22:04:20";
 "deal_sn" = SRV2016090217103461649;
 "deal_status" = 1;
 desc = "";
 "device_arm_id" = 1;
 "end_date" = "2017-02-01";
 flag = 0;
 id = 1;
 "order_time" = "2016-11-01 22:04:00";
 "pay_time" = "2016-11-02 22:04:06";
 price = 500;
 "service_addr" = "\U6d4b\U8bd5";
 "service_com_alarm_tel" = "0551-62632110";
 "service_com_id" = 2;
 "service_com_title" = "\U5408\U80a5\U81f4\U62a4\U5b89\U9632\U6280\U672f\U6709\U9650\U516c\U53f8";
 "service_duration" = 1;
 "start_date" = "2016-11-02";
 */

/*
 "app_user_id" = 25;
 "create_time" = "2016-11-03 22:04:20";
 "deal_sn" = SRV2016090217103461649;
 "deal_status" = 1;
 desc = "";
 "device_arm_id" = 1;
 "device_arm_sn" = 266A00C603FDB7;
 "device_arm_title" = "\U7ba1\U5bb6\U4fdd";
 "end_date" = "2017-02-01";
 flag = 0;
 id = 1;
 "order_time" = "2016-11-01 22:04:00";
 "pay_time" = "2016-11-02 22:04:06";
 price = 500;
 "service_addr" = "\U6d4b\U8bd5";
 "service_com_alarm_tel" = "0551-62632110";
 "service_com_id" = 2;
 "service_com_title" = "\U5408\U80a5\U81f4\U62a4\U5b89\U9632\U6280\U672f\U6709\U9650\U516c\U53f8";
 "service_duration" = 1;
 "start_date" = "2016-11-02";
 */

@property (nonatomic, copy) NSString *deal_sn;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *service_com_title;
@property (nonatomic, copy) NSString *oid;
@property (nonatomic, copy) NSString *service_addr;
@property (nonatomic, copy) NSString *service_com_alarm_tel;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSString *device_arm_sn;

@end
