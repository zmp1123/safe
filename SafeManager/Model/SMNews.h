//
//  SMNews.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMModel.h"

@interface SMNews : SMModel

@property (nonatomic, copy) NSString *newsId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *thumb;
@property (nonatomic, copy) NSString *article;
@property (nonatomic, copy) NSString *createDate;

@end
