//
//  SMSos.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/13.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMSos : SMModel

/*
 {
    id = 34;
    sendMsg =             {
        lat = "30.31256";
        lng = "112.2419";
    };
    sendTime = "2016-11-13 14:47:08";
    sendUser = "\U5c0f\U8349";
    title = "\U5c0f\U8349\U5411\U60a8\U53d1\U9001\U6c42\U52a9\U4fe1\U606f\Uff01";
    type = send;
 }
 */

@property (nonatomic, copy) NSString *sid;
@property (nonatomic, assign) float lat;
@property (nonatomic, assign) float lng;
@property (nonatomic, copy) NSString *sendTime;
@property (nonatomic, copy) NSString *sendUser;
@property (nonatomic, copy) NSString *title;

@end
