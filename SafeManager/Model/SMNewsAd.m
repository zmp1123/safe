//
//  SMNewsAd.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/23.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMNewsAd.h"

@implementation SMNewsAd

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.adid = data[@"id"];
    self.thumb = [NSString stringWithFormat:@"http://139.196.234.54/%@",data[@"thumb"]];
    self.isHasContent = ![data[@"has_content"] boolValue];
    self.url = data[@"url"];
    return self;
}

@end
