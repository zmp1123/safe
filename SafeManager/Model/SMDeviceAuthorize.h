//
//  SMDeviceAuthorize.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMDeviceAuthorize : SMModel

/*
 DeviceId = 1;
 isAuthorize = 0;
 title = "\U7ba1\U5bb6\U4fdd";
 */
@property (nonatomic, copy) NSString *DeviceId;
@property (nonatomic, assign) BOOL isAuthorize;
@property (nonatomic, copy) NSString *title;

@end
