//
//  SMDeviceRecord.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMDeviceRecord : SMModel

/*
 addr = "331\U53f7";
 city = "\U5408\U80a5\U5e02";
 content = "\U8fdc\U7a0b\U5e03\U9632";
 "create_time" = "2016-08-26 12:07:54";
 desc = "";
 "device_arm_title" = "\U7ba1\U5bb6\U4fdd";
 "device_sn" = 266A00C603FDB7;
 district = "\U5305\U6cb3\U533a";
 flag = 0;
 "host_id" = 1;
 "host_type" = 1;
 id = 116690;
 "log_time" = "2016-08-26 12:07:54";
 operator = "\U97e6\U5c0f\U5b9d";
 "operator_id" = 19;
 province = "\U5b89\U5fbd\U7701";
 type = 2;
 "type_name" = "\U5e03\U9632";
 */

@property (nonatomic, copy) NSString *op;
@property (nonatomic, copy) NSString *type_name;
@property (nonatomic, copy) NSString *log_time;

@end
