//
//  SMDeviceAuthorize.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDeviceAuthorize.h"

@implementation SMDeviceAuthorize

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.DeviceId = data[@"DeviceId"];
    self.isAuthorize = [data[@"isAuthorize"] boolValue];
    self.title = data[@"title"];
    return self;
}

@end
