//
//  SMModel.h
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMModel : NSObject

- (instancetype)initWithJson:(NSDictionary*)data;

@end
