//
//  SMServiceComTypeModel.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMServiceComTypeModel.h"

@implementation SMServiceComTypeModel

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    if (self) {
        _title = data[@"title"];
        _parent_id = data[@"parent_id"];
        _level = [data[@"level"] integerValue];
        _tid = data[@"id"];
        NSMutableArray <SMServiceComTypeModel *>* mutableArray = [NSMutableArray array];
        if ([data[@"child"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in data[@"child"]) {
                SMServiceComTypeModel *model = [[SMServiceComTypeModel alloc] initWithJson:dic];
                [mutableArray addObject:model];
            }
        }
        _subList = [mutableArray copy];
    }
    return self;
}

@end
