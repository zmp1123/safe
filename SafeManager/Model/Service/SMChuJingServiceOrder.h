//
//  SMChuJingServiceOrder.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMChuJingServiceOrder : SMModel

/*
 dealId = 4;
 dealSn = SRV2016111021325647658;
 price = 500;
 "service_com_title" = "\U5408\U80a5\U81f4\U62a4\U5b89\U9632\U6280\U672f\U6709\U9650\U516c\U53f8";
 */
@property (nonatomic, copy) NSString *dealId;
@property (nonatomic, copy) NSString *dealSn;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *service_com_title;


@end
