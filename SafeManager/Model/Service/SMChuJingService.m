//
//  SMChuJingService.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingService.h"

@implementation SMChuJingService

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.alarm_tel = data[@"alarm_tel"];
    self.duration = data[@"duration"];
    self.sid = data[@"id"];
    self.price = data[@"price"];
    self.service_com_type_id = data[@"service_com_type_id"];
    self.title = data[@"title"];
    return self;
}

@end
