//
//  SMChuJingService.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMChuJingService : SMModel

/*
 "alarm_tel" = "0551-62632110";
 duration = 1;
 id = 2;
 price = 500;
 "service_com_type_id" = 25;
 title = "\U5408\U80a5\U81f4\U62a4\U5b89\U9632\U6280\U672f\U6709\U9650\U516c\U53f8";
 */

@property (nonatomic, copy) NSString *alarm_tel;
@property (nonatomic, copy) NSString *duration;
@property (nonatomic, copy) NSString *sid;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *service_com_type_id;
@property (nonatomic, copy) NSString *title;

@end
