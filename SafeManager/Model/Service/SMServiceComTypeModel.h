//
//  SMServiceComTypeModel.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMServiceComTypeModel : SMModel

@property (nonatomic, weak) SMServiceComTypeModel *child;
@property (nonatomic, assign) NSInteger level;
@property (nonatomic, copy) NSString *tid;
@property (nonatomic, copy) NSString *parent_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *subList;

@end
