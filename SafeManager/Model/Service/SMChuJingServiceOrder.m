//
//  SMChuJingServiceOrder.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/10.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingServiceOrder.h"

@implementation SMChuJingServiceOrder

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.dealId = data[@"dealId"];
    self.dealSn = data[@"dealSn"];
    self.price = data[@"price"];
    self.service_com_title = data[@"service_com_title"];
    return self;
}

@end
