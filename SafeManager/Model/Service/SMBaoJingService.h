//
//  SMBaoJingService.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMBaoJingService : SMModel

/*
 description = "";
 duration = 2;
 id = 2;
 price = 380;
 title = "2\U5e74\U670d\U52a1\U8d39";
 */

@property (nonatomic, copy) NSString *sid;
@property (nonatomic, copy) NSString *duration;
@property (nonatomic, copy) NSString *descript;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *title;

@end
