//
//  SMBaoJingServiceOrder.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMBaoJingServiceOrder : SMModel

/*
 dealId = 37;
 dealSn = VIP2016110920220072498;
 price = 200;
 "vip_title" = "1\U5e74\U670d\U52a1\U8d39";
 */
@property (nonatomic, copy) NSString *dealId;
@property (nonatomic, copy) NSString *dealSn;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *vip_title;

@end
