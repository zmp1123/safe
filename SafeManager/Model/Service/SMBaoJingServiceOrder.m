//
//  SMBaoJingServiceOrder.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJingServiceOrder.h"

@implementation SMBaoJingServiceOrder

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.dealId = data[@"dealId"];
    self.dealSn = data[@"dealSn"];
    self.price = data[@"price"];
    self.vip_title = data[@"vip_title"];
    return self;
}

@end
