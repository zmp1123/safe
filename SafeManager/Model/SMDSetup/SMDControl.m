//
//  SMDControl.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDControl.h"

@implementation SMDControl

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.device_arm_id = data[@"device_arm_id"];
    self.cid = data[@"id"];
    self.index = data[@"index"];
    return self;
}

@end
