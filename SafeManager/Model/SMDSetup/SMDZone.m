//
//  SMDZone.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMDZone.h"

@implementation SMDZone

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.index = data[@"index"];
    self.position = data[@"position"];
    self.bind_device_video_sn = data[@"bind_device_video_sn"];
    self.zone_type = data[@"zone_type"];
    self.alarm_type = data[@"alarm_type"];
    self.zid = data[@"id"];
    return self;
}

@end
