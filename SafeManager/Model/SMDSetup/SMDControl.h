//
//  SMDControl.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/7.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMDControl : SMModel

@property (nonatomic, copy) NSString *device_arm_id;
@property (nonatomic, copy) NSString *cid;          // 遥控器id
@property (nonatomic, copy) NSString *index;

@end
