//
//  SMDZone.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/8.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMDZone : SMModel

/*
 "alarm_type" = "";
 "all_arm" = 0;
 "bind_device_video_sn" = 76aaf2eaf6994926abbdd9b57427f248;
 "device_arm_id" = 16;
 "half_arm" = 0;
 id = 43;
 index = 2;
 position = "\U5927\U5385";
 "twenty_four_hour_arm" = 0;
 "zone_type" = "\U5168\U5e03\U9632\U9632\U533a";
 */

@property (nonatomic, copy) NSString *index;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *bind_device_video_sn;
@property (nonatomic, copy) NSString *zone_type;
@property (nonatomic, copy) NSString *alarm_type;
@property (nonatomic, copy) NSString *zid;

@end
