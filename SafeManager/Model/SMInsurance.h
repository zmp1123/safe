//
//  SMInsurance.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMModel.h"

@interface SMInsurance : SMModel

@property (nonatomic, copy) NSString *title; //名称
@property (nonatomic, assign) float coverage;//保险赔付额度
@property (nonatomic, assign) int duration; //保险时长（单位：年）
@property (nonatomic, assign) float price;  //	价格
@property (nonatomic, copy) NSString* insurance_type; // 保险类型
@property (nonatomic, copy) NSString *desc; // 	介绍
@property (nonatomic, copy) NSString *avatar; //保险图标
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *insuranceId;

@end
