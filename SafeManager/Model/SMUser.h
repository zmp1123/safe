//
//  SMUser.h
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMModel.h"

typedef enum : NSUInteger {
    SexMale,
    SexFemale,
    SexUnknow,
} Sex;

@interface SMUser : SMModel

@property (nonatomic, copy)NSString *address;
@property (nonatomic, copy)NSString *avatar;
@property (nonatomic, copy)NSString *city;
@property (nonatomic, copy)NSString *district;
@property (nonatomic, copy)NSString *email;
@property (nonatomic, copy)NSString *qq;
@property (nonatomic, copy)NSString *nickName;
@property (nonatomic, copy)NSString *provice;
@property (nonatomic, copy)NSString *token;
@property (nonatomic, copy)NSString *userName;
@property (nonatomic, copy)NSString *idNumber;
@property (nonatomic, copy)NSString *phone;
@property (nonatomic, assign)Sex sex;
@property (nonatomic, assign)NSInteger userId;

@property (nonatomic, copy) NSString *ysToken;
@property (nonatomic, assign)BOOL isYsOn;

@end
