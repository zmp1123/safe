//
//  SMDevice.m
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDevice.h"
#import "NSString+Extension.h"

@implementation SMDevice

-(instancetype)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.ac_status = [data[@"ac_status"] integerValue];
    self.addr = data[@"addr"];
    self.arm_status = [data[@"arm_status"] integerValue];
    self.avatar = data[@"avatar"];
    self.battery_status = [data[@"battery_status"]integerValue];
    self.province = data[@"province"];
    self.device_arm_type = data[@"device_arm_type"];
    self.device_sn = data[@"device_sn"];
    self.is_online = [data[@"is_online"] integerValue];
    self.district = data[@"district"];
    self.city = data[@"city"];
    self.firmware_version = data[@"firmware_version"];
    self.device_id = data[@"id"];
    self.insurance_end_date = [data[@"insurance_end_date"] convert2DateFmt:@"yyyy-MM-dd mm:hh:ss"];
    self.last_bind_date = [data[@"last_bind_date"]convert2DateFmt:@"yyyy-MM-dd"];
    self.service_com_id = [data[@"service_com_id"]intValue];
    self.service_end_date = [data[@"service_end_date"]convert2DateFmt:@"yyyy-MM-dd"];
    self.service_status = [data[@"service_status"]integerValue];
    self.signal_status = [data[@"signal_status"]intValue];
    self.sim_card = data[@"sim_card"];
    self.title = data[@"title"];
    self.vip_end_date = data[@"vip_end_date"];
    self.vip_status = [data[@"vip_status"]intValue];
    self.wuye_contact = data[@"wuye_contact"];
    return self;
}

@end
