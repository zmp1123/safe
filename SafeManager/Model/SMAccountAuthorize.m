//
//  SMAccountAuthorize.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMAccountAuthorize.h"

@implementation SMAccountAuthorize

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.aid = data[@"id"];
    self.nickname = data[@"nickname"];
    self.phone = data[@"phone"];
    return self;
}

@end
