//
//  SMNewsAd.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/23.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMNewsAd : SMModel

@property (nonatomic, copy) NSString *adid;
@property (nonatomic, copy) NSString *thumb;
@property (nonatomic, assign) BOOL isHasContent;
@property (nonatomic, copy) NSString *url;

@end
