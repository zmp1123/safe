//
//  SMChuJing.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJing.h"

@implementation SMChuJing

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.deal_sn = data[@"deal_sn"];
    self.price = data[@"price"];
    self.order_time = data[@"order_time"];
    self.service_com_title = data[@"service_com_title"];
    self.oid = data[@"id"];
    self.service_addr = data[@"service_addr"];
    self.service_com_alarm_tel = data[@"service_com_alarm_tel"];
    self.start_date = data[@"start_date"];
    self.end_date = data[@"end_date"];
    self.device_arm_sn = data[@"device_arm_sn"];
    return self;
}

@end
