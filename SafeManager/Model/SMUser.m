//
//  SMUser.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMUser.h"


@implementation SMUser

- (NSString *)emptyString:(NSString *)source{
    return source == NULL ?@"":source;
}
- (instancetype)initWithJson:(NSDictionary *)data{
    self = [super initWithJson:data];
    self.address = [self emptyString:data[@"addr"]];
    NSString *tmp = [self emptyString :data[@"avatar"]];
    if ([tmp containsString:@"http://"]){
        self.avatar = data[@"avatar"];
    }else{
        self.avatar = [NSString stringWithFormat:@"http://139.196.234.54/%@",[self emptyString :data[@"avatar"]]];
    }
    self.city = [self emptyString:data[@"city"]];
    self.district = [self emptyString:data[@"district"]];
    self.email = [self emptyString:data[@"email"]];
    self.userId = [data[@"id"] integerValue];
    self.idNumber = [self emptyString:data[@"id_number"]];
    self.nickName = [self emptyString:data[@"nickname"]];
    self.phone = [self emptyString:data[@"phone"]];
    self.provice = [self emptyString:data[@"province"]];
    self.qq = [self emptyString:data[@"qq"]];
    self.sex = [data[@"sex"] integerValue];
    self.token = data[@"token"];
    self.userName = [self emptyString:data[@"username"]];
    self.ysToken = data[@"ys_access_token"];
    self.isYsOn = [data[@"ys_service_status"]intValue] == 0;
    return self;
}

- (instancetype)init{
    self.address = @"";
    self.avatar = @"";
    self.city = @"";
    self.district = @"";
    self.email = @"";
    self.idNumber = @"";
    self.nickName = @"";
    self.phone = @"";
    self.provice = @"";
    self.qq = @"";
    self.token = @"";
    self.userName = @"";
    return self;
}

@end
