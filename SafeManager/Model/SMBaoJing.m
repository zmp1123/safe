//
//  SMBaoJing.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJing.h"

@implementation SMBaoJing

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.deal_sn = data[@"deal_sn"];
    self.price = data[@"price"];
    self.order_time = data[@"order_time"];
    self.vip_title = data[@"vip_title"];
    self.oid = data[@"id"];
    
    self.device_arm_sn = data[@"device_arm_sn"];
    self.start_date = data[@"start_date"];
    self.end_date = data[@"end_date"];
    return self;
}

@end
