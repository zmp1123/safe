//
//  SMDevice.h
//  SafeManager
//
//  Created by zxf on 16/10/19.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMModel.h"

typedef enum : NSUInteger {
    SMDeviceStatusOn = 0x00,
    SMDeviceStatusOff = 0x01
} SMDeviceStatus;


/**
 * 布撤防状态（-1未知1布防2半布防0撤防）
 */
typedef enum : NSInteger {
    arm_status_unknown = -1,
    arm_status_on = 1,
     arm_status_half = 2,
    arm_status_off = 0,
} arm_status;

/*
 * 电池状态（-1未知0正常1欠压）
 */
typedef enum : NSInteger {
    battery_status_unknown = -1,
    battery_status_normal = 0,
    battery_status_qianya = 1,
} battery_status;


/*
 * 市电状态（-1未知0市电正常1市电断开）
 */
typedef enum : NSInteger {
    ac_status_unknown = -1,
    ac_status_on = 0,
    ac_status_off = 1,
} ac_status;

/*
 * 报警状态（-1未知0正常 1报警)
 */
typedef enum : NSInteger {
    alarm_status_unknown = -1,
    alarm_status_on = 0,
    alarm_status_off = 1,
} alarm_status;

/*
 * 	是否在线（-1未知0在线1不在线)
 */
typedef enum : NSInteger {
    is_online_unknown = -1,
    is_online_on = 0,
    is_online_off = 1,
} is_online;

/*
 * 服务模式 （0出警 1 不出警）
 */
typedef enum : NSUInteger {
    service_status_on,
    service_status_off,
} service_status;

@interface SMDevice : SMModel

@property (nonatomic, copy) NSString *device_id;
@property (nonatomic,copy) NSString *agent_id;      //运营商ID
@property (nonatomic,copy) NSString *device_sn;     //设备序列号
@property (nonatomic,copy) NSString *app_user_id;   //APP用户ID
@property (nonatomic,copy) NSString *authorize_app_user_ids; // 授权APP用户IDS，JSON格式 {"ids":[{"id":111},{"id":222} ]}
@property (nonatomic,copy) NSString *sim_card;              //SIM流量卡卡号
@property (nonatomic,copy) NSString *title;             //设备昵称
@property (nonatomic,copy) NSString *avatar;            //设备头像
@property (nonatomic,copy) NSString *device_arm_type; //型号
@property (nonatomic,copy) NSString *firmware_version; //软件版本
@property (nonatomic,copy) NSString *wuye_contact; //物业电话号码
@property (nonatomic,copy) NSString *province;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *district;
@property (nonatomic, copy) NSString *addr;
@property (nonatomic, assign) arm_status arm_status; //布撤防状态（-1未知1布防2半布防0撤防）
@property (nonatomic, assign) int signal_status;    //信号强度 0-5 （-1未知 2-5格）
@property (nonatomic, assign) battery_status battery_status;    //电池状态（-1未知0正常1欠压）
@property (nonatomic, assign) ac_status ac_status;  //市电状态（-1未知0市电正常1市电断开）
@property (nonatomic, assign) is_online is_online;  //是否在线（-1未知0在线1不在线）
@property (nonatomic, assign) alarm_status alarm_status;    //报警状态（-1未知0正常 1报警）
@property (nonatomic, strong) NSDate *last_alarm_time;  //最后报警时间
@property (nonatomic, assign) int internet_type_id; //入网类型ID
@property (nonatomic, assign) service_status service_status; //服务模式 （0出警 1 不出警）
@property (nonatomic, assign) int service_com_id;           //出警服务商ID
@property (nonatomic, strong) NSDate *service_end_date;     //出警服务结束时间
@property (nonatomic, assign) int with_vip_balance;        //附带（报警服务）会员时长：0无 !0 时长(天) 设备首次被绑定时开始计算
@property (nonatomic, assign) int vip_status;               //会员状态（0开通1未开通）
@property (nonatomic, strong) NSDate *vip_end_date;          //	会员到期时间
@property (nonatomic, assign) int insurance_status;         //保险状态：0开通1未开通
@property (nonatomic, strong) NSDate *insurance_end_date;  //保险到期时间
@property (nonatomic, strong) NSDate *import_date;          //导入时间
@property (nonatomic, strong) NSDate *first_bind_date;      //首次绑定时间
@property (nonatomic, strong) NSDate *last_bind_date;       //	最近一次绑定时间
//todo
//保险状态、服务模式

@end
