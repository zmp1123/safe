//
//  SMAccountAuthorize.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/4.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMAccountAuthorize : SMModel

/*
 id = 19;
 nickname = "\U7ba1\U4fee\U5cad";
 phone = 18156576455;
 */
@property (nonatomic, copy) NSString *aid;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *phone;

@end
