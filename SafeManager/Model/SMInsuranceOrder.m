//
//  SMInsuranceOrder.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/5.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMInsuranceOrder.h"

@implementation SMInsuranceOrder

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.deal_sn = data[@"deal_sn"];
    self.price = data[@"price"];
    self.order_time = data[@"order_time"];
    self.ins_title = data[@"ins_title"];
    self.people = data[@"people"];
    self.addr_home = data[@"addr_home"];
    self.id_number = data[@"id_number"];
    self.ins_deal_sn = data[@"ins_deal_sn"];
    self.start_date = data[@"start_date"];
    self.end_date = data[@"end_date"];
    self.property = data[@"property"];
    self.coverage = data[@"coverage"];
    self.iid = data[@"id"];
    return self;
}

@end
