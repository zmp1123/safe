//
//  SMInsuranceOrder.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/5.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMModel.h"

@interface SMInsuranceOrder : SMModel

/*
 "addr_home" = "\U5b89\U5fbd\U7701\U5408\U80a5\U5e02\U5e90\U9633\U533a\U5e78\U798f\U822a\U7a7a";
 "addr_ins" = "\U5b89\U5fbd\U7701\U5408\U80a5\U5e02\U5305\U6cb3\U533a331\U53f7";
 "addr_shop" = "";
 "agent_id" = 6;
 "app_user_id" = 25;
 count = 1;
 coverage = 100000;
 "create_time" = "2016-09-30 15:27:35";
 "deal_sn" = INS2016093015273597212;
 "deal_status" = 1;
 desc = "";
 "device_arm_id" = 1;
 "duration_count" = 1;
 email = "642338923@qq.com";
 "end_date" = "";
 flag = 0;
 id = 231;
 "id_number" = 34082219901201610;
 "ins_deal_sn" = "";
 "ins_detail" =             {
    avatar = "";
    coverage = 100000;
    "create_time" = "2016-07-19 08:47:03";
    desc = "";
    description = "";
    duration = 2;
    flag = 0;
    id = 4;
    "insurance_type" = "\U76d7\U62a2\U9669";
    price = 100;
    title = "\U5e73\U5b89\U4fdd\U966910\U4e07\U76d7\U62a2\U9669";
 };
 "ins_id" = 4;
 "ins_title" = "\U5e73\U5b89\U4fdd\U966910\U4e07\U76d7\U62a2\U9669";
 "order_time" = "2016-09-30 15:27:35";
 "pay_time" = "";
 people = "\U5c0f\U8349";
 "people_type" = "\U4e2a\U4eba";
 phone = 17755105477;
 price = 100;
 property = "\U4e00\U653e\U5047";
 "start_date" = "";
 status = 0;

 */

@property (nonatomic, copy) NSString *deal_sn;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *ins_title;

@property (nonatomic, copy) NSString *people;
@property (nonatomic, copy) NSString *addr_home;
@property (nonatomic, copy) NSString *id_number;
@property (nonatomic, copy) NSString *ins_deal_sn;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSString *property;
@property (nonatomic, copy) NSString *coverage;

@property (nonatomic, copy) NSString *iid;
@end
