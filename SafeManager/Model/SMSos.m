//
//  SMSos.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/13.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMSos.h"

@implementation SMSos

- (id)initWithJson:(NSDictionary *)data
{
    self = [super initWithJson:data];
    self.sid = data[@"id"];
    self.lat = [data[@"sendMsg"][@"lat"] floatValue];
    self.lng = [data[@"sendMsg"][@"lng"] floatValue];
    self.sendTime = data[@"sendTime"];
    self.sendUser = data[@"sendUser"];
    self.title = data[@"title"];
    return self;
}

@end
