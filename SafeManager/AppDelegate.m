//
//  AppDelegate.m
//  SafeManager
//
//  Created by zxf on 16/10/2.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "AppDelegate.h"
#import "SMTabViewController.h"
#import "SMLoginViewController.h"
#import "SMUserMgr.h"
#import "SMSignInViewController.h"
#import "SMAddInsuranceViewController.h"
#import "EZOpenSDK.h"
#import "WXApi.h"
#import "WXApiManager.h"
#import "UMessage.h"
#import <UserNotifications/UserNotifications.h>

#import "SMSosViewController.h"
#import "SMSos.h"
#import "SMDeviceActionViewController.h"
#import "SMDArmZoneViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <AudioToolbox/AudioToolbox.h> 

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@property (nonatomic, strong) SMTabViewController* tabBarCtrl;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    self.tabBarCtrl = [SMTabViewController new];
    
    [self initEZOpenSDK];
    [self initWxPay];
    [self initUMessage:launchOptions];
    [self initMap];
    
    SMUserMgr *userMgr = [SMUserMgr instance];
    [userMgr queryUserSuccess:^(SMUser *user) {
        
    } fail:^(NSString *msg) {
        
    }];
    
    if(userMgr.lastUserName && userMgr.lastUserPwd){
        [self showMain];
//       [userMgr loginWithUserName:userMgr.lastUserName
//                         password:userMgr.lastUserPwd
//                          success:^(id res) {
//                              NSLog(@"login success!");
//       }
//                             fail:^(NSString *msg) {
//                                 SMLoginViewController *loginCtr = [SMLoginViewController new];
//                                 self.window.rootViewController = loginCtr;
//       }];
    }
    else{
        SMLoginViewController *loginCtr = [SMLoginViewController new];
        self.window.rootViewController = loginCtr;
    }
    
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)showMain{
     self.window.rootViewController = [SMTabViewController new];
}

-(void)showSignIn{
    self.window.rootViewController = [SMSignInViewController new];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"alipayResult" object:resultDic];
        }];
    }
    if ([url.host isEqualToString:@"platformapi"]){//支付宝钱包快登授权返回authCode
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"alipayResult" object:resultDic];
        }];
    }
    
    return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
}

- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

#pragma mark - 接收通知
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [UMessage registerDeviceToken:deviceToken];
}

//iOS10以下使用这个方法接收通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    /*
     {
        aps =     {
            alert = "\U516c\U53f8\U8fdc\U7a0b\U5e03\U9632\Uff01";
        };
        "create_time" = "2016-11-13 01:30:26";
        d = uu29756147902232163301;
        deviceArmId = 16;
        deviceArmTitle = "\U516c\U53f8";
        operator = "\U5c0f\U8349";
        p = 0;
        type = 2;
     }
     */
    //关闭友盟自带的弹出框
    [UMessage setAutoAlert:NO];
    [UMessage didReceiveRemoteNotification:userInfo];
    
    [self handleNotificationMessage:userInfo];
}


//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于前台时的远程推送接受
        //关闭友盟自带的弹出框
        [UMessage setAutoAlert:NO];
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        [self handleNotificationMessage:userInfo];
        
    }else{
        //应用处于前台时的本地推送接受
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        [self handleNotificationMessage:userInfo];
        
    }else{
        //应用处于后台时的本地推送接受
    }
    
}

- (void)handleNotificationMessage:(NSDictionary *)userInfo
{
    if ([userInfo[@"type"] integerValue] == 1) {
        // SOS推送
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMSosStoryboard" bundle:nil];
        SMSosViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMSosViewController"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }else if ([userInfo[@"type"] integerValue] >= 2 && [userInfo[@"type"] integerValue] <= 4){
        // 布撤防
        SMDeviceActionViewController *vc = [[SMDeviceActionViewController alloc] init];
        vc.deviceArmId = userInfo[@"deviceArmId"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }else if ([userInfo[@"type"] integerValue] == 5){
        // 报警消息
//        SMDeviceActionViewController *vc = [[SMDeviceActionViewController alloc] init];
//        vc.deviceArmId = userInfo[@"deviceArmId"];
//        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@ 正在报警",userInfo[@"operator"]] message:[NSString stringWithFormat:@"警情:%@,时间:%@",userInfo[@"deviceArmTitle"],userInfo[@"create_time"]] preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *shebeijiejing = [UIAlertAction actionWithTitle:@"设备接警" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *jiejinjinghao = [UIAlertAction actionWithTitle:@"解禁警号" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *chefangjiejing = [UIAlertAction actionWithTitle:@"撤防接警" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:shebeijiejing];
        [alert addAction:jiejinjinghao];
        [alert addAction:chefangjiejing];
        [alert addAction:cancel];
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        
    }else if ([userInfo[@"type"] integerValue] >= 6 || [userInfo[@"type"] integerValue] <= 7){
        // 市电消息
        SMDeviceActionViewController *vc = [[SMDeviceActionViewController alloc] init];
        vc.deviceArmId = userInfo[@"deviceArmId"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }else if ([userInfo[@"type"] integerValue] >= 8 || [userInfo[@"type"] integerValue] <= 9){
        // 推送上线||离线消息
        SMDeviceActionViewController *vc = [[SMDeviceActionViewController alloc] init];
        vc.deviceArmId = userInfo[@"deviceArmId"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }else if ([userInfo[@"type"] integerValue] >= 10 || [userInfo[@"type"] integerValue] <= 12){
        // 对码操作消息
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDArmZoneViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDArmZoneViewController"];
        vc.deviceId = userInfo[@"deviceArmId"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }else if ([userInfo[@"type"] integerValue] == 13){
        // 接警回调消息
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SMDeviceSetupStoryboard" bundle:nil];
        SMDArmZoneViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SMDArmZoneViewController"];
        vc.deviceId = userInfo[@"deviceArmId"];
        [((SMTabViewController *)self.window.rootViewController).selectedViewController pushViewController:vc animated:YES];
    }
}
//- (void)playSound:(NSString *)soundName
//{
//    NSString *path = [[NSBundle mainBundle] pathForResource:soundName ofType:@"wav"];
//    //组装并播放音效
//    SystemSoundID soundID;
//    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
//    AudioServicesPlaySystemSound(soundID);
//    //声音停止
//    //AudioServicesDisposeSystemSoundID(soundID);
//}

#pragma mark - init

- (void)initEZOpenSDK
{
    [EZOpenSDK initLibWithAppKey:@"2fa1275c79b94df9aed3a4e2c837bb79"];
    
    [EZOpenSDK enableP2P:YES];
    
    [EZOpenSDK setDebugLogEnable:YES];
}
- (void)initWxPay
{
    [WXApi registerApp:@"wxaa69562eee0a4592" withDescription:@"demo 2.0"];
}

- (void)initUMessage:(NSDictionary *)launchOptions
{
    [UMessage startWithAppkey:@"58272134aed1795ed300263b" launchOptions:launchOptions];
    [UMessage registerForRemoteNotifications];
    //iOS10必须加下面这段代码。
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate=self;
    UNAuthorizationOptions types10=UNAuthorizationOptionBadge|  UNAuthorizationOptionAlert|UNAuthorizationOptionSound;
    [center requestAuthorizationWithOptions:types10     completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            //点击允许
            //这里可以添加一些自己的逻辑
        } else {
            //点击不允许
            //这里可以添加一些自己的逻辑
        }
    }];
    
    [UMessage setLogEnabled:YES];
}
- (void)initMap
{
    BOOL ret = [[[BMKMapManager alloc] init] start:@"toSk8kl2VQvWgg6tznXNl3K5vfhmdlg6"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
}
@end
