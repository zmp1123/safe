//
//  SMUserMgr.h
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseNetMgr.h"
#import "SMUser.h"

@interface SMUserMgr : SMBaseNetMgr

- (void)loginWithUserName:(NSString *)name
                 password:(NSString *)pwd
                  success:(void(^)(id res)) success
                     fail:(void(^)(NSString *msg)) fail;

-(void)queryUserSuccess:(void(^)(SMUser *user))success
                   fail:(void(^)(NSString *msg))fail;


/**
 用户退出

 @param success 成功
 */
- (void)appUserLogout:(void (^)(id res))success
                 fail:(void(^)(NSString *msg))fail;

/**
 *  发送验证码
 */
-(void)sendVerifyCodePhoneNo:(NSString*)no success:(void(^)())success fail:(void(^)(NSString*msg))fail;

/**
 *  用户注册
 */
-(void)registerUserPhone:(NSString *)phone
                password:(NSString *)pwd
              verifyCode:(NSString *)code
                 success:(void(^)(SMUser *user))success
                    fail:(void(^)(NSString *msg))fail;

/**
 用户修改密码（忘记密码找回）

 @param verifyCode 验证码
 @param phone 用户名
 @param password md5(密码)
 @param success 成功
 @param fail 失败
 */
- (void)appUserModifyPwdByVerifyCode:(NSString *)verifyCode
                               phone:(NSString *)phone
                            password:(NSString *)password
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail;

/**
 * 上传用户头像
 */
-(void)uploadImageData:(NSData *)data success:(void(^)(NSDictionary *res))success fail:(void(^)(NSString *msg))fail;

/**
 * 修改用户资料
 */
-(void)updateUserInfoNickName:(NSString *)nickName
                     idNumber:(NSString *)idNumber
                       avatar:(NSString *)avatar
                        email:(NSString*) email
                          sex:(int) sex
                           qq:(NSString *)qq
                     province:(NSString *)provice
                         city:(NSString *)city
                      distrit:(NSString *)district
                         addr:(NSString *)addr
                      success:(void(^)(SMUser *usr))success
                         fail:(void(^)(NSString *msg))fail;

@property (nonatomic, strong) SMUser *user;
@property (nonatomic, copy) NSString *lastUserName;
@property (nonatomic, copy) NSString *lastUserPwd;


/**
 帐号授权列表

 @param success 成功
 */
- (void)getAuthorizeAppUserList:(void(^)(NSArray *array))success;

/**
 帐号授权添加

 @param phone   手机号
 @param success 成功
 */
- (void)addAuthorizeAppUser:(NSString *)phone
                    success:(void(^)())success
                       fail:(void(^)(NSString *msg))fail;


/**
 帐号授权删除

 @param authorizeUserId 用户ID
 @param success         成功
 */
- (void)deleteAuthorizeAppUser:(NSString *)authorizeUserId
                       success:(void(^)())success
                          fail:(void(^)(NSString *msg))fail;


/**
 单授权账户-设备授权列表

 @param authorizeUserId 用户ID
 @param success         成功
 */
- (void)getAuthorizeDeviceList:(NSString *)authorizeUserId
                       success:(void(^)(NSArray *array))success;


/**
 单授权账户-设备授权修改(添加/删除)

 @param authorizeUserId 用户ID
 @param deviceId        设备ID
 @param action          操作（0增1删）
 @param success         成功
 */
- (void)updateAuthorizeDevice:(NSString *)authorizeUserId
                     deviceId:(NSString *)deviceId
                       action:(BOOL)action
                      success:(void (^)())success;


/**
 获取萤石token

 @param success 成功
 */
- (void)getYsAccessToken:(void (^)(NSDictionary *response))success;

@end
