//
//  SMDeviceMgr.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseNetMgr.h"
#import "SMDevice.h"
#import "SMNews.h"

typedef enum:NSInteger{
    SMDeviceMessageType_All     = 0,    // 所有
    SMDeviceMessageType_Arm     = 1,    // 布撤防消息
    SMDeviceMessageType_Alarm   = 2,    // 报警消息
}SMDeviceMessageType;

@interface SMDeviceMgr : SMBaseNetMgr

@property (nonatomic, strong) NSArray<SMDevice*> *myDevices;


/**
 报警设备列表

 @param success 成功
 @param fail    失败
 */
- (void)getArmDeviceListSuccess:(void(^)(NSArray *list)) success Fail:(void(^)(NSString *msg))fail;

/**
 报警设备列表

 @param deviceId 设备ID
 @param success  成功
 @param fail     失败
 */
- (void)getDeviceInfo:(NSString *)deviceId Success:(void(^)(SMDevice *info)) success Fail:(void(^)(NSString *msg))fail;

/**
 获取最新消息列表

 @param deviceId  设备ID
 @param page      页数
 @param type      类型（0所有1布撤防消息2报警消息）
 @param startDate 开始日期
 @param endDate   结束日期
 @param success   成功
 */
- (void)getDeviceLastRecord:(NSString *)deviceId
                       page:(NSInteger)page
                       type:(SMDeviceMessageType)type
                  startDate:(NSString *)startDate
                    endDate:(NSString *)endDate
                    success:(void(^)(NSArray *list))success
                       fail:(void(^)(NSString *msg))fail;

/**
 报警设备资料修改

 @param deviceId     设备ID
 @param title        名称
 @param wuye_contact 物业电话
 @param province     省
 @param city         市
 @param district     区
 @param addr         详细地址
 @param success      成功
 */
- (void)setDeviceArmInfo:(NSString *)deviceId
                   title:(NSString *)title
            wuye_contact:(NSString *)wuye_contact
                province:(NSString *)province
                    city:(NSString *)city
                district:(NSString *)district
                    addr:(NSString *)addr
                 success:(void(^)())success
                    fail:(void(^)(NSString *msg))fail;

/**
 报警设备绑定

 @param deviceSn    序列号
 @param deviceTitle 设备标题
 @param success     成功
 */
- (void)deviceArmBind:(NSString *)deviceSn
          deviceTitle:(NSString *)deviceTitle
              success:(void(^)())success
                 fail:(void(^)(NSString *msg))fail;

/**
 报警设备解绑

 @param deviceId 设备ID
 @param success  成功
 */
- (void)deviceArmUnbind:(NSString *)deviceId
                success:(void(^)())success
                   fail:(void(^)(NSString *msg))fail;

/**
 报警设备布防

 @param deviceArmId 设备ID
 */
- (void)setDeviceArmDOTArm:(NSString *)deviceArmId
                   success:(void(^)(NSString *msg))success
                      fail:(void(^)(NSString *msg))fail;

/**
 报警设备半布防

 @param deviceArmId 设备ID
 @param success     成功
 */
- (void)setDeviceArmDOTHalfArm:(NSString *)deviceArmId
                       success:(void(^)(NSString *msg))success
                          fail:(void(^)(NSString *msg))fail;

/**
 报警设备撤防

 @param deviceArmId 设备ID
 @param success     成功
 */
- (void)setDeviceArmDOTDisarm:(NSString *)deviceArmId
                      success:(void(^)(NSString *msg))success
                         fail:(void(^)(NSString *msg))fail;

/**
 防区对码

 @param deviceArmId         设备ID
 @param eDeviceCodeProperty DCPAllArm、DCPHalfArm、DCP24HourArm、
 @param success             成功
 */
- (void)setDeviceArmZoneCodeAdd:(NSString *)deviceArmId
            eDeviceCodeProperty:(NSString *)eDeviceCodeProperty
                        success:(void(^)())success
                           fail:(void(^)(NSString *msg))fail;

/**
 防区删除

 @param deviceId 设备ID
 @param zoneId   防区编号（1-99）
 @param success  成功
 */
- (void)setDeviceArmZoneCodeDel:(NSString *)deviceId
                         zoneId:(NSString *)zoneId
                        success:(void(^)())success
                           fail:(void(^)(NSString *msg))fail;

/**
 遥控器对码

 @param deviceId 设备ID
 @param success  成功
 */
- (void)setDeviceArmRemoteControlAdd:(NSString *)deviceId
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail;

/**
 遥控器删除

 @param deviceId  设备ID
 @param controlId 遥控器编号（1-8）
 @param success   成功
 */
- (void)setDeviceArmRemoteControlDel:(NSString *)deviceId
                           controlId:(NSString *)controlId
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail;

/**
 同步时间

 @param deviceId 设备ID
 @param success  成功
 */
- (void)timeSync:(NSString *)deviceId
         success:(void (^)())success
            fail:(void(^)(NSString *msg))fail;


/**
 更新设备固件

 @param deviceId 设备ID
 @param success  成功
 */
- (void)updateFirmware:(NSString *)deviceId
               success:(void(^)())success
                  fail:(void(^)(NSString *msg))fail;

/**
 接警

 @param deviceId           设备ID
 @param eDeviceAnswerAlarm DAAOnlyAnswer、DAAOnlySilent、DAADisarmAndRemove
 @param success            成功
 */
- (void)answerAlarm:(NSString *)deviceId
 eDeviceAnswerAlarm:(NSString *)eDeviceAnswerAlarm
            success:(void(^)())success;

/**
 获取防区列表

 @param deviceId 设备ID
 @param success  成功
 */
- (void)getZoneList:(NSString *)deviceId
            success:(void(^)(NSArray *list))success;

/**
 更改防区属性（含绑定视频设备）

 @param zoneId               防区ID
 @param position             防区位置
 @param alarm_type           报警类型
 @param bind_device_video_sn 绑定视频设备SN
 @param success              成功
 */
- (void)setZoneProperty:(NSString *)zoneId
               position:(NSString *)position
             alarm_type:(NSString *)alarm_type
   bind_device_video_sn:(NSString *)bind_device_video_sn
                success:(void(^)())success
                   fail:(void(^)(NSString *msg))fail;

/**
 获取遥控器列表

 @param deviceId 设备ID
 @param success  成功
 */
- (void)getControlList:(NSString *)deviceId
               success:(void(^)(NSArray *array))success;

/**
 *  新闻
 */
- (void)getDeviceNewsSuccess:(void(^)(NSArray *list))success Fail:(void(^)(NSString *msg))fail;

/**
 *  新闻详情
 */
- (void)getDeviceNewDetailId:(NSString*)newsId Success:(void(^)(SMNews* news))success Fail:(void(^)(NSString *msg))fail;


/**
 发送求助信息【废弃】

 @param authorizeUserId 用户ID
 @param deviceId 设备ID
 @param action 操作（0增1删）
 */
- (void)sendSosWithAuthorizeUserId:(NSInteger)authorizeUserId
                          deviceId:(NSString *)deviceId
                            action:(NSInteger)action
                           success:(void(^)())success
                              fail:(void(^)(NSString *msg))fail;

/**
 发送求助信息

 @param lat 经度
 @param lng 纬度
 @param success 成功
 @param fail 失败
 */
- (void)sendSosWithLat:(CGFloat)lat
                   lng:(CGFloat)lng
               success:(void(^)())success
                  fail:(void(^)(NSString *msg))fail;

/**
 求助信息列表

 @param success 成功
 */
- (void)getSosList:(void(^)(NSArray *list))success;

/**
 只接警

 @param success 成功
 @param fail 失败
 */
- (void)DAAOnlyAnswer:(void(^)())success
                 fail:(void(^)(NSString *msg))fail;
/**
 只静音

 @param success 成功
 @param fail 失败
 */
- (void)DAAOnlySilent:(void(^)())success
                 fail:(void(^)(NSString *msg))fail;
/**
 接警并撤防并静音

 @param success 成功
 @param fail 失败
 */
- (void)DAADisarmAndRemove:(void(^)())success
                      fail:(void(^)(NSString *msg))fail;

/**
 修改通道名称

 @param mid 消息id号，传入任意整数即可
 @param system key 开放平台appkey值
               sign 签名值
               time UTC时间戳
               ver 协议版本号
 @param method HTTP请求方法
 @param params accessToken 访问校验码
               cameraId 通道id
               cameraName 通道名称
 @param success 成功
 @param fail 失败
 */
- (void)updateCameraNameWithID:(NSInteger)mid
                        system:(NSDictionary *)system
                        method:(NSString *)method
                        params:(NSDictionary *)params
                       success:(void(^)())success
                          fail:(void(^)(NSString *msg))fail;
@end
