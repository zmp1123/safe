//
//  SMBaoJingMgr.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseNetMgr.h"

@class SMBaoJing;
@class SMBaoJingServiceOrder;

@interface SMBaoJingMgr : SMBaseNetMgr


/**
 报警服务历史订单

 @param success 成功
 */
- (void)getBaoJingOrderList:(void (^)(NSArray *))success;


/**
 报警服务订单详情

 @param oid     报警服务订单id
 @param success 成功
 */
- (void)getBaoJingOrderDetail:(NSString *)oid
                      success:(void (^)(SMBaoJing *baojing))success;


/**
 报警服务类型列表

 @param success 成功
 */
- (void)getVipTypeList:(void(^)(NSArray *list))success;


/**
 报警服务订单创建

 @param vipId 报警服务ID
 @param deviceId 设备ID
 @param success 成功
 */
- (void)vipDealCreate:(NSString *)vipId
             deviceId:(NSString *)deviceId
              success:(void(^)(SMBaoJingServiceOrder *order))success;

@end
