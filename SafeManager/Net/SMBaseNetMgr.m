//
//  SMBaseNetMgr.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseNetMgr.h"
#import <AFNetworking.h>
#import "NSString+Extension.h"
#import "AppDelegate.h"
#import "SMLoginViewController.h"

@interface SMBaseNetMgr ()

@property(nonatomic, strong) AFHTTPSessionManager *mgr;
@property(nonatomic, strong) NSDictionary *baseParam;

@end

@implementation SMBaseNetMgr
@synthesize token = _token;

+ (id)instance{
    return nil;
}

- (NSString *)token{
    if(!_token){
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    }
    return _token;
}

- (void)setToken:(NSString *)token{
    _token = token;
    [[NSUserDefaults standardUserDefaults] setObject:_token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)uploadData:(NSData *)data url:(NSString *)url formData:(void (^)(id<AFMultipartFormData>))formBlock params:(NSDictionary *)params success:(void (^)(NSDictionary *))success fail:(void (^)(NSString *))fail{

    NSError *error;
    NSMutableDictionary *reqParams = [[NSMutableDictionary alloc]initWithDictionary:self.baseParam];
    if(params){
        [reqParams addEntriesFromDictionary:params];
    }
    
    NSMutableURLRequest *req = [self.mgr.requestSerializer multipartFormRequestWithMethod:@"POST" URLString:url parameters:reqParams constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        formBlock(formData);
    } error:&error];
    
    AFURLSessionManager *sessionMgr = [[AFURLSessionManager alloc]init];
    sessionMgr.responseSerializer = self.mgr.responseSerializer;
    
//    NSURLSessionDataTask *task = [sessionMgr dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        if(!error){
//            if(success){
//                success(responseObject);
//            }
//        }else{
//            if(fail){
//                fail(error.description);
//            }
//        }
//    }];
//    [task resume];
    NSURLSessionDataTask *task = [self.mgr POST:url parameters:reqParams constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        formBlock(formData);
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(success){
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(fail){
            fail(error.description);
        }
    }];
    
    [task resume];
}

- (void)postUrl:(NSString *)url data:(id)data success:(httpCallBack)successBlock fail:(httpFailBlock)failBlock{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]initWithDictionary:self.baseParam];
    
    NSAssert([data isKindOfClass:[NSDictionary class]], @"para style error");
    [postData addEntriesFromDictionary:data];
    NSURLSessionDataTask *task = [self.mgr POST:url
                                    parameters:postData
                                      progress:^(NSProgress * _Nonnull uploadProgress) {
        
    }
                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           NSDictionary *response = responseObject;
                                           int code = [response[@"status"]intValue];
                                           if(code == 0){
                                               successBlock(response);
                                           }
                                           //token 过期
                                           else if(code == 112){
                                               
                                               SMLoginViewController *loginCtr = [SMLoginViewController new];
                                               AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                               app.window.rootViewController = loginCtr;
                                           }
                                           else{
                                               if(failBlock){
                                                   NSString *msg = response[@"message"];
                                                   failBlock(msg == NULL ? @"" : msg);
                                               }
                                           }
    }
                                       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           NSLog(@"net error:%@", error);
                                           if (failBlock) {
                                               NSString *msg = error.domain;
                                               failBlock(msg == NULL ? @"系统错误" :msg);
                                           }
        
    }];
    [task resume];
    
}

- (void)post2Url:(NSString *)url data:(id)data success:(httpCallBack)successBlock fail:(httpFailBlock)failBlock{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]initWithDictionary:self.baseParam];
    
    NSAssert([data isKindOfClass:[NSDictionary class]], @"para style error");
//    [postData addEntriesFromDictionary:data];
    NSURLSessionDataTask *task = [self.mgr POST:url
                                     parameters:data
                                       progress:^(NSProgress * _Nonnull uploadProgress) {
                                           
                                       }
                                        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                            NSDictionary *response = responseObject;
                                            int code = [response[@"status"]intValue];
                                            if(code == 0){
                                                successBlock(response);
                                            }
                                            //token 过期
                                            else if(code == 112){
                                                
                                            }
                                            else{
                                                if(failBlock){
                                                    NSString *msg = response[@"message"];
                                                    failBlock(msg == NULL ? @"" : msg);
                                                }
                                            }
                                        }
                                        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                            NSLog(@"net error:%@", error);
                                            
                                        }];
    [task resume];
    
}


-(AFHTTPSessionManager *)mgr{
    if(!_mgr){
        _mgr = [AFHTTPSessionManager manager];
        _mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    return _mgr;
}

- (NSDictionary *)baseParam{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    dic[@"appName"] = @"zhihuApp";
    dic[@"version"] = @"1.0.0";
    dic[@"timeStamp"] = [NSString stringWithFormat:@"%f", [NSDate date].timeIntervalSince1970];
    dic[@"imei"] = [[UIDevice currentDevice].identifierForVendor UUIDString];
    const NSString *key = @"7802408150050123";
    dic[@"sign"] = [NSString stringWithFormat:@"%@%@%@", dic[@"appName"], dic[@"timeStamp"], key].md5_32;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]){
          dic[@"token"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    }
    return dic;
}
@end
