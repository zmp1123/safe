//
//  SMPayMgr.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMPayMgr.h"

@implementation SMPayMgr

+ (id)instance{
    static SMPayMgr *ins;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ins = [SMPayMgr new];
    });
    return ins;
}

- (void)getAlipaySignWithPartner:(NSString *)partner
                         service:(NSString *)service
                         success:(void(^)(NSString *sign))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=AliPay&a=getSign";
    NSDictionary *dic = @{@"partner":partner,
                          @"service":service};
    [self postUrl:url data:dic success:^(id json) {
        success(json[@"data"][@"sign"]);
    } fail:^(id error) {
        
    }];
}

- (void)getAlipaySignWithPartner:(NSString *)partner
                         service:(NSString *)service
                        sellerId:(NSString *)sellerId
                      outTradeNo:(NSString *)outTradeNo
                         subject:(NSString *)subject
                            body:(NSString *)body
                        totalFee:(NSString *)totalFee
                         success:(void (^)(NSDictionary *data))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=AliPay&a=getSign";
    NSDictionary *dic = @{@"partner":partner,
                          @"service":service,
                          @"seller_id":sellerId,
                          @"out_trade_no":outTradeNo,
                          @"subject":subject,
                          @"body":body,
                          @"total_fee":totalFee,
                          @"notify_url":@"http://139.196.234.54/AppApi/AliPay/notifyUrl",
                          @"return_url":@"m.alipay.com",
                          @"_input_charset":@"utf-8",
                          @"it_b_pay":@"30m",
                          @"payment_type":@"1"};
    [self post2Url:url data:dic success:^(id json) {
        success(json[@"data"]);
    } fail:^(id error) {
        
    }];
}

/*
 appid = wxaa69562eee0a4592;
 noncestr = LYmOETk8CuklnTRG;
 package = "Sign=WXPay";
 partnerid = 1378289102;
 prepayid = wx20161110155519318a4c716e0261744179;
 sign = EB97A358663E7FDF6D3B720EDD5557D1;
 timestamp = 1478764520;
 */

- (void)getWxPayIndex:(NSString *)WIDout_trade_no
         WIDtotal_fee:(NSString *)WIDtotal_fee
              success:(void(^)(NSDictionary *data))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=WxPay&a=index";
    NSDictionary *dic = @{@"WIDout_trade_no":WIDout_trade_no,
                          @"WIDtotal_fee":WIDtotal_fee};
    [self postUrl:url data:dic success:^(id json) {
        success(json[@"result"]);
    } fail:^(id error) {
        
    }];
}

@end
