//
//  SMDeviceMgr.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMDeviceMgr.h"
#import "SMDevice.h"
#import "SMNews.h"
#import "SMDControl.h"
#import "SMDeviceRecord.h"
#import "SMDZone.h"
#import "SMSos.h"
#import "NSString+Extension.h"
@implementation SMDeviceMgr

- (void)getDeviceInfo:(NSString *)deviceId Success:(void (^)(SMDevice *))success Fail:(void (^)(NSString *))fail{
    
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=getDeviceArmDetail";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        SMDevice *device = [[SMDevice alloc] initWithJson:json[@"result"]];
        success(device);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}


- (void)getDeviceNewDetailId:(NSString *)newsId Success:(void (^)(SMNews *))success Fail:(void (^)(NSString *))fail{
    NSString *url = @"http://139.196.234.54/Home?c=Article";
    NSDictionary *para = @{@"id":newsId};
    [self postUrl:url data:para success:^(id json) {
//        NSLog(@"%@", json);
    } fail:^(id error) {
        
    }];
}

- (void)getDeviceNewsSuccess:(void (^)(NSArray *))success Fail:(void (^)(NSString *))fail{
    NSString *url = @"http://139.196.234.54/AppApi?c=News&a=getNewsList";
    [self postUrl:url data:@{} success:^(NSDictionary* json) {
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"data"]){
            SMNews *news = [SMNews new];
            news.title = item[@"title"];
            news.newsId = item[@"id"];
            news.createDate = item[@"create_time"];
            news.image = item[@"url"];
            news.thumb = [NSString stringWithFormat:@"http://139.196.234.54/%@",item[@"thumb"]];
            [list addObject:news];
        }
        success(list);
    } fail:^(id error) {
        
    }];
    
}

- (void)getArmDeviceListSuccess:(void (^)(NSArray *))success Fail:(void (^)(NSString *))fail{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=getDeviceArmList";
    [self postUrl:url data:@{} success:^(id json) {
        NSDictionary *data = json;
        NSMutableArray *list = [NSMutableArray new];
        for (NSDictionary *item in data[@"result"]) {
            [list addObject:[[SMDevice alloc] initWithJson:item]];
        }
        if(success){
            success(list);
        }
        _myDevices = list;
    } fail:^(id error) {
        NSLog(@"%@", error);
    }];
}

- (void)getDeviceLastRecord:(NSString *)deviceId
                       page:(NSInteger)page
                       type:(SMDeviceMessageType)type
                  startDate:(NSString *)startDate
                    endDate:(NSString *)endDate
                    success:(void(^)(NSArray *list))success
                       fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=getDeviceLastRecord";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"page":[NSNumber numberWithInteger:page],
                          @"type":[NSNumber numberWithInteger:type],
                          @"startDate":startDate?startDate:@"",
                          @"endDate":endDate?endDate:@""};
    [self postUrl:url data:dic success:^(id json) {
        NSDictionary *data = json;
        if ([data[@"result"] isKindOfClass:[NSArray class]]) {
            NSMutableArray *array = [NSMutableArray array];
            for (NSDictionary *item in json[@"result"]) {
                SMDeviceRecord *model = [[SMDeviceRecord alloc] initWithJson:item];
                [array addObject:model];
            }
            success(array);
        }else{
            if (fail) {
                NSString *msg = @"暂无数据";
                fail(msg == NULL ? @"系统错误" :msg);
            }
        }
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmInfo:(NSString *)deviceId
                   title:(NSString *)title
            wuye_contact:(NSString *)wuye_contact
                province:(NSString *)province
                    city:(NSString *)city
                district:(NSString *)district
                    addr:(NSString *)addr
                 success:(void(^)())success
                    fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmInfo";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"title":title,
                          @"wuye_contact":wuye_contact,
                          @"province":province,
                          @"city":city,
                          @"district":district,
                          @"addr":addr};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)deviceArmBind:(NSString *)deviceSn
          deviceTitle:(NSString *)deviceTitle
              success:(void(^)())success
                 fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=deviceArmBind";
    NSDictionary *dic = @{@"deviceSn":deviceSn,
                          @"deviceTitle":deviceTitle};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)deviceArmUnbind:(NSString *)deviceId
                success:(void(^)())success
                   fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=deviceArmUnbind";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmDOTArm:(NSString *)deviceArmId
                   success:(void(^)(NSString *msg))success
                      fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmDOTArm";
    NSDictionary *dic = @{@"deviceArmId":deviceArmId};
    [self postUrl:url data:dic success:^(id json) {
        NSString *msg = json[@"message"];
        success(msg);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmDOTHalfArm:(NSString *)deviceArmId
                       success:(void(^)(NSString *msg))success
                          fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmDOTHalfArm";
    NSDictionary *dic = @{@"deviceArmId":deviceArmId};
    [self postUrl:url data:dic success:^(id json) {
        NSString *msg = json[@"message"];
        success(msg);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmDOTDisarm:(NSString *)deviceArmId
                      success:(void(^)(NSString *msg))success
                         fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmDOTDisarm";
    NSDictionary *dic = @{@"deviceArmId":deviceArmId};
    [self postUrl:url data:dic success:^(id json) {
        NSString *msg = json[@"message"];
        success(msg);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmZoneCodeAdd:(NSString *)deviceArmId
            eDeviceCodeProperty:(NSString *)eDeviceCodeProperty
                        success:(void(^)())success
                           fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmZoneCodeAdd";
    NSDictionary *dic = @{@"deviceArmId":deviceArmId,
                          @"eDeviceCodeProperty":eDeviceCodeProperty};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)setDeviceArmZoneCodeDel:(NSString *)deviceId
                         zoneId:(NSString *)zoneId
                        success:(void(^)())success
                           fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmZoneCodeDel";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"zoneId":zoneId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)setDeviceArmRemoteControlAdd:(NSString *)deviceId
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmRemoteControlAdd";
    NSDictionary *dic = @{@"deviceArmId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)setDeviceArmRemoteControlDel:(NSString *)deviceId
                           controlId:(NSString *)controlId
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setDeviceArmRemoteControlDel";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"controlId":controlId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)timeSync:(NSString *)deviceId
         success:(void (^)())success
            fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=timeSync";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)updateFirmware:(NSString *)deviceId
               success:(void(^)())success
                  fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=updateFirmware";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)answerAlarm:(NSString *)deviceId
 eDeviceAnswerAlarm:(NSString *)eDeviceAnswerAlarm
            success:(void(^)())success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=answerAlarm";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"eDeviceAnswerAlarm":eDeviceAnswerAlarm};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)getZoneList:(NSString *)deviceId
            success:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=getZoneList";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *item in json[@"result"]) {
            SMDZone *model = [[SMDZone alloc] initWithJson:item];
            [arr addObject:model];
        }
        success(arr);
    } fail:^(id error) {
        
    }];
}

- (void)setZoneProperty:(NSString *)zoneId
               position:(NSString *)position
             alarm_type:(NSString *)alarm_type
   bind_device_video_sn:(NSString *)bind_device_video_sn
                success:(void(^)())success
                   fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=setZoneProperty";
    NSDictionary *dic = @{@"zoneId":zoneId,
                          @"position":position,
                          @"alarm_type":alarm_type,
                          @"bind_device_video_sn":bind_device_video_sn};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)getControlList:(NSString *)deviceId
               success:(void(^)(NSArray *array))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceArm&a=getControlList";
    NSDictionary *dic = @{@"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        NSMutableArray *list = [NSMutableArray array];
        for (NSDictionary *item in json[@"result"]) {
            SMDControl *model = [[SMDControl alloc] initWithJson:item];
            [list addObject:model];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)sendSosWithAuthorizeUserId:(NSInteger)authorizeUserId
                          deviceId:(NSString *)deviceId
                            action:(NSInteger)action
                           success:(void(^)())success
                              fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Sos&a=sendSos";
    NSDictionary *dic = @{@"authorizeUserId":[NSNumber numberWithInteger:authorizeUserId],
                          @"deviceId":deviceId,
                          @"action":[NSNumber numberWithInteger:action]};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)getSosList:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Sos&a=getSosList";
    [self postUrl:url data:@{} success:^(id json) {
        if ([json[@"result"] isKindOfClass:[NSArray class]]) {
            NSMutableArray *arr = [NSMutableArray array];
            for (NSDictionary *item in json[@"result"]) {
                SMSos *model = [[SMSos alloc] initWithJson:item];
                [arr addObject:model];
            }
            success(arr);
        }
    } fail:^(id error) {
        
    }];
}

- (void)sendSosWithLat:(CGFloat)lat
                   lng:(CGFloat)lng
               success:(void(^)())success
                  fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Sos&a=sendSos";
    NSDictionary *dic = @{@"lat":[NSNumber numberWithFloat:lat],
                          @"lng":[NSNumber numberWithFloat:lng]};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

+  (id)instance{
    static SMDeviceMgr *ins;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ins = [SMDeviceMgr new];
    });
    return ins;
}

- (NSArray<SMDevice *> *)myDevices{
    if(!_myDevices){
        [self getArmDeviceListSuccess:^(NSArray *list) {
            
        } Fail:^(NSString *msg) {
            
        }];
    }
    return _myDevices;
}

- (void)DAAOnlyAnswer:(void(^)())success
                 fail:(void(^)(NSString *msg))fail
{
    
}
- (void)DAAOnlySilent:(void(^)())success
                 fail:(void(^)(NSString *msg))fail
{
    
}
- (void)DAADisarmAndRemove:(void(^)())success
                      fail:(void(^)(NSString *msg))fail
{
    
}

- (void)updateCameraNameWithID:(NSInteger)mid
                        system:(NSDictionary *)system
                        method:(NSString *)method
                        params:(NSDictionary *)params
                       success:(void(^)())success
                          fail:(void(^)(NSString *msg))fail
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    dic[@"appName"] = @"zhihuApp";
    dic[@"version"] = @"1.0.0";
    dic[@"timeStamp"] = [NSString stringWithFormat:@"%f", [NSDate date].timeIntervalSince1970];
    dic[@"imei"] = [[UIDevice currentDevice].identifierForVendor UUIDString];
    const NSString *key = @"7802408150050123";
    dic[@"sign"] = [NSString stringWithFormat:@"%@%@%@", dic[@"appName"], dic[@"timeStamp"], key].md5_32;
    NSDictionary *d1 = @{@"ver":@"1.0.0",
                         @"sign":[NSString stringWithFormat:@"%@%@%@", dic[@"appName"], dic[@"timeStamp"], key].md5_32,
                         @"key":@"7802408150050123",
                         @"time":[NSString stringWithFormat:@"%f", [NSDate date].timeIntervalSince1970]};
    NSDictionary *d2 = @{@"accessToken":@"",
                         @"cameraId":@"1",
                         @"cameraName":@"123456"};
    NSDictionary *data = @{@"system":d1,
                          @"method":@"camera/updateCameraName",
                          @"params":params};
    [self postUrl:@"https://open.ys7.com/api/method" data:data success:^(id json) {
        
    } fail:^(id error) {
        
    }];
}

@end
