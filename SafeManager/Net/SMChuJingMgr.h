//
//  SMChuJingMgr.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseNetMgr.h"

@class SMChuJing;
@class SMChuJingService;
@class SMChuJingServiceOrder;

@interface SMChuJingMgr : SMBaseNetMgr

- (void)getChuJingOrderList:(void (^)(NSArray *))success;

/**
 出警服务订单详情
 
 @param oid     出警服务订单id
 @param success 成功
 */
- (void)getChuJingOrderDetail:(NSString *)oid
                      success:(void (^)(SMChuJing *chujing))success;


/**
 出警服务商类型树

 @param success 成功
 */
- (void)getServiceComTypeList:(void(^)(NSArray *list))success;

/**
 出警服务商列表

 @param typeId 类型ID
 @param success 成功
 */
- (void)getServiceComList:(NSString *)typeId
                  success:(void(^)(NSArray *list))success;


/**
 出警服务商详情

 @param sid 服务商ID
 @param success 成功
 */
- (void)getServiceComDetail:(NSString *)sid
                    success:(void(^)(SMChuJingService *model))success;


/**
 出警服务订单创建

 @param deviceId 设备ID
 @param serviceComId 服务商ID
 @param serviceAddr 出警地址
 @param success 成功
 */
- (void)serviceDealCreate:(NSString *)deviceId
             serviceComId:(NSString *)serviceComId
              serviceAddr:(NSString *)serviceAddr
                  success:(void(^)(SMChuJingServiceOrder *order))success;

@end
