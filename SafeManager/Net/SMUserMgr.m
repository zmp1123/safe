//
//  SMUserMgr.m
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMUserMgr.h"
#import "NSString+Extension.h"
#import "SMAccountAuthorize.h"
#import "SMDeviceAuthorize.h"

@interface SMUserMgr ()

@end

@implementation SMUserMgr
@synthesize user = _user, lastUserPwd = _lastUserPwd, lastUserName = _lastUserName;

+ (id)instance{
    static SMUserMgr* ins;
    static dispatch_once_t t;
    dispatch_once(&t, ^{
        ins = [SMUserMgr new];
    });
    return ins;
}

- (void)sendVerifyCodePhoneNo:(NSString *)no success:(void (^)())success fail:(void (^)(NSString *))fail
{
    [self postUrl:@"http://139.196.234.54/AppApi?c=Util&a=getMsgVerifyCode" data:@{@"phone":no} success:^(id json) {
        if (success) {
            success();
        }
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)registerUserPhone:(NSString *)phone
                 password:(NSString *)pwd
               verifyCode:(NSString *)code
                  success:(void (^)(SMUser *))success
                     fail:(void (^)(NSString *))fail{
    id paras = @{@"phone":phone,
                 @"password":pwd.md5_32,
                 @"verifyCode":code};
    [self postUrl:@"http://139.196.234.54/AppApi?c=Util&a=appUserRegister" data:paras success:^(id json) {
        if(success){
            SMUser *user = [[SMUser alloc]initWithJson:json[@"result"]];
            self.user = user;
            success(user);
        }
    } fail:^(id error) {
        if(fail){
            fail(error);
        }
    }];
}

- (void)loginWithUserName:(NSString *)name
                 password:(NSString *)pwd
                  success:(void (^)(id))success
                     fail:(void(^)(NSString *msg)) fail{
    NSDictionary *data = @{@"username":name,
                           @"password":pwd.md5_32};
    [self postUrl:@"http://139.196.234.54/AppApi?c=Util&a=appUserLogin" data:data success:^(NSDictionary* json) {
        NSDictionary *resData = json[@"result"];
        _user = [[SMUser alloc]initWithJson:resData];
        if(self.user.userName){
            [[NSUserDefaults standardUserDefaults]setObject:self.user.userName forKey:@"user.lastUserName"];
        }
        [[NSUserDefaults standardUserDefaults] setObject:pwd forKey:@"user.lastUserPwd"];
        
        if(self.user.token){
            self.token = self.user.token;
        };
        
        if(success){
            success(resData);
        }

    } fail:^(id error) {
        if(fail){
            fail(error);
        }
    }];
}

- (void)appUserModifyPwdByVerifyCode:(NSString *)verifyCode
                               phone:(NSString *)phone
                            password:(NSString *)password
                             success:(void(^)())success
                                fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Util&a=appUserModifyPwdByVerifyCode";
    NSDictionary *dic = @{@"phone":phone,
                          @"password":password.md5_32,
                          @"verifyCode":verifyCode};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)appUserLogout:(void (^)(id res))success
                 fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=appUserLogout";
    [self postUrl:url data:@{} success:^(id json) {
        self.token = nil;
        success(json);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)queryUserSuccess:(void (^)(SMUser *))success fail:(void (^)(NSString *))fail{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=getAppUserInfo";
    [self postUrl:url data:@{} success:^(id json) {
        SMUser *user = [[SMUser alloc]initWithJson:json[@"result"]];
        _user = user;
        if(success){
            success(user);
        }
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)uploadImageData:(NSData *)data success:(void (^)(NSDictionary *))success fail:(void (^)(NSString *))fail{
    NSString *url = @"http://139.196.234.54/AppApi?c=Util&a=imgUpload";
    [self uploadData:data url:url formData:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"upload" fileName:@"upload.jpeg" mimeType:@"jpeg"];
        
    } params:@{@"upload":data} success:^(NSDictionary *response) {
        if(success){
            success(response);
        }
    } fail:^(NSString *msg) {
        if(fail){
            fail(msg);
        }
    }];
    
    
}

- (void)updateUserInfoNickName:(NSString *)nickName
                      idNumber:(NSString *)idNumber
                        avatar:(NSString *)avatar
                         email:(NSString *)email
                           sex:(int)sex
                            qq:(NSString *)qq
                      province:(NSString *)provice
                          city:(NSString *)city
                       distrit:(NSString *)district
                          addr:(NSString *)addr
                       success:(void(^)(SMUser *usr))success
                          fail:(void(^)(NSString *msg))fail{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=setAppUserInfo";
    id data = @{@"nickname":nickName == NULL ? self.user.nickName : nickName,
                @"idNumber":idNumber == NULL ? self.user.idNumber : idNumber,
                @"avatar":avatar == NULL ? self.user.avatar : avatar,
                @"email":email == NULL ? self.user.email : email,
                @"sex":@(sex >= 2 ? self.user.sex : sex),
                @"qq":qq == NULL ? self.user.qq : qq,
                @"province":provice == NULL ? self.user.provice : provice,
                @"city":city == NULL ? self.user.city : city,
                @"district":district == NULL ? self.user.district : district,
                @"addr":addr == NULL ? self.user.address : addr};
    [self postUrl:url data:data success:^(NSDictionary* json) {
        if(success){
            if(nickName)
            self.user.nickName = nickName;
            if(idNumber)
            self.user.idNumber = idNumber;
            if(avatar)
            self.user.avatar = avatar;
            if(email)
            self.user.email = email;
            if(sex < 2)
            self.user.sex = sex;
            if(qq)
            self.user.qq = qq;
            if(provice)
            self.user.provice = provice;
            if(city)
            self.user.city = city;
            if(district)
            self.user.district = district;
            if(addr)
            self.user.address = addr;
            success(self.user);
        }
    } fail:^(id error) {
        if(fail){
            fail(error);
        }
    }];
}

- (void)setUser:(SMUser *)user{
    if(self.user.userName){
        [[NSUserDefaults standardUserDefaults]setObject:user.userName forKey:@"user.lastUserName"];
    }
    _user = user;
}

- (SMUser *)user{
    if(!_user){
        //todo
        //查询user
        // _user = [SMUser new];
    }
    return _user;
}

- (NSString *)lastUserName{
    if(!_lastUserName){
        _lastUserName = [[NSUserDefaults standardUserDefaults]objectForKey:@"user.lastUserName"];
    }
    return _lastUserName;
}

- (NSString *)lastUserPwd{
    if(!_lastUserPwd){
        _lastUserPwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user.lastUserPwd"];
    }
    return _lastUserPwd;
}

- (void)getAuthorizeAppUserList:(void(^)(NSArray *array))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=getAuthorizeAppUserList";
    [self postUrl:url data:@{} success:^(id json) {
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMAccountAuthorize *aa = [[SMAccountAuthorize alloc] initWithJson:item];
            [list addObject:aa];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)addAuthorizeAppUser:(NSString *)phone
                    success:(void(^)())success
                       fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=authorizeAppUserCreate";
    NSDictionary *dic = @{@"phone":phone};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)deleteAuthorizeAppUser:(NSString *)authorizeUserId
                       success:(void(^)())success
                          fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=authorizeAppUserDel";
    NSDictionary *dic = @{@"authorizeUserId":authorizeUserId};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)getAuthorizeDeviceList:(NSString *)authorizeUserId
                       success:(void(^)(NSArray *array))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=authorizeDeviceList";
    NSDictionary *dic = @{@"authorizeUserId":authorizeUserId};
    [self postUrl:url data:dic success:^(id json) {
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMDeviceAuthorize *da= [[SMDeviceAuthorize alloc] initWithJson:item];
            [list addObject:da];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)updateAuthorizeDevice:(NSString *)authorizeUserId
                     deviceId:(NSString *)deviceId
                       action:(BOOL)action
                      success:(void (^)())success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Profile&a=authorizeDeviceUpdate";
    NSDictionary *dic = @{@"authorizeUserId":authorizeUserId,
                          @"deviceId":deviceId,
                          @"action":[NSNumber numberWithBool:!action]};
    [self postUrl:url data:dic success:^(id json) {
        success();
    } fail:^(id error) {
        
    }];
}

- (void)getYsAccessToken:(void (^)(NSDictionary *response))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=DeviceVideo&a=getYsAccessToken";
    [self postUrl:url data:@{} success:^(id json) {
        success(json[@"result"]);
    } fail:^(id error) {
        
    }];
}

@end
