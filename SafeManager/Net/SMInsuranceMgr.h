//
//  SMInsuranceMgr.h
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMBaseNetMgr.h"

@class SMInsuranceOrder;

@interface SMInsuranceMgr : SMBaseNetMgr

- (void)getInsuranceList:(void(^)(NSArray *list)) success;


/**
 保险历史订单

 @param success 成功
 */
- (void)getInsuranceDealList:(void(^)(NSArray *list))success;


/**
 保险订单详情

 @param iid     保险订单ID
 @param success 成功
 */
- (void)getInsuranceDealDetail:(NSString *)iid
                       success:(void (^)(SMInsuranceOrder *order))success;

/**
 保险订单创建

 @param property 财产
 @param count 份数
 @param durationCount 年数
 @param deviceId 设备ID
 @param insuranceId 保险ID
 @param shopAddr 店铺地址
 @param peopleType 客户类型
 @param success 成功
 */
- (void)insuranceDealCreateWithProperty:(NSString *)property
                                  count:(NSInteger)count
                          durationCount:(NSInteger)durationCount
                               deviceId:(NSString *)deviceId
                            insuranceId:(NSString *)insuranceId
                               shopAddr:(NSString *)shopAddr
                             peopleType:(NSString *)peopleType
                                success:(void(^)(NSDictionary *data))success;

/**
 图片广告模块

 @param success 成功返回图片广告列表
 @param fail 失败
 */
- (void)getAdList:(void(^)(NSArray *list))success
             fail:(void(^)(NSString *msg))fail;

@end
