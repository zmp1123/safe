//
//  SMChuJingMgr.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMChuJingMgr.h"
#import "SMChuJing.h"
#import "SMServiceComTypeModel.h"
#import "SMChuJingService.h"
#import "SMChuJingServiceOrder.h"

@implementation SMChuJingMgr

+ (id)instance{
    static SMChuJingMgr *ins;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ins = [SMChuJingMgr new];
    });
    return ins;
}

- (void)getChuJingOrderList:(void (^)(NSArray *))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=getServiceDealList";
    [self postUrl:url data:@{} success:^(id json) {
//        NSLog(@"%@", json);
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMChuJing *chujing = [[SMChuJing alloc] initWithJson:item];
            [list addObject:chujing];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)getChuJingOrderDetail:(NSString *)oid
                      success:(void (^)(SMChuJing *chujing))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=getServiceDealDetail";
    NSDictionary *dic = @{@"id":oid};
    [self postUrl:url data:dic success:^(id json) {
        SMChuJing *chujing = [[SMChuJing alloc] initWithJson:json[@"result"]];
        success(chujing);
    } fail:^(id error) {
        
    }];
}

- (void)getServiceComTypeList:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=getServiceComTypeList";
    [self postUrl:url data:@{} success:^(id json) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *item in json[@"result"]) {
            SMServiceComTypeModel *model = [[SMServiceComTypeModel alloc] initWithJson:item];
            [arr addObject:model];
        }
        success(arr);
    } fail:^(id error) {
        
    }];
}

- (void)getServiceComList:(NSString *)typeId
                  success:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=getServiceComList";
    NSDictionary *dic = @{@"typeId":typeId};
    [self postUrl:url data:dic success:^(id json) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *item in json[@"result"]) {
            SMChuJingService *model = [[SMChuJingService alloc] initWithJson:item];
            [arr addObject:model];
        }
        success(arr);
    } fail:^(id error) {
        
    }];
}

- (void)getServiceComDetail:(NSString *)sid
                    success:(void(^)(SMChuJingService *model))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=getServiceComDetail";
    NSDictionary *dic = @{@"id":sid};
    [self postUrl:url data:dic success:^(id json) {
        SMChuJingService *m = [[SMChuJingService alloc] initWithJson:json[@"result"]];
        success(m);
    } fail:^(id error) {
        
    }];
}

- (void)serviceDealCreate:(NSString *)deviceId
             serviceComId:(NSString *)serviceComId
              serviceAddr:(NSString *)serviceAddr
                  success:(void(^)(SMChuJingServiceOrder *order))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Service&a=serviceDealCreate";
    NSDictionary *dic = @{@"deviceId":deviceId,
                          @"serviceComId":serviceComId,
                          @"serviceAddr":serviceAddr};
    [self postUrl:url data:dic success:^(id json) {
        SMChuJingServiceOrder *order = [[SMChuJingServiceOrder alloc] initWithJson:json[@"result"]];
        success(order);
    } fail:^(id error) {
        
    }];
}

@end
