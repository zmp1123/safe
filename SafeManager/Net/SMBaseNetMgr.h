//
//  SMBaseNetMgr.h
//  SafeManager
//
//  Created by zxf on 16/10/21.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFURLSessionManager.h>

typedef void(^httpCallBack)(id json);
typedef void(^httpFailBlock)(id error);

@interface SMBaseNetMgr : NSObject

@property(nonatomic, strong) NSString *token;

-(void)postUrl:(NSString *)url
          data:(id)data
       success:(httpCallBack) successBlock
          fail:(httpFailBlock) failBlock;
-(void)post2Url:(NSString *)url
          data:(id)data
       success:(httpCallBack) successBlock
          fail:(httpFailBlock) failBlock;

-(void)uploadData:(NSData *)data
              url:(NSString *)url
         formData:(void(^)(id<AFMultipartFormData> formData))formBlock
           params:(NSDictionary *)params
          success:(void(^)(NSDictionary *response)) success
             fail:(void(^)(NSString *msg)) fail;

+(id)instance;
@end
