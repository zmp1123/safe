//
//  SMInsuranceMgr.m
//  SafeManager
//
//  Created by zxf on 16/10/22.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import "SMInsuranceMgr.h"
#import "SMInsurance.h"
#import "SMInsuranceOrder.h"
#import "SMNewsAd.h"

@implementation SMInsuranceMgr

- (void)getInsuranceList:(void (^)(NSArray *))success{
    NSString *url = @"http://139.196.234.54/AppApi?c=Insurance&a=getInsuranceTypeList";
    [self postUrl:url data:@{} success:^(id json) {
        NSLog(@"%@", json);
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMInsurance *insurance = [SMInsurance new];
            insurance.avatar = item[@"avatar"];
            insurance.coverage = [item[@"coverage"]floatValue];
            insurance.duration = [item[@"duration"]intValue];
            insurance.insuranceId = item[@"id"];
            insurance.insurance_type = item[@"insurance_type"];
            insurance.price = [item[@"price"]floatValue];
            insurance.title = item[@"title"];
            [list addObject:insurance];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)getInsuranceDealList:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Insurance&a=getInsuranceDealList";
    [self postUrl:url data:@{} success:^(id json) {
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMInsuranceOrder *order = [[SMInsuranceOrder alloc] initWithJson:item];
            [list addObject:order];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

- (void)getInsuranceDealDetail:(NSString *)iid
                       success:(void (^)(SMInsuranceOrder *order))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Insurance&a=getInsuranceDealDetail";
    NSDictionary *dic = @{@"id":iid};
    [self postUrl:url data:dic success:^(id json) {
        SMInsuranceOrder *order = [[SMInsuranceOrder alloc] initWithJson:json[@"result"]];
        success(order);
    } fail:^(id error) {
        
    }];
}

- (void)insuranceDealCreateWithProperty:(NSString *)property
                                  count:(NSInteger)count
                          durationCount:(NSInteger)durationCount
                               deviceId:(NSString *)deviceId
                            insuranceId:(NSString *)insuranceId
                               shopAddr:(NSString *)shopAddr
                             peopleType:(NSString *)peopleType
                                success:(void(^)(NSDictionary *data))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Insurance&a=insuranceDealCreate";
    NSDictionary *dic = @{@"property":property,
                          @"count":[NSNumber numberWithInteger:count],
                          @"durationCount":[NSNumber numberWithInteger:durationCount],
                          @"deviceId":deviceId,
                          @"insuranceId":insuranceId,
                          @"shopAddr":shopAddr,
                          @"peopleType":peopleType};
    [self postUrl:url data:dic success:^(id json) {
        success(json[@"result"]);
    } fail:^(id error) {
        
    }];
}

- (void)getAdList:(void(^)(NSArray *list))success
             fail:(void(^)(NSString *msg))fail
{
    NSString *url = @"http://139.196.234.54/AppApi?c=News&a=getAdList";
    [self postUrl:url data:@{} success:^(id json) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *item in json[@"data"]) {
            SMNewsAd *model = [[SMNewsAd alloc] initWithJson:item];
            [arr addObject:model];
        }
        success(arr);
    } fail:^(id error) {
        if (fail) {
            fail(error);
        }
    }];
}

+ (id)instance{
    static SMInsuranceMgr *ins;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ins = [SMInsuranceMgr new];
    });
    return ins;
}

@end
