//
//  SMPayMgr.h
//  SafeManager
//
//  Created by zmp1123 on 16/11/9.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaseNetMgr.h"

@interface SMPayMgr : SMBaseNetMgr

/**
 支付宝签名

 @param partner 2088421635267699
 @param service mobile.securitypay.pay
 @param success
 */
- (void)getAlipaySignWithPartner:(NSString *)partner
                         service:(NSString *)service
                         success:(void(^)(NSString *sign))success;

- (void)getAlipaySignWithPartner:(NSString *)partner
                         service:(NSString *)service
                        sellerId:(NSString *)sellerId
                      outTradeNo:(NSString *)outTradeNo
                         subject:(NSString *)subject
                            body:(NSString *)body
                        totalFee:(NSString *)totalFee
                         success:(void (^)(NSDictionary *data))success;


/**
 发起预支付

 @param WIDout_trade_no
 @param WIDtotal_fee
 @param success
 */
- (void)getWxPayIndex:(NSString *)WIDout_trade_no
         WIDtotal_fee:(NSString *)WIDtotal_fee
              success:(void(^)(NSDictionary *data))success;



@end
