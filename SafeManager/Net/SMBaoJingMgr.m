//
//  SMBaoJingMgr.m
//  SafeManager
//
//  Created by zmp1123 on 16/11/3.
//  Copyright © 2016年 zmp1123. All rights reserved.
//

#import "SMBaoJingMgr.h"
#import "SMBaoJing.h"
#import "SMBaoJingService.h"
#import "SMBaoJingServiceOrder.h"

@implementation SMBaoJingMgr

+ (id)instance{
    static SMBaoJingMgr *ins;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ins = [SMBaoJingMgr new];
    });
    return ins;
}

- (void)getBaoJingOrderList:(void (^)(NSArray *))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Vip&a=getVipDealList";
    [self postUrl:url data:@{} success:^(id json) {
//        NSLog(@"%@", json);
        NSMutableArray *list = [NSMutableArray new];
        for(NSDictionary *item in json[@"result"]){
            SMBaoJing *baojing = [[SMBaoJing alloc] initWithJson:item];
            [list addObject:baojing];
        }
        success(list);
    } fail:^(id error) {
        
    }];
}

/*
 "app_user_id" = 25;
 "create_time" = "2016-09-16 17:16:13";
 "deal_sn" = VIP2016091617161335122;
 "deal_status" = 1;
 desc = "";
 "device_arm_id" = 1;
 "device_arm_sn" = 266A00C603FDB7;
 "device_arm_title" = "\U7ba1\U5bb6\U4fdd";
 "end_date" = "2017-04-01";
 flag = 0;
 id = 32;
 "order_time" = "2016-09-16 17:16:13";
 "pay_time" = "";
 price = 666;
 "start_date" = "2016-11-03";
 "vip_detail" =         {
    description = "";
    duration = 99;
    id = 1;
    price = 666;
    title = "\U7ec8\U8eab\U670d\U52a1\U8d39";
 };
 "vip_id" = 1;
 "vip_title" = "\U7ec8\U8eab\U670d\U52a1\U8d39";
 */
- (void)getBaoJingOrderDetail:(NSString *)oid
                      success:(void (^)(SMBaoJing *baojing))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Vip&a=getVipDealDetail";
    NSDictionary *dic = @{@"id":oid};
    [self postUrl:url data:dic success:^(id json) {
        SMBaoJing *baojing = [[SMBaoJing alloc] initWithJson:json[@"result"]];
        success(baojing);
    } fail:^(id error) {
        
    }];
    
}

- (void)getVipTypeList:(void(^)(NSArray *list))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Vip&a=getVipTypeList";
    [self postUrl:url data:@{} success:^(id json) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *item in json[@"result"]) {
            SMBaoJingService *model = [[SMBaoJingService alloc] initWithJson:item];
            [arr addObject:model];
        }
        success(arr);
    } fail:^(id error) {
        
    }];
}

- (void)vipDealCreate:(NSString *)vipId
             deviceId:(NSString *)deviceId
              success:(void(^)(SMBaoJingServiceOrder *order))success
{
    NSString *url = @"http://139.196.234.54/AppApi?c=Vip&a=vipDealCreate";
    NSDictionary *dic = @{@"vipId":vipId,
                          @"deviceId":deviceId};
    [self postUrl:url data:dic success:^(id json) {
        SMBaoJingServiceOrder *order = [[SMBaoJingServiceOrder alloc] initWithJson:json[@"result"]];
        success(order);
    } fail:^(id error) {
        
    }];
}

@end
