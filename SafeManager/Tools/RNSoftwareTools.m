//
//  RNSoftwareTools.m
//  ReNao
//
//  Created by zmp1123 on 10/12/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import "RNSoftwareTools.h"

static RNSoftwareTools *s_defaultServices = nil;

@interface RNSoftwareTools()


@end

@implementation RNSoftwareTools

+ (RNSoftwareTools *) defaultServices
{
    if(s_defaultServices == nil) {
        s_defaultServices = [[RNSoftwareTools alloc] init];
    }
    return s_defaultServices;
}

- (id) init
{
    self = [super init];
    if (self) {
        [self customInit_RTSoftwareTools];
    }
    return self;
}

- (void) customInit_RTSoftwareTools
{
    
}

-(void)showMessageForMessage:(NSString *)message
                 cancelTitle:(NSString *)cancelTitle
                  otherTitle:(NSString *)otherTitle
                   andAction:(CompleteBlock)block
{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:RNNSLocalizedString(@"_RNbase_notice_", nil) message:message delegate:nil cancelButtonTitle:cancelTitle otherButtonTitles:otherTitle, nil];
//    
//    [alert showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//        block(buttonIndex);
//    }];
}

- (void)showMessageForMessage:(NSString *)message
                confirmlTitle:(NSString *)cancelTitle
                    andAction:(CompleteBlock)block
{
    [self showMessageForMessage:message cancelTitle:cancelTitle otherTitle:nil andAction:block];
}

- (void)showMessageForMessage:(NSString *)message
{
//    [self showMessageForMessage:message confirmlTitle:RNNSLocalizedString(@"_RNbase_confirm_Btn_", nil) andAction:^(NSInteger buttonIndex) {
//    }];
}

-(long long int)nowTimeString
{
    return [[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]] longLongValue];
}

-(long long int)timeString:(NSString *)string withFormat:(NSString *)formatString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:formatString]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    return [[NSString stringWithFormat:@"%ld", (long)[[formatter dateFromString:string] timeIntervalSince1970]] longLongValue];
}

-(NSString *)dateForString:(long long int)time withFormat:(NSString *)formatString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:formatString];
    return [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
}

-(CGRect)CountTheString:(NSString *)string withTheStringFont:(UIFont *)font andMaxSize:(CGSize )size
{
    return [string boundingRectWithSize:size
                                options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{NSFontAttributeName:font}
                                context:nil];
}

-(NSString*)getUUID
{
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    return result;
}


- (NSString *)getTheLanguageStringByIdentifierString:(NSString *)identifierString andDefaultString:(NSString *)defaultString
{
    return NSLocalizedString(identifierString, nil);
}



- (NSString *)getTheLanguageStringByZhString:(NSString *)zhString andEnString:(NSString *)enString
{
    if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0] isEqualToString:@"zh-Hans"]){
        return zhString;
    }else{
        return enString;
    }
}

- (void)setCurrentUserData
{

}

- (BOOL)judgeTheUserIsLogin
{
    return YES;
}


+(BOOL) isChinese:(NSString *)str{
    int length = 0;
    char* p = (char*)[str cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i = 0; i < [str lengthOfBytesUsingEncoding:NSUnicodeStringEncoding]; i++) {
        if (*p) {
            p++;
            length++;
        }
        else {
            p++;
        }
    }
    return length % 2 == 1;
}

+ (NSInteger)strLength:(NSString*)str
{
    int length = 0;
    char* p = (char*)[str cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i = 0; i < [str lengthOfBytesUsingEncoding:NSUnicodeStringEncoding]; i++) {
        if (*p) {
            p++;
            length++;
        }
        else {
            p++;
        }
    }
    return length;
}

+(BOOL)isValidatePhoneNumber:(NSString *)phoneNumber{
    // phoneNumber begin with 13,15,18,
    // then following by 9 numbers
    NSString *reg = @"^1[3,5,8][0-9]{9}$";
    NSError *error = nil;
    NSRegularExpression *telEx = [NSRegularExpression regularExpressionWithPattern:reg options:NSRegularExpressionCaseInsensitive error:&error];
    if([telEx firstMatchInString:phoneNumber options:0 range:NSMakeRange(0, phoneNumber.length)]) return true;
    return false;
}
@end
