//
//  RNSoftwareTools.h
//  ReNao
//
//  Created by zmp1123 on 10/12/15.
//  Copyright © 2015 zmp1123. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^CompleteBlock) (NSInteger buttonIndex);

@interface RNSoftwareTools : NSObject

@property (nonatomic, strong) UIViewController *currentVC;  //当前的视图控制器

+ (RNSoftwareTools *) defaultServices;
/**
 *消息提示框
 */
- (void)showMessageForMessage:(NSString *)message cancelTitle:(NSString *)cancelTitle otherTitle:(NSString *)otherTitle andAction:(CompleteBlock)block;
/**
 *消息提示框OnlyOneBtn
 */
- (void)showMessageForMessage:(NSString *)message confirmlTitle:(NSString *)cancelTitle andAction:(CompleteBlock)block;
/**
 *消息提示框
 */
- (void)showMessageForMessage:(NSString *)message;
/**
 *获取当前时间戳
 */
-(long long int)nowTimeString;
/**
 * 字符串转时间戳
 */
-(long long int)timeString:(NSString *)string withFormat:(NSString *)formatString;
/**
 *根据时间前戳进行转换
 */
-(NSString *)dateForString:(long long int)time withFormat:(NSString *)formatString;
/**
 *计算字符串长度
 */
-(CGRect)CountTheString:(NSString *)string withTheStringFont:(UIFont *)font andMaxSize:(CGSize )size;
/**
 *获取UUID
 */
-(NSString*)getUUID;
/**
 * 语言/语种
 */
- (NSString *)getTheLanguageStringByIdentifierString:(NSString *)identifierString andDefaultString:(NSString *)defaultString;
/**
 *  根据系统语言获取值
 *
 *  @param zhString 中文
 *  @param enString 英文
 *
 *  @return 字符串
 */
- (NSString *)getTheLanguageStringByZhString:(NSString *)zhString andEnString:(NSString *)enString;
/**
 * 设置当前用户
 */
- (void)setCurrentUserData;
/**
 * 判断用户是否登录,没有登录跳转登录页面
 */
- (BOOL)judgeTheUserIsLogin;

/**
 *  是否中文字符串
 *
 *  @param str 字符串
 *
 *  @return BOOL
 */
+(BOOL)isChinese:(NSString*)str;

/**
 *  字符串长度，英文1个字母占一个长度，中文占2个长度
 *
 *
 *  @param str
 *
 *  @return 
 */
+(NSInteger)strLength:(NSString*)str;

/*
 * 验证手机号码正确性
 */
+(BOOL) isValidatePhoneNumber:(NSString*)phoneNumber;

@end
