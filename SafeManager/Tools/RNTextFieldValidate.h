//
//  RNTextFieldValidate.h
//  HotNow
//
//  Created by zmp1123 on 12/21/15.
//  Copyright © 2015 com.renaoapp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RNTextFieldValidate : NSObject


/**
 *  判断手机号是否合规
 *
 *  @param phone 手机号码
 *
 *  @return
 */
+ (BOOL)judgePhoneNumberLegal:(NSString *)phone;
/**
 *  判断验证码是否合规
 *
 *  @param code 验证码
 *
 *  @return 
 */
+ (BOOL)judgeCodeNumberLegal:(NSString *)code;

/**
 *  判断密码是否合规
 *
 *  @param password 密码
 *
 *  @return
 */
+ (BOOL)judgePassWordLegal:(NSString *)password;
/**
 *  判断昵称是否合规
 *
 *  @param nickname 昵称
 *
 *  @return 
 */
+ (BOOL)judgeNicknameLegal:(NSString *)nickname;

/**
 *  判断身份证号是否合规
 *
 *  @param cardNo 身份证号
 *
 *  @return 
 */
+ (BOOL)judgeCardNoLegal:(NSString *)cardNo;
/**
 *  判断输入的文本内容长度范围(按字符计算)
 *
 *  @param min 最小字符
 *  @param max 最大字符
 *
 *  @return
 */
+ (BOOL)judgeTextLengthWithText:(NSString *)text andMin:(NSInteger)min andMax:(NSInteger)max;

/**
 判断邮箱格式

 @param email 邮箱
 @return 
 */
+ (BOOL)isValidateEmail:(NSString *)email;

@end
