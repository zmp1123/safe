//
//  AppDelegate.h
//  SafeManager
//
//  Created by zxf on 16/10/2.
//  Copyright © 2016年 zxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)showMain;
-(void)showSignIn;

@end

